<style>
*, *:before, *:after {
  box-sizing: inherit;
}
.row{
  width: 90%;
  margin: 0 auto;
  overflow: hidden;
  border-top: 1px solid #e500b2;
  border-top-color: #e500b2;
  /*background-color: pink;*/
}
.column {
  float: left;
  width: 25%;
  margin-bottom: 16px;
  padding: 0 8px;
}

.card {
  margin: 8px;
  margin: 0 auto;
  font-size: 0.9em;
  min-width: 100px;
  border-radius: 5px 5px 0 0;
  overflow: hidden;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);

}

.about-section {
  padding: 50px;
  text-align: center;
  background-color: #474e5d;
  color: white;
}

.content-container {
  padding: 16px;
  text-align: center;
}

.content-container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}
.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #ca2fa7;
  text-align: center;
  cursor: pointer;
  width: 60%;
}

.button:hover {
  background-color: #555;
}
.xem_them{
  color: #e500b2;
}
.title{
  margin-bottom: 20px;
}
.fas_button{
  width: 30px;
  height: 30px;
  font-size: 1.2em;
  text-align: center;
  line-height: 30px;
  display: inline-block;
  color: grey;
  /*padding: 5px;*/
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2);
}
@media screen and (max-width: 500px) {
  .column {
    width: 100%;
    /*display: block;*/
  }
}
</style>

</div>
  <?php 
  include "connect.php";
  $thu_muc_anh = "image/product/";
  $sql = "SELECT * from do_dung ORDER BY ma DESC limit 0,4";
  $result = mysqli_query($connect, $sql);
  

   ?>
   <br>
   <br>
<div class="row">
<br>
<br>
  <h2 align="center">Hàng mới về</h2>
<br> <br>
  <?php foreach ($result as $each) { ?>
  <div class="column">
    <div class="card">
      <p style="background: url('<?php echo $thu_muc_anh . $each['anh'] ?>');width: 100%;height: 300px;background-size: cover;background-position: center;background-repeat: no-repeat;"></p>
      </div>
      <div class="content-container">
        <h2><?php echo $each['ten'] ?></h2>
        <p><?php echo mb_substr($each['mo_ta'], 0, 40, 'utf-8') ?></p>
    
        <a href="chi_tiet_san_pham.php?ma=<?php echo $each['ma'] ?>"><button class="button">Xem thêm</button></a>
      </div>
    <!-- </div> -->
  </div>
  <?php } ?>
</div>

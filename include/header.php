<?php 
	include "connect.php";
	$thu_muc_anh = 'image/banner/';
	$sql = "SELECT anh from background where loai_anh = 0 order by ma desc limit 0,1";
	$result = mysqli_query($connect,$sql);
	$each = mysqli_fetch_array($result);
 ?>
<div class="menu">
	<div class="logo">
		<a href="index.php" style="background: url('<?php echo $thu_muc_anh . $each['anh'] ?>');background-size: cover;background-repeat: no-repeat;background-position: center;background-color: #F6ADE7;height: 90%;width: 90%;display: inline-block;border-radius: 10px;">
		</a>
	</div>
	
	<?php include "include/menu_ngang.php" ?>

	<!--Menu tài khoản--->

	<?php if(!isset($_SESSION['ma_kh'])){ ?>
		<?php include "dang_nhap.php" ?>
		<?php include "dang_ky.php" ?>
		<ul id="tai_khoan">
			<li>
				<a><b>Tài khoản</b></a>
				<ul class="menu_tai_khoan">
					<li>
						<a onclick="document.getElementById('id01').style.display='block'">Đăng nhập</a>
					</li>					
					<li>
						<a do="register" name="register" onclick="document.getElementById('id02').style.display='block'">
							Đăng ký
						</a>
					</li>
				</ul>
			</li>
		</ul>
	<?php }else{ ?>
		<ul id="tai_khoan">
			<li>
				<a><b><?php echo $_SESSION['ten_kh'] ?></b></a>
				<ul class="menu_tai_khoan">
					<li>
						<a href="xem_hoa_don.php">
							Đơn mua
						</a>
					</li>
					<li>
						<a href="xem_ho_so.php">
							Hồ sơ
						</a>
					</li>
					<li>
						<a href="dang_xuat.php">
							Đăng xuất
						</a>
					</li>
					
				</ul>
			</li>
		</ul>
	<?php } ?>
	<!--End quản lí tài khoản-->
	<!--Cart-->
	<a id="cart" href="xem_gio_hang.php" title="Giỏ hàng">
	
	</a>
	<!-- Nhận value ô tìm kiếm-->
	<?php 
	$tim_kiem = '';
	if (isset($_GET['tim_kiem'])) {
		$tim_kiem = $_GET['tim_kiem'];
	}
	?>

	<div class="search-container">
		<form action="ket_qua_tim_kiem.php">
			<input type="search" placeholder="Tìm tất cả sản phẩm..." name="tim_kiem" value="<?php echo($tim_kiem) ?>">
			<button type="submit"><i class="fa fa-search"></i></button>
		</form>
	</div>

</div>
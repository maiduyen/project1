<?php session_start() ?>
<?php include "title_chung.php" ?>
<?php include "include/header.php" ?>
<?php include "thong_bao.php" ?>
	<?php include 'connect.php' ?>
	<?php 
	if (!empty($_SESSION['gio_hang'])) {
		$thu_muc_anh = 'image/product/'; 
		$sum_price = 0;
		$count=0;
		?>
		<div class="gio_hang_chi_tiet">
		<div class="gio_hang">
			<table border="1px" width="100%" style="border-collapse: collapse;">
				<tr>
					<th class="tieu_de_gio_hang">Tên sản phẩm</th>
					<th class="tieu_de_gio_hang">Ảnh</th>
					<th class="tieu_de_gio_hang">Số lượng</th>

					
					<th class="tieu_de_gio_hang">Giá</th>
					<th class="tieu_de_gio_hang">Tổng</th>
					<th class="tieu_de_gio_hang">Xóa</th>
				</tr>
				<?php foreach ($_SESSION['gio_hang'] as $ma_item => $number_item): $count++; ?>
					<?php 
					$sql = "select * from do_dung where ma = '$ma_item'";
					$result= mysqli_query($connect, $sql);
					$each = mysqli_fetch_array($result);
					?>
					<tr class="bang_gio_hang">
						<td>
							<?php echo $each['ten'] ?>
						</td>
						<td>
							<img height="200px" src="<?php echo $thu_muc_anh . $each['anh'] ?>">
						</td>
						<td>
							<a class="active" href="tang_giam_san_pham.php?ma=<?php echo $ma_item ?>&kieu=tru">
								-
							</a>
							<span class="active">
								<?php echo $number_item ?>
							</span>
							
							<a class="active" href="tang_giam_san_pham.php?ma=<?php echo $ma_item ?>&kieu=cong">
								+
							</a>
						</td>
						<td>
							<?php echo $each['gia'] ?>
						</td>
						<td>
							<?php echo number_format($each['gia'] * $number_item,0,",",".").' đ';
									$sum_price += $each['gia'] * $number_item;
							?>

						</td>
						<td>
							<a style="color:red" href="xoa_san_pham.php?ma=<?php echo $ma_item ?>" onclick="return confirm('Bạn muốn xóa chứ?')">
								Xóa
							</a>
						</td>
					</tr>
				<?php endforeach ?>
			</table>
			<h1 align="right" style="color: red;">
				Tổng tiền: 
				<?php 
				echo number_format($sum_price,0,",",".").' đ';?>
			</h1>

		</div>


		<?php if(isset($_SESSION['ma_kh'])) {?>
		<?php 
			$ma = $_SESSION['ma_kh'];
			$sql = "select*from khach_hang where ma = '$ma'";
			$result = mysqli_query($connect, $sql);
			$each = mysqli_fetch_array($result);
		 ?>
		 
		<div class="tong">
				<form action="xu_ly_dat_hang.php" method="post">
					<div class="tieu_de">
						<h3>Thông tin người nhận</h3>
					</div>
					<div class="form_dat_hang">
						<div class="input-fields">
							<label for="ten_nguoi_nhan">Tên người nhận:</label>
							<span class="error" id="error_ten_nguoi_nhan"></span>
							<input name="ten_nguoi_nhan" id="ten_nguoi_nhan" class="inputt" type="text" value="<?php echo $each['ten'] ?>">
							<label for="so_dien_thoai_nguoi_nhan">Số điện thoại người nhận:</label>
							<span class="error" id="error_so_dien_thoai_nguoi_nhan"></span>
							<input name="so_dien_thoai_nguoi_nhan" id="so_dien_thoai_nguoi_nhan" class="inputt" type="text" value="<?php echo $each['so_dien_thoai'] ?>">
							<label for="dia_chi_nguoi_nhan">Địa chỉ người nhận:</label>
							<span class="error" id="error_dia_chi_nguoi_nhan"></span>
							<input name="dia_chi_nguoi_nhan" id="dia_chi_nguoi_nhan" class="inputt" type="text" value="<?php echo $each['dia_chi'] ?>">
						</div>
						<div class="goi_y">
							Ghi chú: 
							<textarea name="ghi_chu" placeholder="Để lại lời nhắn cho chúng tôi"></textarea>
							<button class="dat_hang" onclick="return kiem_tra_dat_hang()">Đặt hàng</button>
						</div> 
					</div>

				</form>
			</div>

	<?php } else { ?>
		<div class="tong">
				<form action="xu_ly_dat_hang.php" method="post">
					<div class="tieu_de">
						<h3>Thông tin đặt hàng</h3>
					</div>
					<div class="form_dat_hang">
						<div class="input-fields">
							<label for="ten_nguoi_nhan">Tên người nhận:</label>
							<span class="error" id="error_ten_nguoi_nhan"></span>
							<input name="ten_nguoi_nhan" id="ten_nguoi_nhan" class="inputt" type="text" placeholder="Tên người nhận">
							<label for="so_dien_thoai_nguoi_nhan">Số điện thoại người nhận:</label>
							<span class="error" id="error_so_dien_thoai_nguoi_nhan"></span>
							<input name="so_dien_thoai_nguoi_nhan" id="so_dien_thoai_nguoi_nhan" class="inputt" type="text" placeholder="Số điện thoại người nhận">
							<label for="dia_chi_nguoi_nhan">Địa chỉ người nhận:</label>
							<span class="error" id="error_dia_chi_nguoi_nhan"></span>
							<input name="dia_chi_nguoi_nhan" id="dia_chi_nguoi_nhan" class="inputt" type="text" placeholder="Địa chỉ người nhận">
						</div>
						<div class="goi_y">
							Ghi chú:  
							<textarea name="ghi_chu" placeholder="Để lại lời nhắn cho chúng tôi"></textarea>
							<button class="dat_hang" onclick="return kiem_tra_dat_hang()">Đặt hàng</button>
						</div> 
					</div>

				</form>
				<p>
					<span>Hoặc bạn có thể </span>
					<button class="btn_dat_hang" onclick="register_new_customer()">Đăng ký</button>
					<span>để theo dõi đơn hàng.</span>
				</p>
			</div>
	<?php } ?>

		
		</div>
	<?php } else { ?>
		<h1>Giỏ hàng trống, về
			<a style="color: #e500b2;" href="index.php">
				trang chủ
			</a>
			để tiếp tục mua hàng.
		</h1>
	<?php } ?>
	<script>
		function kiem_tra_dat_hang() {
  var kiem_tra_loi = false;
  //Tên người nhận
  var ten_nguoi_nhan = document.getElementById('ten_nguoi_nhan').value;
  var ten_nguoi_nhan_regex = /^[A-Za-z\d\sáàảãạăâẩẳắằấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
  if(ten_nguoi_nhan_regex.test(ten_nguoi_nhan)){
    document.getElementById('error_ten_nguoi_nhan').innerHTML = '';
  }
  else{
    document.getElementById('error_ten_nguoi_nhan').innerHTML = 'Vui lòng nhập tên hợp lệ.';
    kiem_tra_loi = true;
  }
  //so_dien_thoai_nguoi_nhan
var so_dien_thoai_nguoi_nhan = document.getElementById('so_dien_thoai_nguoi_nhan').value;
var so_dien_thoai_nguoi_nhan_regex = /^\d*\.?\d+$/;
if(so_dien_thoai_nguoi_nhan_regex.test(so_dien_thoai_nguoi_nhan)){
  document.getElementById('error_so_dien_thoai_nguoi_nhan').innerHTML = '';
}
else{
  document.getElementById('error_so_dien_thoai_nguoi_nhan').innerHTML = 'Vui lòng nhập số điện thoại của bạn.';
  kiem_tra_loi = true;
}
//dia_chi_nguoi_nhan
var dia_chi_nguoi_nhan = document.getElementById('dia_chi_nguoi_nhan').value;
var dia_chi_nguoi_nhan_regex = /^[a-zA-Z\d,. -\sáàảãạăâẩắằấầặẵẫậéèẻ ẽẹếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
if(dia_chi_nguoi_nhan_regex.test(dia_chi_nguoi_nhan)){
  document.getElementById('error_dia_chi_nguoi_nhan').innerHTML = '';
}
else{
  document.getElementById('error_dia_chi_nguoi_nhan').innerHTML = 'Vui lòng nhập vào địa chỉ hợp lệ.';
  kiem_tra_loi = true;
}

if(kiem_tra_loi==true){
  return false;
}
}
	</script>
	<?php mysqli_close($connect) ?>
<?php include "include/footer.php" ?>
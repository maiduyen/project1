<?php session_start(); ?>
<?php if (isset($_SESSION['ma_kh'])) { ?>
	
<?php include "title_chung.php" ?>
<?php include "include/header.php" ?>
<?php include "include/back-to-top.php" ?>
	<?php 
	$ma_khach_hang=$_SESSION['ma_kh'];

	include 'connect.php';

	$sql="select * from hoa_don where hoa_don.ma_khach_hang='$ma_khach_hang' order by thoi_gian_mua desc";

	$result = mysqli_query($connect, $sql);
	$count = mysqli_num_rows($result);
	$so_hoa_don_1_trang = 8;
	//tính số trang
	$tong_so_trang = ceil($count / $so_hoa_don_1_trang);
	$trang_hien_tai = 1;
	if(isset($_GET['trang'])){
		$trang_hien_tai = $_GET['trang'];
	}
	$so_hoa_don_bo_qua = ($trang_hien_tai - 1)* $so_hoa_don_1_trang;
	$sql = "$sql limit $so_hoa_don_1_trang offset $so_hoa_don_bo_qua";
	$result = mysqli_query($connect,$sql);
	?>
	<?php if ($count > 0) { ?>
		
	<table class="bang_hoa_don" style="border: 1px solid #d63bb3; border-collapse: collapse; width: 90%;margin: 0 auto;margin-top: 15px;">
		<tr  class="tieu_de_hoa_don">
			<th rowspan="2">Thời gian</th>
			<th rowspan="2">Tình trạng</th>
			<th colspan="3">Thông tin người nhận</th>
			<th rowspan="2">Ghi chú</th>
			<th rowspan="2">Xem</th>
		</tr>
		<tr  class="tieu_de_hoa_don">
			<td>Tên người nhận</td>
			<td>SĐT người nhận</td>
			<td>Địa chỉ người nhận</td>
		</tr>
		<?php foreach ($result as $each): ?>
			<tr>
				<tr rowspan="2">
					<td class="hoa_don_khach_hang">
						<?php echo date_format(date_create($each['thoi_gian_mua']),'Y-m-d H:i:s') ?>
					</td>
					<td class="hoa_don_khach_hang">
						<?php
						$trang_thai = $each['trang_thai'] ;
						if($trang_thai==1){
							echo "Đang chờ xử lý";
						}else if($trang_thai==2){
							echo "Đã duyệt";
						}else{
							echo "Đã hủy";
						}
						?>
					</td>
					<td class="hoa_don_khach_hang">
						<?php echo $each['ten_nguoi_nhan']; ?>
					</td>
					<td class="hoa_don_khach_hang">
						<?php echo $each['so_dien_thoai_nguoi_nhan']; ?>
					</td>
					<td class="hoa_don_khach_hang">
						<?php echo $each['dia_chi_nguoi_nhan']; ?>
					</td>
					<td class="hoa_don_khach_hang">
						<?php echo $each['ghi_chu'] ?>
					</td>
					 <td class="hoa_don_khach_hang">
					 	<a href="xem_hoa_don_chi_tiet.php?ma=<?php echo $each['ma'] ?>">
					 		Xem
					 	</a>
					 </td>
				</tr>
			</tr>
		<?php endforeach ?>
	</table>
	<p align="center" class="so_trang">
		<?php for($i = 1; $i <=$tong_so_trang; $i++){ ?>
			<a class="<?php echo(($trang_hien_tai == $i)?'active':'' )?>" href="?trang=<?php echo $i ?>#div_san_pham"><?php echo $i ?></a>
		<?php } ?>
	</p>				
<?php } else { ?>
	<h1>Bạn chưa mua gì cả :(</h1>

<?php } ?>

	<?php include "include/footer.php" ?>
<?php } else {
	header("location:index.php?error_login=Bạn phải đăng nhập để tiếp tục.");
	// header("location:404page.php");
}
<?php session_start(); ?>
<?php include 'title_chung.php' ?>
<?php include 'include/header.php' ?>
<?php if (isset($_GET['ma'])) { ?>
	<?php 
	$ma = $_GET['ma'];
	$thu_muc_anh = 'image/product/';

	include 'connect.php';

	$sql = "SELECT do_dung.*, loai_do_dung.ten as ten_loai_do_dung from do_dung inner join loai_do_dung on do_dung.ma_loai_do_dung = loai_do_dung.ma where do_dung.ma = '$ma'";
	$result = mysqli_query($connect,$sql);
	$each = mysqli_fetch_array($result);
	$count = mysqli_num_rows($result);
	?>
	<?php if ($count == 0) { ?>
		<div class="chi_tiet_san_pham">
		<h1>
			Sản phẩm này đang được cập nhật...
			<br>
			<a href="index.php" style="color: red;cursor: pointer;">Quay lại trang chủ</a>
		</h1>
		</div>
	<?php } else{ ?>
		<div class="chi_tiet_san_pham">

			
			<div class="noi_dung_san_pham">
				<h1 class="ten_chi_tiet">
					<?php echo $each['ten'] ?>
				</h1>
				<p class="gia_chi_tiet">
					<?php echo number_format($each['gia'],0,",",".").' đ'?>
				</p>
				<p class="loai_chi_tiet">
					Phân loại: 
					<?php echo ($each['ten_loai_do_dung']) ?>
				</p>
				<br>
				<a class="dat_hang" href="them_vao_gio_hang.php?ma=<?php echo $each['ma'] ?>">
					Thêm vào giỏ hàng
				</a>
			</div>
			<div class="anh_san_pham">
				<input type="checkbox" id="zoomCheck">
				<label for="zoomCheck">
					<img src="<?php echo $thu_muc_anh . $each['anh'] ?>">
				</label>
			</div>
			
			
			<div class="mo_ta_chi_tiet">
				<?php echo $each['mo_ta'] ?>
			</div>
		</div>
	<?php } ?>
	
<?php } else{ ?>
	<div class="chi_tiet_san_pham">
		<h1>
			Sản phẩm này đang được cập nhật...
			<br>
			<a href="index.php" style="color: red;cursor: pointer;">Quay lại trang chủ</a>
		</h1>
	</div>
<?php } ?>
<?php mysqli_close($connect) ?>
<?php include 'include/footer.php' ?>
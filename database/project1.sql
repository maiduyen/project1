-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 25, 2021 lúc 09:35 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bkacad`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` varchar(5) CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai` varchar(12) NOT NULL,
  `email` varchar(150) CHARACTER SET utf8 NOT NULL,
  `dia_chi` text NOT NULL,
  `can_cuoc_cong_dan` varchar(12) NOT NULL,
  `mat_khau` varchar(150) CHARACTER SET utf8 NOT NULL,
  `cap_do` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`ma`, `ten`, `ngay_sinh`, `gioi_tinh`, `so_dien_thoai`, `email`, `dia_chi`, `can_cuoc_cong_dan`, `mat_khau`, `cap_do`) VALUES
(1, 'Đức CT', '1996-07-02', 'Nam', '0968845131', 'ductran0207.bkhn@gmail.com', 'Thanh Hóa', '174656898', '123', 1),
(4, 'CHỤP ẢNH', '1996-07-02', 'Nam', '0968845131', 'ductran@gmail.com', 'Thanh Hóa', '123456', '123', 0),
(5, 'Đức CT', '1996-07-02', 'Nam', '0968845131', 'ductv0207@gmail.com', 'Thanh Hóa', '123', '123', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `background`
--

CREATE TABLE `background` (
  `ma` int(11) NOT NULL,
  `anh` varchar(150) CHARACTER SET utf8 NOT NULL,
  `loai_anh` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `background`
--

INSERT INTO `background` (`ma`, `anh`, `loai_anh`) VALUES
(4, '1611173252.jpg', 1),
(5, '1611339366.jpg', 1),
(12, '1611334062.jpg', 0),
(13, '1611339391.jpg', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `do_dung`
--

CREATE TABLE `do_dung` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ma_loai_do_dung` int(11) NOT NULL,
  `gia` float NOT NULL,
  `mo_ta` text CHARACTER SET utf8 NOT NULL,
  `anh` varchar(150) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `do_dung`
--

INSERT INTO `do_dung` (`ma`, `ten`, `ma_loai_do_dung`, `gia`, `mo_ta`, `anh`) VALUES
(1, 'Bút bi cà rốt', 5, 19000, 'Bút bi dành cho học sinh - sinh viên & văn phòng', 'but_bi.jpg'),
(2, 'Bút bi màu', 5, 19000, 'Bút bi dành cho học sinh - sinh viên & văn phòng', 'but_bi_3.jpg'),
(3, 'Để học tốt toán 9', 8, 19000, 'Sách dành cho học sinh - sinh viên & văn phòng', 'tham_khao_1.png'),
(4, 'Vở kẻ ngang campus', 7, 19000, 'Vở Campus 200 Trang B5\r\n\r\n- Bìa vở trẻ trung, bắt mắt và là thiết kế độc quyền của Campus\r\n\r\n- Được làm từ chất liệu giấy ngoại nhập chất lượng cao, bề mặt giấy trơn láng, viết đẹp hơn, mượt mà hơn\r\n\r\n- Giấy viết không bị lem, ăn mực hầu hết các loại bút, giấy viết không nhòe, không thấm mực ra trang sau\r\n\r\n- Vở Campus được trang bị hệ thống đánh dấu bằng số thông minh cùng dòng kẻ (ly ngang 2mm) in chính xác, rõ nét trên trang giấy, giúp việc học tập dễ dàng và thuận tiện hơn.\r\n\r\n- Số trang: 200 trang\r\n\r\n- Gáy vở được đóng theo công nghệ ép keo đa lớp của Nhật Bản, giúp vở luôn mở phẳng đẹp trên bàn học, dễ dàng lật và viết từ trang đầu tiên đến trang cuối cùng. Gáy vở vuông đẹp, không bị bong ra trong quá trình sử dụng', 'vo_campus.jpg'),
(5, 'Cặp tài liệu', 9, 19000, 'Cặp dành cho học sinh - sinh viên & văn phòng', 'cap_tai_lieu_2.jpg'),
(6, 'Cặp tài liệu', 9, 19000, 'Cặp dành cho học sinh - sinh viên & văn phòng', 'cap_tai_lieu_3.jpg'),
(7, 'Bút xóa', 12, 19000, 'Bút xóa khô idol BTS cartoon happy together', 'but_xoa.jpg'),
(8, 'Bút xóa', 12, 19000, '<p>X&oacute;a d&agrave;nh cho học sinh - sinh vi&ecirc;n &amp; văn ph&ograve;ng</p>\r\nBút xóa khô squishy Khủng long má hồng cute', 'but_xoa_2.jpg '),
(13, 'Balo thời trang nam nữ ', 19, 290000, '<p>Balo nữ gi&aacute; rẻ đẹp đi học du lịch c&aacute; t&iacute;nh dễ thương LOT STORE BL124 Balo laptop du lịch đi học mini nữ đẹp BALO LAPTOP ĐẸP &hearts;️ K&iacute;ch thước: 30cm* 13cm* 40cm &hearts;️ Chất liệu: Vải canvas cao cấp &hearts;️ M&agrave;u sắc c&oacute; 3 m&agrave;u : Đen, xanh dương, xanh l&aacute; &hearts;️ Style: Korea BALO NỮ kh&ocirc;ng những đi học m&agrave; c&ograve;n đi chơi, đi du lịch được H&atilde;y chọn cho m&igrave;nh 1 m&agrave;u ph&ugrave; hợp nh&eacute;... Trong thế giới thời trang của ph&aacute;i đẹp BALO DU LỊCH lu&ocirc;n chiếm một vị tr&iacute; quan trọng. Từ những c&ocirc; n&agrave;ng b&igrave;nh thường nhất cho tới những ng&ocirc;i sao h&agrave;ng đầu, tất cả đều chia sẻ một t&igrave;nh y&ecirc;u vĩ đại với những chiếc balo Chiếc BALO ĐI HỌC hợp d&aacute;ng người, hợp m&agrave;u sắc l&agrave;m tăng vẻ đẹp của trang phục bạn mặc v&agrave; khẳng định ấn tượng của bạn trong mắt người đối diện. Tuy nhi&ecirc;n, kh&ocirc;ng phải ai cũng biết chọn một chiếc balo thực sự ph&ugrave; hợp với phom cơ thể của m&igrave;nh. Mang tới cho c&aacute;c c&ocirc; n&agrave;ng sự thoải m&aacute;i khi đi dạo phố hoặc hẹn h&ograve; b&egrave; bạn v&igrave; kh&ocirc;ng phải cầm mang những vật dụng linh tinh, balo đ&atilde; trở th&agrave;nh người bạn kh&ocirc;ng thể thiếu c&aacute;c n&agrave;ng. Ch&uacute;ng c&oacute; sự đa dạng từ kiểu c&aacute;ch tới m&agrave;u sắc, size&hellip;t&ugrave;y theo nhu cầu của m&igrave;nh m&agrave; c&aacute;c n&agrave;ng lựa chọn một sản phẩm th&iacute;ch hợp. V&agrave; nếu bạn cũng đang đi t&igrave;m một balo thể thể hiện được c&aacute; t&iacute;nh của bản th&acirc;n một c&aacute;ch r&otilde; n&eacute;t nhất v&agrave; đang... lạc lối, th&igrave; h&atilde;y c&ugrave;ng kh&aacute;m ph&aacute; v&agrave; cảm nh&acirc;̣n những n&eacute;t đẹp v&agrave; quy&ecirc;́n rũ của</p>\r\n', 'balo_cute.jpg '),
(14, 'Tẩy ', 12, 20000, 'Cục tẩy gôm Cute animal ice cream', 'tay_gom.jpg'),
(15, 'Tẩy gôm', 12, 15000, 'Cục tẩy gôm Phía sau animal', 'tay_gom_3.jpg'),
(16, 'Tẩy gôm campus', 12, 15000, 'Tẩy Campus Colour chính là người bạn tuyệt vời nhất dành cho các bạn học sinh luôn khát khao sự hoàn hảo. Dù bạn có vẽ đồ thị sai lệch hay một bức tranh cần chỉnh sửa thì tẩy Campus chính là người bạn đồng hành tuyệt vời. Tẩy rất sạch. Mình cam kết nhé. ', 'tay_gom_1.jpg'),
(17, 'Tẩy gôm đen', 12, 15000, 'Sản phẩm được làm bằng chất liệu Hypolyme cao cấp, sử dụng cho việc bôi xóa trên giấy.\r\n- Gôm/tẩy dẻo dai, ít bụi trong quá trình bôi xóa, không làm rách giấy, không chứa hóa chất độc hại, an toàn cho trẻ em.\r\n\r\n CHÚNG TÔI CAM KẾT:\r\n\r\n✔ Hàng chính hiệu 100%\r\n✔ Hình đúng như mô tả 100%\r\n✔ Hoàn tiền 200% Nếu phát hiện hàng giả\r\n✔ Tốc độ xử lí hàng siêu nhanh trong 4H\r\n', 'tay_gom_3.jpg'),
(18, 'Bảng trắng học sinh', 11, 30000, 'Bảng trắng học sinh, nền trắn viền xanh\r\nBảng được làm bằng chất liệu nhựa mỏng, nhẹ, dễ dàng sử dụng, thích hợp cho việc học tập theo nhóm. Sản phẩm có thể sử dụng được cả 2 mặt, một mặt dùng bút lông, một mặt dùng phấn rất tiện lợi. Đặc biệt có thể cuộn tròn sản phẩm mang theo hoặc cất trữ rất tiện lợi, tiết kiệm tối đa không gian sử dụng.\r\nThông tin sản phẩm\r\n+ Mặt bảng có in dòng rõ ràng, thiết kế dạng ngang giúp học sinh dễ dàng viết chữ, thuận tiện cho việc học tập theo nhóm.\r\n+ Cấu tạo 2 mặt thông minh tiện dụng: mặt trước màu trắng, phủ bóng dùng cho bút lông; mặt sau màu đen phủ mờ dùng phấn để viết.\r\n+ Mặt bảng có đặc tính không bám dính, dễ dàng vệ sinh lau xóa sau khi viết.\r\n+ Bảng có thiết kế nhỏ gọn, đặc biệt có thể cuộn tròn mang theo khi cần thiết. Ngoài ra còn có dây treo bảng lên cao để trình bày một cách dễ dàng.\r\n+ Chất liệu nhựa an toàn, bền bỉ, không dễ bị nứt, đồng thời an toàn cho sức khỏe người tiêu dùng.', 'bang_xanh_duong.jpg'),
(19, 'Bảng đen học sinh', 11, 37000, 'Kích thước: Khổ A4, 1 mặt viết phấn, 1 mặt viết lông bảng, trên bảng có kẹp thêm 01 bút lông bảng có sẵn đồ xóa chuyên dành cho học sinh.\r\n - Mặt viết phấn mịn và bám phấn giúp viết rõ nét, chữ viết đẹp.\r\n - Mặt viết lông bảng màu trắng, mặt trơn dễ viết, dễ xóa', 'bang.jpg'),
(20, 'Bảng đen học sinh ', 11, 37000, '<p>K&iacute;ch thước:</p>\r\n\r\n<p>Khổ A4, 1 mặt viết phấn, 1 mặt viết l&ocirc;ng bảng, tr&ecirc;n bảng c&oacute; kẹp th&ecirc;m 01 b&uacute;t l&ocirc;ng bảng c&oacute; sẵn đồ x&oacute;a chuy&ecirc;n d&agrave;nh cho học sinh.</p>\r\n\r\n<p>- Mặt viết phấn mịn v&agrave; b&aacute;m phấn gi&uacute;p viết r&otilde; n&eacute;t, chữ viết đẹp.</p>\r\n\r\n<p>- Mặt viết l&ocirc;ng bảng m&agrave;u trắng, mặt trơn dễ viết, dễ x&oacute;a</p>\r\n', 'bang_2.jpg '),
(21, 'Vở kẻ ngang campus', 7, 10000, 'Vở campus là thương hiệu hàng đầu sử dụng công nghệ nhật bản \r\nChất lượng quốc tế mà giá cả Việt nam \r\nVở với độ trắng đạt tiêu chuẩn ISO ', 'vo_campus_2.jpg'),
(22, 'Vở kẻ ngang campus', 7, 10000, 'Vở Campus được trang bị hệ thống đánh dấu bằng số thông minh cùng dòng kẻ in chính xác, rõ nét trên trang giấy giúp việc học tập dễ dàng và thuận tiện hơn.\r\nVở Campus sử dụng giấy ngoại nhập chất lượng cao, bề mặt giấy trơn láng, viết đẹp hơn, mượt hơn. Các bìa vở Campus được thiết kế trẻ trung, bắt mắt và là thiết kế độc quyền của Campus. ', 'vo_campus_3.jpg'),
(23, 'Vở kẻ ngang campus', 7, 10000, 'Phong cách Vintage là phong cách của kỉ niệm, của dấu ấn thời gian, được thể hiện qua những đồ dùng \"cổ và cũ\" sử dụng trong các lĩnh vực: thời trang, nội thất, nhiếp ảnh, hội họa… Trào lưu Vintage đang được các bạn trẻ đặc biệt yêu thích nay đã được \"thổi hồn\" trên những cuốn vở 4 ly ngang gần gũi và quen thuộc với các bạn học sinh mỗi ngày.\r\n   Mỗi trang tập được in ngày tháng năm và ô đánh dấu những trang quan trọng giúp các bạn thuận tiện hơn trong việc học và công việc ', 'vo_campus_4.jpg'),
(24, 'Vở kẻ ngang campus ', 7, 10000, '<p>Phong c&aacute;ch Vintage l&agrave; phong c&aacute;ch của kỉ niệm, của dấu ấn thời gian, được thể hiện qua những đồ d&ugrave;ng &quot;cổ v&agrave; cũ&quot; sử dụng trong c&aacute;c lĩnh vực: thời trang, nội thất, nhiếp ảnh, hội họa&hellip;</p>\r\n\r\n<p>Tr&agrave;o lưu Vintage đang được c&aacute;c bạn trẻ đặc biệt y&ecirc;u th&iacute;ch nay đ&atilde; được &quot;thổi hồn&quot; tr&ecirc;n những cuốn vở 4 ly ngang gần gũi v&agrave; quen thuộc với c&aacute;c bạn học sinh mỗi ng&agrave;y. Mỗi trang tập được in ng&agrave;y th&aacute;ng năm v&agrave; &ocirc; đ&aacute;nh dấu những trang quan trọng gi&uacute;p c&aacute;c bạn thuận tiện hơn trong việc học v&agrave; c&ocirc;ng việc</p>\r\n', 'vo_campus_5.jpg '),
(25, 'Vở kẻ ngang campus', 7, 10000, 'Vở enjoy Campus.Bìa vở trẻ trung, bắt mắt và là thiết kế độc quyền của Campus.Gáy vở được đóng theo công nghệ ép keo đa lớp của Nhật Bản, giúp vở luôn mở phẳng đẹp trên bàn học, dễ dàng lật và viết từ trang đầu tiên đến trang cuối cùng. Gáy vở vuông đẹp, không bị bong ra trong quá trình sử dụng.Dòng kẻ in chính xác, rõ nét trên trang giấy, giúp việc học tập dễ dàng và thuận tiện hơn.\r\nShop cam kết vở campus bên shop là Vở chính hãng campus các b nhé \r\nCampus dùng là thích và mê luôn ạ ^^', 'vo_campus_6.jpg'),
(26, 'Vở kẻ ngang campus', 7, 10000, 'Vở Campus được trang bị hệ thống đánh dấu bằng số thông minh cùng dòng kẻ in chính xác, rõ nét trên trang giấy giúp việc học tập dễ dàng và thuận tiện hơn.\r\nVở Campus sử dụng giấy ngoại nhập chất lượng cao, bề mặt giấy trơn láng, viết đẹp hơn, mượt hơn. Các bìa vở Campus được thiết kế trẻ trung, bắt mắt và là thiết kế độc quyền của Campus. ', 'vo_campus_7.jpg'),
(27, 'Vở kẻ ngang campus ', 9, 10000, '<p>L&agrave;m từ giấy c&oacute; độ trắng tự nhi&ecirc;n.</p>\r\n\r\n<p>Chống l&oacute;a mắt, mỏi mắt khi đọc viết.</p>\r\n\r\n<p>Chống c&aacute;c hiện tượng suy giảm thị lực.</p>\r\n\r\n<p>Đặc biệt chất liệu giấy mịn v&agrave; d&agrave;y gi&uacute;p viết dễ d&agrave;ng v&agrave; kh&ocirc;ng bị in mực ra trang sau.</p>\r\n\r\n<p>Thiết kế b&igrave;a tươi s&aacute;ng v&agrave; bắt mắt sẽ gi&uacute;p c&aacute;c em dễ d&agrave;ng nhận biết c&aacute;c m&ocirc;n học, đồng thời hứng th&uacute; hơn khi ghi ch&eacute;p.</p>\r\n\r\n<p>Sản phẩm đạt ti&ecirc;u chuẩn h&agrave;ng Việt Nam chất lượng cao v&agrave; sản phẩm được ưa chuộng do người ti&ecirc;u d&ugrave;ng b&igrave;nh chọn, l&agrave; lựa chọn ho&agrave;n hảo cho c&aacute;c bậc phụ huynh v&agrave; học sinh.</p>\r\n', 'vo_campus_8.jpg '),
(28, 'Bút bi ', 5, 10000, '<p>B&Uacute;T BI MỰC XANH &Aacute;NH S&Aacute;NG MS B33 ĐẦU BI CẢM ƠN MỌI NGƯỜI Đ&Atilde; MUA H&Agrave;NG ỦNG HỘ SHOP NGƯỜI N&Ocirc;NG D&Acirc;N.</p>\r\n\r\n<p>B&Uacute;T BI MỰC XANH</p>\r\n\r\n<p>B&uacute;t Đặc điểm: Đầu bi:</p>\r\n\r\n<p>B&uacute;t bi dạng bấm c&ograve;, viết &ecirc;m tay, kh&ocirc;ng trơn tuột</p>\r\n', 'but_bi_1.jpg '),
(29, 'Bút đỏ', 5, 10000, '<p>TH&Ocirc;NG TIN CHI TIẾT:</p>\r\n\r\n<p>&ndash; B&uacute;t đỏ&nbsp;viết cực trơn, mực ra đều v&agrave; li&ecirc;n tục</p>\r\n\r\n<p>&ndash; Thiết kế ng&ograve;i b&uacute;t 0,5mm với 3 m&agrave;u mực: xanh, đen, đỏ</p>\r\n\r\n<p>&ndash; C&oacute; nắp đậy để bảo quản b&uacute;t tốt hơn, cho thời gian sử dụng l&acirc;u d&agrave;i hơn</p>\r\n\r\n<p>LƯU &Yacute;:</p>\r\n\r\n<p>&ndash; Hạn chế kh&ocirc;ng để b&uacute;t bị rơi xuống đất dễ g&acirc;y hỏng ng&ograve;i</p>\r\n\r\n<p>&ndash; Khi sử dụng xong phải đậy nắp b&uacute;t, kh&ocirc;ng n&ecirc;n để b&uacute;t tiếp x&uacute;c với kh&ocirc;ng kh&iacute; qu&aacute; l&acirc;u m&agrave; kh&ocirc;ng viết, sẽ l&agrave;m cho mực kh&ocirc; v&agrave; dễ tắc nghẽn v&agrave;o lần sử dụng sau.</p>\r\n', 'but_bi_do.jpg '),
(30, 'Bút bi', 5, 10000, '<p>Kiểu d&aacute;ng đẹp với lớp vỏ sơn nh&aacute;m, th&acirc;n bằng nhựa gi&uacute;p trọng lượng b&uacute;t gọn nhẹ</p>\r\n\r\n<p>C&aacute;c chi tiết của b&uacute;t được mạ ni-ken</p>\r\n\r\n<p>Cơ chế bấm n&uacute;t viết tiện lợi Sở hữu Sheaffer White Dot&reg;, biểu tượng thương hiệu của trải nghiệm viết xuất sắc</p>\r\n\r\n<p>Được trưng b&agrave;y trong hộp qu&agrave; tặng</p>\r\n\r\n<p>Bảo h&agrave;nh một năm c&oacute; giới hạn</p>\r\n', 'but_bi_2.jpg '),
(31, 'Bút màu', 20, 10000, '<p>Kiểu d&aacute;ng đẹp với lớp vỏ sơn nh&aacute;m, th&acirc;n bằng nhựa gi&uacute;p trọng lượng b&uacute;t gọn nhẹ C&aacute;c chi tiết của b&uacute;t được mạ ni-ken Cơ chế bấm n&uacute;t viết tiện lợi Sở hữu Sheaffer White Dot&reg;, biểu tượng thương hiệu của trải nghiệm viết xuất sắc Được trưng b&agrave;y trong hộp qu&agrave; tặng Bảo h&agrave;nh một năm c&oacute; giới hạn</p>\r\n', 'but_bi_3.jpg '),
(32, 'Bút bi', 5, 10000, '<p>Kiểu d&aacute;ng đẹp vỏ nh&aacute;m, th&acirc;n bằng nhựa gi&uacute;p trọng lượng b&uacute;t gọn nhẹ</p>\r\n\r\n<p>C&aacute;c chi tiết của b&uacute;t được mạ ni-ken</p>\r\n\r\n<p>Cơ chế bấm n&uacute;t viết tiện lợi Sở hữu Sheaffer White Dot&reg;, biểu tượng thương hiệu của trải nghiệm viết xuất sắc</p>\r\n\r\n<p>Được trưng b&agrave;y trong hộp qu&agrave; tặng</p>\r\n\r\n<p>Bảo h&agrave;nh một năm c&oacute; giới hạn</p>\r\n\r\n<p>Shop sẽ giao sản phẩm ngẫu nhi&ecirc;n, kh&aacute;ch h&agrave;ng vui l&ograve;ng th&ocirc;ng cảm cho shop ạ!!!</p>\r\n', 'but_bi_4.jpg '),
(33, 'Bút bi mực đen', 5, 10000, '<p>TH&Ocirc;NG TIN CHI TIẾT:</p>\r\n\r\n<p>&ndash; B&uacute;t bi viết cực trơn, mực ra đều v&agrave; li&ecirc;n tục</p>\r\n\r\n<p>&ndash; Chiều d&agrave;i th&acirc;n b&uacute;t: 15cm</p>\r\n\r\n<p>&ndash; C&oacute; nắp đậy để bảo quản b&uacute;t tốt hơn, cho thời gian sử dụng l&acirc;u d&agrave;i hơn</p>\r\n\r\n<p>LƯU &Yacute;:</p>\r\n\r\n<p>&ndash; Hạn chế kh&ocirc;ng để b&uacute;t bị rơi xuống đất dễ g&acirc;y hỏng ng&ograve;i</p>\r\n\r\n<p>&ndash; Khi sử dụng xong phải đậy nắp b&uacute;t, kh&ocirc;ng n&ecirc;n để b&uacute;t tiếp x&uacute;c với kh&ocirc;ng kh&iacute; qu&aacute; l&acirc;u m&agrave; kh&ocirc;ng viết, sẽ l&agrave;m cho mực kh&ocirc; v&agrave; dễ tắc nghẽn v&agrave;o lần sử dụng sau.</p>\r\n', 'but_bi_5.jpg '),
(34, 'Bút máy', 6, 30000, 'Bút máy viết nét hoa siêu đẹp siêu xịn', 'but_may.jpg'),
(35, 'Bút máy ', 6, 30000, '<p>B&uacute;t m&aacute;y cao cấp</p>\r\n\r\n<p>B&uacute;t m&aacute;y thư ph&aacute;p ng&ograve;i cong</p>\r\n\r\n<p>B&uacute;t bi gel&nbsp;</p>\r\n\r\n<p>B&uacute;t k&iacute; t&ecirc;n Hero 530.</p>\r\n\r\n<p>B&uacute;t m&aacute;y hero 530 l&agrave; d&ograve;ng b&uacute;t m&aacute;y thương hiệu nội địa Trung Quốc nổi tiếng tr&ecirc;n thế giới với hơn 30 năm sản xuất v&agrave; chế tạo ra những chiếc b&uacute;t m&aacute;y h&agrave;ng đầu thế giới - Th&acirc;n b&uacute;t l&agrave;m từ kim loại mạ v&agrave;ng, chống gỉ, kiểu d&aacute;ng giả gỗ độc đ&aacute;o</p>\r\n', 'but_may_2.jpg '),
(36, 'Bút máy ', 6, 30000, '<p>Đặc trưng:</p>\r\n\r\n<p>1. Th&acirc;n b&uacute;t nhựa cao cấp, nhẹ v&agrave; cầm tay.</p>\r\n\r\n<p>2. Cảm gi&aacute;c thoải m&aacute;i v&agrave; tinh tế, cho ph&eacute;p bạn tiếp tục viết trong một thời gian d&agrave;i.</p>\r\n\r\n<p>3. Đầu 0.38 mm mang lại cho bạn trải nghiệm viết trơn tru, dễ d&agrave;ng.</p>\r\n\r\n<p>4. Chữ viết trơn tru v&agrave; dễ d&agrave;ng chảy ra khỏi b&uacute;t m&agrave; kh&ocirc;ng l&agrave;m đổ mực.</p>\r\n\r\n<p>5. Xoay mực để sử dụng dễ d&agrave;ng hơn m&agrave; kh&ocirc;ng l&agrave;m ướt tay.</p>\r\n\r\n<p>6. Một loạt c&aacute;c m&agrave;u gradient cho bạn cảm gi&aacute;c viết kh&aacute;c nhau.</p>\r\n\r\n<p>7. Đ&acirc;y l&agrave; một m&oacute;n qu&agrave; tốt cho sinh vi&ecirc;n, gia đ&igrave;nh, bạn b&egrave;, v.v.</p>\r\n\r\n<p>8. Chỉ c&oacute; b&uacute;t m&aacute;y, c&aacute;c phụ kiện kh&aacute;c trong h&igrave;nh kh&ocirc;ng được bao gồm!</p>\r\n', 'but_may_3.jpg '),
(38, 'Mực in canon ', 10, 21000, '<p>Mực đổ RPT 1300 Mực bột In đậm ,tương th&iacute;ch cao với mọi d&ograve;ng m&aacute;y in laser canon/Hp, &iacute;t thải . Gi&uacute;p tăng tuổi thọ cho hộp mưc v&agrave; m&aacute;y in của bạn Sử dụng cho c&aacute;c d&ograve;ng m&aacute;y Hp 1320/401D/2035/5200/706N/Canon 2900/251DW/252D</p>\r\n', '1611339489.jpg'),
(39, 'Mực in màu Hàn Quốc', 10, 95000, 'Mực in Inktec Hàn Quốc Mực in Inktec Hàn Quốclà hệ mực dạng nước, đây là dòng sản phẩm mực in chuyên nghiệp, chuyên được dùng trong việc in ấn các ấn phẩm quảng cáo, in ảnh thẻ, in hồ sơ, tài liệu, các ấn phẩm văn phòng,…. Với công nghệ sản xuất cao cùng tính năng phân tán các hạt mực đều trên bản in nênmực in Inktec Hàn Quốchay còn gọi làmực in liên tụcthường xuyên được lựa chọn sử dụng và trở nên phổ biến rộng rãi trên thị trường  Đặc điểmmực in Inktec Hàn Quốc Chữ inktec được in nổi trên thân chai Đủ 6 màu : vàng, đỏ, xanh , đen Tính năng củamực in Inktec Hàn Quốc Dòng mực in chuyên dụng, chuyên dùng trong lĩnh vực in ảnh chuyên nghiệp Kết cấu hạt mực siêu mịn, mưc xuống đều, không gây hại đầu phun trong quá trình sử dụng, tăng tuổi thọmáy in Bản in chất lượng cao, gần như tương đồng so với mực chính hãng nhưng giá thấp hơn Dòng mực in chuyên dùng chomáy in phun, tương thích tốt vớimáy in phun4 màu hoặc 6 màu, đặc biệt làmáy in Epson Chất lượng màu sắc bản in đẹp, hài hòa, tính thẩm mĩ cao Ưu điểmmực in Inktec Hàn Quốc Hạt mực nhỏ, không gây hại đầu phun Giá thành thấp, dễ mua ở bất kì đâu Mực thấm nhanh vào vật phẩm in trong quá trình thao tác Cho bản in sắc nét, màu sắc ổn định Độ bền cao In được số lượng lớn Nhược điểmmực in Inktec Hàn Quốc Dễ bị nhòe màu khi gặp nước , nên sử dụng thêm các loại màng ép để giữ được bản in lâu hơn Khả năng kháng nước thấp Không in được trên các vật phẩm có màu tối Không in được trên vải Bảng thông số chi tiết mực in chuyển nhiệt inktec: Tên mực in	Mực in Inktec Hàn Quốc	Kiểu	Hệ mực nước Công nghệ in	In chuyển nhiệt	Màu	Đen, Xanh, Vàng, Đỏ Giấy sử dụng	Giấy in chuyển nhiệt	Thể tích	1 lít/chai Hệ thống mực	Mực in liên tục	Ứng dụng	in tài liệu, in ảnh thẻ, in ảnh hàn quốc,... Xuất xứ	Cập nhật	Bảo quản	Nơi khô thoáng từ 15 - 35 độ C Ứng dụngmực in Inktec Hàn Quốc In ấn các loại tài liệu In ảnh thẻ In ảnh chụp hình hàn quốc In các ấn phẩm thiết kế quảng cáo In màu', 'muc_in_2.jpg'),
(40, 'Mực in', 10, 91000, 'Mô tả chung: Dùng để đổ vào trực tiếp vào hộp mực (Mực trong hộp mực chính hãng hết, đổ thêm mực bằng cách bơm trực tiếp vào hộp mực đã hết) hoặc đổ vào bộ tiếp mực ngoài của máy. Mực chất lượng cao bản in sắc nét tương đương với mực in chính hãng cho chúng ta hình ảnh sống động và màu sắc thật nhất.', 'muc_in_3.jpg'),
(41, 'Kĩ năng giải toán', 8, 139000, 'Những kĩ năng giải toán đỉnh cao', 'sach_tham_khao.jpg'),
(42, 'Đề ôn toán lớp 6', 8, 139000, 'Những đề thi học sinh giải toán hay nhất trong những năm gần đây lớp 6', 'sach_tham_khao_2.jpg'),
(43, 'Đề ôn toán lớp 7', 8, 139000, 'Những bộ đề thi học sinh giải toán hay nhất trong những năm gần đây lớp 7', 'sach_tham_khao_3.jpg'),
(44, 'Đề ôn toán lớp 12', 8, 139000, 'Những bộ đề thi học sinh giải toán hay nhất trong những năm gần đây lớp 12, làm chủ toán trong vòng 30 ngày', 'sach_tham_khao_4.jpg'),
(45, 'Dạng toán lớp 11', 8, 139000, 'Các dạng toán hình điển hình, hay nhất lớp 11', 'sach_tham_khao_5.jpg'),
(46, 'Cặp tài liệu màu trắng', 9, 10000, 'Cặp tài liệu màu trắng đẽ nhìn để được nhiều đồ, tiện dụng', 'cap_tai_lieu_4.jpg'),
(47, 'Cặp tài liệu màu trắng', 9, 10000, 'Cặp tài liệu màu trắng đẽ nhìn để được nhiều đồ, tiện dụng', 'cap_tai_lieu_5.jpg'),
(48, 'Cặp tài liệu', 9, 10000, 'Cặp tài liệu nhựa, tiện dụng', 'cap_tai_lieu.jpg'),
(49, 'Cặp tài liệu ', 9, 10000, '<p>Cặp t&agrave;i liệu nhựa, tiện dụng</p>\r\n\r\n<p>cute</p>\r\n', 'cap_tai_lieu_6.jpg '),
(50, 'Cặp tài liệu ', 9, 10000, '<p>Cặp t&agrave;i liệu nhựa, tiện dụng</p>\r\n\r\n<p>dễ d&ugrave;ng, v&agrave; rất dễ thương cute nha</p>\r\n', 'cap_tai_lieu_7.jpg '),
(51, 'Bút bi ', 5, 9000, '<p>&nbsp;</p>\r\n\r\n<p>Giới thiệu b&uacute;t bi Pilot Frixion ball</p>\r\n\r\n<p>---------------------------------------------</p>\r\n\r\n<p>Pilot Frixion ball l&agrave; d&ograve;ng b&uacute;t bi đặc biệt cho ph&eacute;p bạn tẩy x&oacute;a v&agrave; chỉnh sửa những g&igrave; đ&atilde; viết một c&aacute;ch dễ d&agrave;ng bằng cục tẩy ph&iacute;a đu&ocirc;i b&uacute;t y hệt như b&uacute;t ch&igrave;.</p>\r\n\r\n<p>Với c&ocirc;ng nghệ thermo-sensitive độc quyền của h&atilde;ng Pilot ( c&ocirc;ng nghệ mực nhạy nhiệt). Đầu tẩy của b&uacute;t FriXion sử dụng chất liệu đặc biệt, kh&ocirc;ng bị m&ograve;n v&agrave; đổi mầu trong qu&aacute; tr&igrave;nh sử dụng.</p>\r\n\r\n<p>Khi cọ x&aacute;t, cục tẩy khiến bề mặt giấy bị l&agrave;m n&oacute;ng rất nhanh m&agrave; kh&ocirc;ng hề bị tổn hại.</p>\r\n\r\n<p>Mực bị l&agrave;m n&oacute;ng l&ecirc;n đến 65&deg;C sẽ trở n&ecirc;n trong suốt. B&uacute;t bi Frixion ball thực sự tiện lợi với gi&aacute;o vi&ecirc;n, học sinh, sinh vi&ecirc;n, nh&acirc;n vi&ecirc;n văn ph&ograve;ng, những người thường xuy&ecirc;n đọc s&aacute;ch, viết t&agrave;i liệu.</p>\r\n\r\n<p>Ch&uacute; &yacute;</p>\r\n\r\n<p>----------</p>\r\n\r\n<p>Kh&ocirc;ng d&ugrave;ng để k&yacute; t&ecirc;n hoặc viết trong những văn bản quan trọng, c&oacute; gi&aacute; trị về mặt ph&aacute;p l&yacute;. Mực c&oacute; thể bị nhạt dần nếu để gần những nguồn nhiệt lớn như dưới &aacute;nh nắng mặt trời hoặc bếp hoặc chạy qua m&aacute;y photocopy nhiều lần.</p>\r\n\r\n<p>Nếu ph&aacute;t hiện mực bị nhạt hoặc mất m&agrave;u, bạn chỉ cần bỏ trang giấy đ&oacute; v&agrave;o tủ lạnh trong 1 thời gian ngắn, mực sẽ l&ecirc;n m&agrave;u trở lại m&agrave; kh&ocirc;ng l&agrave;m mất những g&igrave; bạn đ&atilde; viết.</p>\r\n', '1611325220.jpg '),
(52, 'Balo học sinh   ', 19, 157000, '<p>Balo ULZZANG laptop du lịch đi học mini nữ đẹp</p>\r\n\r\n<p>BALO ULZZANG nữ&nbsp;</p>\r\n\r\n<p>&hearts;️ Chất liệu: Oxford</p>\r\n\r\n<p>&hearts;️ Style: Korea BALO ULZZANG NỮ</p>\r\n\r\n<p>kh&ocirc;ng những đi học m&agrave; c&ograve;n đi chơi, đi du lịch được H&atilde;y chọn cho m&igrave;nh 1 m&agrave;u ph&ugrave; hợp nh&eacute;...</p>\r\n\r\n<p>Trong thế giới thời trang của ph&aacute;i đẹp BALO DU LỊCH lu&ocirc;n chiếm một vị tr&iacute; quan trọn Từ những c&ocirc; n&agrave;ng b&igrave;nh thường nhất cho tới những ng&ocirc;i sao h&agrave;ng đầu, tất cả đều chia sẻ một t&igrave;nh y&ecirc;u vĩ đại với những chiếc balo Chiếc BALO ĐI HỌC hợp d&aacute;ng người, hợp m&agrave;u sắc l&agrave;m tăng vẻ đẹp của trang phục bạn mặc v&agrave; khẳng định ấn tượng của bạn trong mắt người đối diện.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, kh&ocirc;ng phải ai cũng biết chọn một chiếc balo thực sự ph&ugrave; hợp với phom cơ thể của m&igrave;nh.</p>\r\n\r\n<p>Mang tới cho c&aacute;c c&ocirc; n&agrave;ng sự thoải m&aacute;i khi đi dạo phố hoặc hẹn h&ograve; b&egrave; bạn v&igrave; kh&ocirc;ng phải cầm mang những vật dụng linh tinh, balo đ&atilde; trở th&agrave;nh người bạn kh&ocirc;ng thể thiếu c&aacute;c n&agrave;ng. Ch&uacute;ng c&oacute; sự đa dạng từ kiểu c&aacute;ch tới m&agrave;u sắc, size&hellip;t&ugrave;y theo nhu cầu của m&igrave;nh m&agrave; c&aacute;c n&agrave;ng lựa chọn một sản phẩm th&iacute;ch hợp. V&agrave; nếu bạn cũng đang đi t&igrave;m một balo thể thể hiện được c&aacute; t&iacute;nh của bản th&acirc;n một c&aacute;ch r&otilde; n&eacute;t nhất v&agrave; đang... lạc lối, th&igrave; h&atilde;y c&ugrave;ng kh&aacute;m ph&aacute; v&agrave; cảm nh&acirc;̣n những n&eacute;t đẹp v&agrave; quy&ecirc;́n rũ của ?</p>\r\n\r\n<p>Lu&ocirc;n l&agrave; nơi cập nhật những xu hướng balo đẹp BALO BI STORE đ&atilde; khẳng định vị tr&iacute; của m&igrave;nh với nhiều bạn trẻ bởi phong c&aacute;ch thời trang sang trọng, thanh lịch nhưng kh&ocirc;ng thiếu sự năng động v&agrave; c&aacute; t&iacute;nh. Kh&eacute;o l&eacute;o kết hợp trang phục c&ugrave;ng phụ kiện, bạn dễ d&agrave;ng mang lại một set đồ h&agrave;i h&ograve;a, thể hiện được c&aacute; t&iacute;nh ri&ecirc;ng của bạn</p>\r\n\r\n<p>HƯỚNG DẪN BẢO QUẢN: Kh&ocirc;ng để sản balo đi học tr&ecirc;n s&agrave;n nh&agrave; Tr&aacute;nh đặt balo vải l&ecirc;n s&agrave;n nh&agrave; để sản phẩm kh&ocirc;ng bị giảm chất lượng.Tốt nhất, bạn n&ecirc;n để sản phẩm trong ngăn tủ kh&ocirc; r&aacute;o.</p>\r\n\r\n<p>Ch&uacute; &yacute; đến thời tiết</p>\r\n\r\n<p>- Tr&aacute;nh đeo sản phẩm khi trời mưa hoặc thời tiết xấu để ch&uacute;ng kh&ocirc;ng bị ướt dẫn đến hỏng.</p>\r\n\r\n<p>- Cất giữ sản phẩm ở nơi tho&aacute;ng m&aacute;t để giữ g&igrave;n chất lượng của sản phẩm ở mức tốt nhất. Thường xuy&ecirc;n lau ch&ugrave;i balo - Lau ch&ugrave;i ngay bằng loại x&agrave; ph&ograve;ng chuy&ecirc;n d&ugrave;ng cho chất liệu balo nữ bị vấy bẩn.</p>\r\n\r\n<p>- Lau ch&ugrave;i balo thường xuy&ecirc;n để tr&aacute;nh bụi. Rửa tay trước khi đeo sản balo đẹp- N&ecirc;n rửa tay thật sạch trước khi đeo balo laptop.</p>\r\n\r\n<p>- Mồ h&ocirc;i v&agrave; chất nhờn từ b&agrave;n tay c&oacute; thể l&agrave;m hỏngbalo v&agrave; l&agrave;m cho phần tay cầm bị sậm m&agrave;u.</p>\r\n\r\n<p>- Xịt dung dịch bảo vệ balo du lịch Trước khi sử dụng balo nữ, bạn n&ecirc;n xịt dung dịch chuy&ecirc;n bảo vệ chất liệu vải l&ecirc;n to&agrave;n bộ bề mặt của sản phẩm, để qua đ&ecirc;m cho kh&ocirc; r&aacute;o. C&oacute; thể mua sản phẩm n&agrave;y ở ngay cửa h&agrave;ng b&aacute;n sản phẩm da hoặc ở tiệm thuốc.</p>\r\n\r\n<p>BALO BI STORE - Thương hiệu Việt Nam được được giới trẻ ưa chuộng v&agrave; biết đến, chuy&ecirc;n cung cấp c&aacute;c d&ograve;ng sản phẩm phụ kiện thời trang. Kh&ocirc;ng ngừng ph&aacute;t triển v&agrave; ho&agrave;n thiện,BALO BI STORE lu&ocirc;n mong muốn đem đến cho</p>\r\n', '1611339517.jpg'),
(53, 'Balo học sinh  ', 19, 210000, '<p>Thời gian để thị trường: 2017 m&ugrave;a thu v&agrave; m&ugrave;a đ&ocirc;ng</p>\r\n\r\n<p>K&iacute;ch thước: Trung b&igrave;nh</p>\r\n\r\n<p>Th&iacute;ch ứng với k&iacute;ch thước m&aacute;y t&iacute;nh: 11 inches</p>\r\n\r\n<p>C&oacute; đệm b&ocirc;ng sau: kh&ocirc;ng</p>\r\n\r\n<p>Th&iacute;ch hợp cho: tuổi trẻLoại k&ecirc;nh b&aacute;n h&agrave;ng: kinh doanh điện nguy&ecirc;n chất (chỉ b&aacute;n h&agrave;ng trực tuyến</p>\r\n\r\n<p>Kh&ocirc;ng thấm nước: Kh&ocirc;ng thấm nước</p>\r\n\r\n<p>Giới t&iacute;nh: NữKết cấu: Oxford quayChọn bộ phận loại: tay cầm mềmC&aacute;ch đ&oacute;ng: d&acirc;y k&eacute;o</p>\r\n\r\n<p>Cơ cấu nội bộ: d&acirc;y k&eacute;o t&uacute;i điện thoại t&uacute;i t&uacute;i t&agrave;i liệu t&uacute;i b&aacute;nh m&igrave; kẹp t&uacute;i m&aacute;y t&iacute;nh t&uacute;i</p>\r\n\r\n<p>T&uacute;i x&aacute;ch T&uacute;i b&ecirc;n ngo&agrave;i: T&uacute;i ba chiều</p>\r\n\r\n<p>Mẫu: M&agrave;u rắn</p>\r\n\r\n<p>Ph&acirc;n loại m&agrave;u: Đen lớn kaki xanh tuba tuba tuba hồng t&iacute;m tuba đỏ tuba đen trumpet kaki k&egrave;n trumpet m&agrave;u xanh l&aacute; c&acirc;y k&egrave;n m&agrave;u hồng t&iacute;m trumpet đỏ k&egrave;n đen tăng số Khaki XL Hồng cộng với k&iacute;ch thước</p>\r\n\r\n<p>C&oacute; một tầng lửng: c&oacute;</p>\r\n\r\n<p>T&uacute;i cứng: mềm</p>\r\n\r\n<p>C&oacute; thể gập lại được: C&oacute;</p>\r\n\r\n<p>Điều kiện: thương hiệu mới</p>\r\n\r\n<p>&Aacute;p dụng cảnh: giải tr&iacute;</p>\r\n\r\n<p>Nh&atilde;n hiệu: DupontizItem: 5657Kiểu d&aacute;ng: Nhật Bản v&agrave; H&agrave;n Quốc</p>\r\n\r\n<p>H&igrave;nh dạng: h&igrave;nh vu&ocirc;ng đứngVai đeo phong c&aacute;ch: đ&ocirc;i gốc</p>\r\n\r\n<p>Vật liệu l&oacute;t: polyester</p>\r\n', 'balo_cute.jpg  '),
(54, 'Balo da unisex three-box Chính Hãng   ', 19, 310000, '<p>&ndash; Chất liệu: Da Pu cao cấp , đường may chắc chắn, tỉ mỉ, c&oacute; ngăn chống sốc d&agrave;nh cho ipad</p>\r\n\r\n<p>&nbsp;Ph&ugrave; hợp với laptop: 13inch, 14inch</p>\r\n\r\n<p>&ndash; Chất liệu da Pu của Threebox chất lượng tốt, bền, đẹp, chống nước.</p>\r\n\r\n<p>_ Tuổi thọ của da c&oacute; thể l&ecirc;n đến 4 năm sử dụng kh&ocirc;ng nổ da</p>\r\n\r\n<p>Hướng dẫn bảo quản :</p>\r\n\r\n<p>+) Để tăng tuổi thọ da, sau khi đi mưa n&ecirc;n lau kh&ocirc; sản phẩm, để nơi kh&ocirc; tho&aacute;ng, n&ecirc;n giữ hạt chống ẩm trong t&uacute;i để trong trường hợp da bị độ ẩm cao dễ mốc da +) Kh&ocirc;ng n&ecirc;n để sản phẩm dưới &aacute;nh nắng trực tiếp qu&aacute; l&acirc;u sẽ l&agrave;m hư hại , nứt da.</p>\r\n\r\n<p>+) Lau sạch da định k&igrave; 3 th&aacute;ng/lần bằng kem lau t&uacute;i</p>\r\n', '1611339541.jpg'),
(55, 'Balo hello kitty  ', 19, 250000, '<p>- Balo hello kitty cho b&eacute;chất liệu nhẹ, kh&ocirc;ng thấm nước cho b&eacute; đi học.</p>\r\n\r\n<p>- Kiểu d&aacute;ng bắt mắt, ngộ nghĩnh gi&uacute;p b&eacute; th&iacute;ch th&uacute; hơn khi đi học.</p>\r\n\r\n<p>- Thiết kế vừa l&agrave; cặp s&aacute;ch đi học, vừa c&oacute; thể l&agrave;m balo đi du lịchcho c&aacute;c b&eacute;.</p>\r\n\r\n<p>- Balo cho b&eacute;chất liệu bền, nhẹ kh&ocirc;ng độc hại.</p>\r\n', 'balo_hello_kitty.jpg  '),
(56, 'Balo nam ', 19, 250000, '<p>Khi đặt h&agrave;ng c&aacute;c bạn nhớ bấm chọn voucher shopee để nhận m&atilde; FREESHIP.</p>\r\n\r\n<p>Balo HIGH basic là must-have item của các cô nàng, anh ch&agrave;ng hay lăn tăn. Đeo balo dù nhỏ-to đi đâu cũng thấy oke nè.</p>\r\n\r\n<p>Nhiều ngăn nhỏ - to đầy đủ để đựng đồ b&uacute;t, thước, s&aacute;ch vở ngăn nắp hơn</p>\r\n\r\n<p>❌ C&oacute; 2 size để c&aacute;c b&aacute;c dễ d&agrave;ng lựa chọn với sở th&iacute;ch ❌</p>\r\n\r\n<p>✔️Balo được l&agrave;m từ chất liệu vải tốt nhất thị trường, d&agrave;y v&agrave; chống nước 100% m&agrave; vẫn mềm mại thoải m&aacute;i cho người đeo</p>\r\n\r\n<p>✔️Balo mang phong c&aacute;ch trẻ trung, năng động v&agrave; c&aacute; t&iacute;nh.</p>\r\n\r\n<p>✔️Phụ kiện k&egrave;m theo cực kỳ dễ thương</p>\r\n', 'balo_nam.jpg   '),
(57, 'Balo vải học sinh ', 19, 250000, '<p>- Sản phẩm lấy mẫu từ Quảng Ch&acirc;u nhưng được XƯỞNG NH&Agrave; gia c&ocirc;ng,</p>\r\n\r\n<p>- Ch&uacute;ng t&ocirc;i chọn chất liệu vải d&agrave;y dặn chống nước 100%, kho&aacute; k&eacute;o thương hiệu YKK, c&aacute;c m&oacute;c kho&aacute; xịn, đường may tỉ mỉ v&agrave; chắc chắn, n&ecirc;n CHẤT LƯỢNG HO&Agrave;N TO&Agrave;N HƠN HẲN với những sp tương tự tr&ecirc;n thị trường bạn nh&eacute;.</p>\r\n\r\n<p>- Ch&uacute;ng t&ocirc;i muốn gửi đến qu&yacute; kh&aacute;ch h&agrave;ng những sp chất lượng nhất. (shop c&ograve;n nhiều mẫu xịn nữa bạn nh&eacute;!!)</p>\r\n\r\n<p>-&gt;&gt;H&atilde;y kết bạn Zal:O9O2-995-189 để cập nhật những mẫu mới nhất v&agrave; nhận những ưu đ&atilde;i từ shop ạ!!!</p>\r\n', 'balo_vai.jpg   '),
(58, 'Túi vải canvas    ', 19, 250000, '<p>&hearts;️ Chất liệu: vải</p>\r\n\r\n<p>&hearts;️ M&agrave;u sắc c&oacute; 2 m&agrave;u : đen,trắng</p>\r\n\r\n<p>&hearts;️ Style: Korea</p>\r\n\r\n<p>&hearts;️T&Uacute;I VẢI BỐ kh&ocirc;ng những l&agrave; m&oacute;n phụ kiện thời trang sang trọng l&agrave;m qu&agrave; tặng, đi chơi, dự tiệc m&agrave; c&ograve;n l&agrave; vật hộ mệnh cho đường tiền bạc của bạn. H&atilde;y chọn cho m&igrave;nh 1 m&agrave;u ph&ugrave; hợp nh&eacute;...</p>\r\n\r\n<p>&hearts;️T&Uacute;I VẢI TOTE hợp d&aacute;ng người, hợp m&agrave;u sắc l&agrave;m tăng vẻ đẹp của trang phục bạn mặc v&agrave; khẳng định ấn tượng của bạn trong mắt người đối diện.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, kh&ocirc;ng phải ai cũng biết chọn một chiếc T&Uacute;I VẢI CANVAS thực sự ph&ugrave; hợp với phom cơ thể của m&igrave;nh. V&agrave; nếu bạn cũng đang đi t&igrave;m một chiếc v&iacute; thể thể hiện được c&aacute; t&iacute;nh của bản th&acirc;n một c&aacute;ch r&otilde; n&eacute;t nhất v&agrave; đang... lạc lối, th&igrave; h&atilde;y c&ugrave;ng kh&aacute;m ph&aacute; v&agrave; cảm nh&acirc;̣n những n&eacute;t đẹp v&agrave; quy&ecirc;́n rũ của T&Uacute;I ĐEO CH&Eacute;O -T&Uacute;I TOTE - T&Uacute;I VẢI - D&ograve;ng Sản Phẩm HOT TREND 2020.</p>\r\n', 'balo_vai_2.jpg    '),
(59, 'Bút màu nước học sinh', 20, 19000, 'Có thể thoả sức vẽ, không lo bị bẩn \r\n12/18/24màu sắc để lựa chọn, chân thật và tươi sáng \r\nỐng bút trong suốt dễ nhìn, giúp cho việc sắp xếp dễ dàng hơn', 'but_mau.jpg'),
(60, 'Bộ bút màu cho bé BestKids ', 20, 600000, '<p>Bộ b&uacute;t m&agrave;u cho b&eacute; BestKids được trang bị bao gồm 54 dụng cụ đủ loại b&uacute;t m&agrave;u</p>\r\n\r\n<p><img alt=\"\" src=\"http://www.chiemtaimobile.vn/images/XIAOMI-upload2/xiaomi-khac/bo-but-mau-bestkids/bo-but-mau-cho-be-bestkids%20(14).jpg?1558691907606\" /></p>\r\n\r\n<p>Ph&ugrave; hợp với mọi bức tranh m&agrave; b&eacute; y&ecirc;u th&iacute;ch, đ&aacute;p ứng đầy đủ sự s&aacute;ng tạo kh&ocirc;ng giới hạn của b&eacute;.</p>\r\n\r\n<p>B&ecirc;n cạnh đ&oacute;, với sự kết hợp đầy đủ m&agrave;u sắc, dụng cụ như vậy c&ograve;n c&oacute; thể gi&uacute;p trẻ thiết lập được một hệ thống m&agrave;u sắc ho&agrave;n hảo trong giai đoạn ph&aacute;t triển đầu đời, th&uacute;c đẩy sự ph&aacute;t triển gi&aacute;o dục thẩm mỹ của trẻ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Chất liệu cao cấp, tạo m&agrave;u đẹp</h2>\r\n\r\n<p>Mỗi vật liệu l&agrave;m n&ecirc;n c&aacute;c loại m&agrave;u sắc trong bộ b&uacute;t m&agrave;u BestKids đều được chế tạo từ những nguy&ecirc;n liệu tạo m&agrave;u cao cấp.</p>\r\n\r\n<p>Ch&iacute;nh v&igrave; vậy m&agrave; m&agrave;u sắc được tạo ra kh&aacute; ch&acirc;n thực, đẹp mắt v&agrave; mềm mại. Hơn nữa, cấu tạo m&agrave;u c&ograve;n cho ph&eacute;p b&eacute; c&oacute; thể thỏa sức pha trộn, s&aacute;ng tạo m&agrave;u sắc độc đ&aacute;o ri&ecirc;ng.</p>\r\n\r\n<p>Đối với m&agrave;u nước, cha mẹ kh&ocirc;ng cần phải lo lắng khi b&eacute; v&acirc;y bẩn v&agrave;o quần &aacute;o, m&agrave;u c&oacute; thể dễ d&agrave;ng giặt sạch bằng phương ph&aacute;p th&ocirc;ng thường.</p>\r\n\r\n<p><img alt=\"\" src=\"http://www.chiemtaimobile.vn/images/XIAOMI-upload2/xiaomi-khac/bo-but-mau-bestkids/bo-but-mau-cho-be-bestkids%20(13).jpg?1558691892085\" /></p>\r\n', 'but_mau_2.jpg '),
(61, 'Bộ bút brush Future color ', 20, 109000, '<p>Bộ b&uacute;t brush Future color, 10 m&agrave;u</p>\r\n\r\n<p>Đầu cọ mềm dễ d&agrave;ng viết handlettering, hoặc chữ c&oacute; n&eacute;t thanh n&eacute;t đậm</p>\r\n\r\n<p><img alt=\"\" src=\"https://ae01.alicdn.com/kf/HTB1CPnzSIfpK1RjSZFOq6y6nFXaQ/10-Chi-c-Fabricolor-Vi-t-B-t-L-ng-M-u-Th-Ph-p-nh.jpg_q50.jpg\" style=\"height:1000px; width:1000px\" /></p>\r\n\r\n<p>M&agrave;u b&uacute;t tươi, mau kh&ocirc;, kh&ocirc;ng loang thấm</p>\r\n\r\n<p>Gi&aacute; th&agrave;nh rất hợp l&yacute;</p>\r\n\r\n<p><img alt=\"\" src=\"https://cf.shopee.vn/file/6ebbaf8bf878f49bb83e5ab1ce026e15\" /></p>\r\n', 'but_mau_3.jpg '),
(62, 'Túi vải canvas ', 18, 120000, '<p>✅Ch&agrave;o mừng bạn đến cửa h&agrave;ng của ch&uacute;ng t&ocirc;i ?C&aacute;c sản phẩm trong trung t&acirc;m thương mại n&agrave;y sẽ được kiểm tra cẩn thận trước khi vận chuyển, h&atilde;y y&ecirc;n t&acirc;m mua h&agrave;ng. ?Nếu bạn c&oacute; bất kỳ c&acirc;u hỏi n&agrave;o, bạn c&oacute; thể li&ecirc;n hệ dịch vụ chăm s&oacute;c kh&aacute;ch h&agrave;ng Th&ocirc;ng tin chi tiết: Chất liệu: vải Phong c&aacute;ch: t&uacute;i vu&ocirc;ng nhỏ Yếu tố phổ biến: in ấn phong c&aacute;ch: Nhật Bản v&agrave; H&agrave;n Quốc Loại kh&oacute;a: d&acirc;y k&eacute;o</p>\r\n', 'tui_vai.jpg '),
(63, 'Túi vải canvas  ', 18, 100000, '<p>✅Ch&agrave;o mừng bạn đến cửa h&agrave;ng của ch&uacute;ng t&ocirc;i</p>\r\n\r\n<p>T&uacute;i Vải Canvas Mềm Mại Phong C&aacute;ch Retro Nhật Bản Xinh Xắn</p>\r\n\r\n<p><img alt=\"\" src=\"https://cf.shopee.vn/file/aa7ecbf97eab4a8770e1ca77341f37ba\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://cf.shopee.vn/file/85228c4cae615e1ca5b99b62fa5947f4\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://cf.shopee.vn/file/64790c4527a40b4d345dd96ac658c478\" /></p>\r\n', 'tui_vai_2.jpg  '),
(64, 'Túi vải canvas  ', 18, 160000, '<p>T&uacute;i vải đeo ch&eacute;o nữ sinh vi&ecirc;n H&agrave;n Quốc phi&ecirc;n bản mới của Harajuku ulzzang Nhật Bản</p>\r\n\r\n<h3>Sản phẩm sẽ được gửi ngẫu nhi&ecirc;n qu&yacute; kh&aacute;ch h&agrave;ng vui l&ograve;ng th&ocirc;ng cảm cho shop ạ!!!</h3>\r\n\r\n<p><img alt=\"\" src=\"https://gd1.alicdn.com/imgextra/i1/2014279112/O1CN01buKkxX2HBLRNJiyu3_!!2014279112.jpg\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://gd2.alicdn.com/imgextra/i2/2014279112/O1CN01D7tgfJ2HBLRMveZzz_!!2014279112.jpg\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://img.alicdn.com/imgextra/i1/6000000006340/O1CN01g0wZJ31whlXzW6awH_!!6000000006340-0-tbvideo.jpg\" /></p>\r\n', 'tui_vai_3.jpg  '),
(65, 'Bút Bi Mực Xanh ', 5, 12000, '<p>C&acirc;y B&uacute;t Bi Flexoffice Fo-024 được sản xuất theo c&ocirc;ng nghệ mới.</p>\r\n\r\n<p>Mực b&uacute;t hệ dầu, đạt ti&ecirc;u chuẩn an to&agrave;n của Mỹ ASTM D-4236, ti&ecirc;u chuẩn Ch&acirc;u &Acirc;u EN 71/3.</p>\r\n\r\n<p>N&eacute;t viết trơn, &ecirc;m, mực ra đều v&agrave; li&ecirc;n tục.</p>\r\n\r\n<p>C&ocirc;ng nghệ Smooth writing ti&ecirc;n tiến, viết trơn, &ecirc;m, mực ra đều li&ecirc;n tục Đầu bi 0.7mm, viết trơn &ecirc;m, m&agrave;u mực đậm tươi, mực ra đều v&agrave; li&ecirc;n tục.</p>\r\n\r\n<p>Thiết kế đẹp, đơn giản, nhỏ gọn, rất chắc chắn, dễ cầm v&agrave; chắc tay.</p>\r\n\r\n<p>C&aacute;n b&uacute;t bằng nhựa trong suốt in trasfer film.</p>\r\n\r\n<p>Tảm v&agrave; dắt b&uacute;t c&ugrave;ng m&agrave;u mực.</p>\r\n\r\n<p>Dắt b&uacute;t được in PAD.</p>\r\n\r\n<p>Ống ruột m&agrave;u trắng đục.</p>\r\n\r\n<p>Định mức mực: 0.18 &plusmn;0.02g.</p>\r\n\r\n<p>Chiều d&agrave;i viết được &iacute;t nhất 1.000m.</p>\r\n\r\n<p><img alt=\"\" src=\"https://img.websosanh.vn/v2/users/root_product/images/but-bi-thien-long-fo-024/5xp6lv0sxm0o6.jpg?compress=85&amp;width=220\" style=\"height:220px; width:220px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://img.websosanh.vn/v2/users/wss/images/but-bi-tl-fo-024/rwmf81fbd4pq1.jpg?compress=85&amp;width=220\" style=\"height:220px; width:220px\" /></p>\r\n', 'but_bi_7.jpg '),
(66, 'Bút bi Thiên Long ', 5, 12000, '<p>C&acirc;y B&uacute;t Bi Flexoffice Fo-024 được sản xuất theo c&ocirc;ng nghệ mới.</p>\r\n\r\n<p>Mực b&uacute;t hệ dầu, đạt ti&ecirc;u chuẩn an to&agrave;n của Mỹ ASTM D-4236, ti&ecirc;u chuẩn Ch&acirc;u &Acirc;u EN 71/3.</p>\r\n\r\n<p>N&eacute;t viết trơn, &ecirc;m, mực ra đều v&agrave; li&ecirc;n tục.</p>\r\n\r\n<p>C&ocirc;ng nghệ Smooth writing ti&ecirc;n tiến, viết trơn, &ecirc;m, mực ra đều li&ecirc;n tục Đầu bi 0.7mm, viết trơn &ecirc;m, m&agrave;u mực đậm tươi, mực ra đều v&agrave; li&ecirc;n tục.</p>\r\n\r\n<p>Thiết kế đẹp, đơn giản, nhỏ gọn, rất chắc chắn, dễ cầm v&agrave; chắc tay.</p>\r\n\r\n<p>C&aacute;n b&uacute;t bằng nhựa trong suốt in trasfer film.</p>\r\n\r\n<p>Tảm v&agrave; dắt b&uacute;t c&ugrave;ng m&agrave;u mực.</p>\r\n\r\n<p>Dắt b&uacute;t được in PAD. Ống ruột m&agrave;u trắng đục. Định mức mực: 0.18 &plusmn;0.02g. Chiều d&agrave;i viết được &iacute;t nhất 1.000m.</p>\r\n\r\n<p><img alt=\"\" src=\"https://prooffice.vn/wp-content/uploads/2019/05/butduntl-093_grande-300x300.jpg\" style=\"height:300px; width:300px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://prooffice.vn/wp-content/uploads/2019/05/but_bi_thien_long_tl-93__25__large-300x300.jpg\" style=\"height:300px; width:300px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://prooffice.vn/wp-content/uploads/2019/05/but_bi_thien_long_tl-93__20__large-300x300.jpg\" style=\"height:300px; width:300px\" /></p>\r\n', '1611337528.png'),
(67, 'Bút bi thiên long', 5, 13000, '<p>B&uacute;t bi Thi&ecirc;n Long 031 do Tập đo&agrave;n Thi&ecirc;n Long sản xuất.</p>\r\n\r\n<p>Th&acirc;n b&uacute;t l&agrave;m từ nhựa cứng, giắt b&uacute;t bằng kim loại mạ crom s&aacute;ng b&oacute;ng, sang trọng.</p>\r\n\r\n<p>Với kiểu d&aacute;ng ph&ugrave; hợp cho đối tượng học sinh, nh&acirc;n vi&ecirc;n văn ph&ograve;ng, tiểu thương&hellip;</p>\r\n\r\n<p>Sản phẩm được chăm ch&uacute;t trong thiết kế v&agrave; tinh tế từng chi tiết.</p>\r\n\r\n<p>B&uacute;t c&oacute; thiết kế hiện đại, th&acirc;n nhỏ v&agrave; thon d&agrave;i, dễ cầm nắm khi viết.</p>\r\n', '1611337773.jpg'),
(68, 'Bút bi thiên long', 5, 7000, '<p>B&uacute;t bi thi&ecirc;n long chất lượng cao</p>\r\n', '1611337901.jpg'),
(69, 'Túi vải canvas cỡ lớn', 18, 129000, '<p>T&uacute;i Đeo Ch&eacute;o Canvas Cỡ Lớn Thời Trang H&agrave;n Quốc Học Sinh Sinh Vi&ecirc;n Đi Học , Đi Chơi</p>\r\n\r\n<p>TH&Ocirc;NG TIN SẢN PHẨM</p>\r\n\r\n<p>T&ecirc;n sản phẩm : T&uacute;i đeo ch&eacute;o canvas cỡ lớn thời trang h&agrave;n quốc học sinh sinh vi&ecirc;n đi học đi chơi</p>\r\n\r\n<p>Số ngăn: 2 ngăn lớn b&ecirc;n ngo&agrave;i</p>\r\n\r\n<p>Xuất xứ Quảng Ch&acirc;u n&ecirc;n h&agrave;ng si&ecirc;u đẹp nha mấy bạn !</p>\r\n\r\n<p>Chất liệu : Vải Canvas</p>\r\n\r\n<p>Mỗi l&ocirc; h&agrave;ng v&agrave; mỗi m&agrave;n h&igrave;nh hiển thị kh&aacute;c nhau m&agrave; m&agrave;u sắc c&oacute; thể kh&aacute;c nhau ch&uacute;t &iacute;t, ch&uacute;ng t&ocirc;i đề cao chất lượng của sản phẩm, mong muốn những KH của OpeStore nhận được những sản phẩm gi&aacute; hợp l&yacute; với chất lượng tốt nhất.</p>\r\n\r\n<p>------------------------------------</p>\r\n\r\n<p>Lu&ocirc;n l&agrave; nơi cập nhật những xu hướng T&Uacute;I TOTE mới nhất</p>\r\n\r\n<p>OPE đ&atilde; khẳng định vị tr&iacute; của m&igrave;nh với nhiều bạn trẻ bởi phong c&aacute;ch thời trang sang trọng, thanh lịch nhưng kh&ocirc;ng thiếu sự năng động v&agrave; c&aacute; t&iacute;nh. Kh&eacute;o l&eacute;o kết hợp trang phục c&ugrave;ng phụ kiện, bạn dễ d&agrave;ng mang lại một set đồ h&agrave;i h&ograve;a, thể hiện được c&aacute; t&iacute;nh ri&ecirc;ng của bạn</p>\r\n\r\n<p>HƯỚNG DẪN BẢO QUẢN:</p>\r\n\r\n<p>Sản phẩm vải Canvas n&ecirc;n để tr&aacute;nh xa lửa, kh&ocirc;ng sử dụng c&aacute;c ho&aacute; chất tẩy rửa mạnh, ch&uacute;ng sẽ l&agrave;m sợi vải trở n&ecirc;n yếu sau một thời gian.</p>\r\n\r\n<p>TH&Ocirc;NG TIN NH&Agrave; B&Aacute;N OPESTORE</p>\r\n\r\n<p>Gần 10 năm trong lĩnh vực nhập khẩu v&agrave; ph&acirc;n phối h&agrave;ng ho&aacute; phụ kiện thời trang, ch&uacute;ng t&ocirc;i tự tin về chất lượng v&agrave; gi&aacute; th&agrave;nh sản phẩm của m&igrave;nh.</p>\r\n\r\n<p>C&aacute;c bạn mua sắm v&agrave; sử dụng dịch vụ của Ope nếu c&oacute; những vấn đề ph&aacute;t sinh v&agrave; c&oacute; &yacute; định đ&aacute;nh gi&aacute; shop dưới 5* th&igrave; h&atilde;y nhắn tin với shop trước để được giải quyết nh&eacute;, Ope lu&ocirc;n lu&ocirc;n giải quyết tất cả mọi khiếu nại ch&iacute;nh đ&aacute;ng của kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p><img alt=\"\" src=\"https://salt.tikicdn.com/ts/tmp/f8/18/bb/962ce0ade8433eec84989a94b66c2b94.jpg\" style=\"height:750px; width:750px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://salt.tikicdn.com/ts/tmp/2a/e1/ca/8790a64aeeaf2d818879e727bf6bbf52.jpg\" style=\"height:750px; width:750px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://salt.tikicdn.com/ts/tmp/93/08/ff/64f47c7fd5f308d01fdbb0ad1bd918c8.jpg\" style=\"height:750px; width:750px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://salt.tikicdn.com/ts/tmp/72/b0/f6/aca88e84ba54e3dc0af3a91e360106e8.jpg\" style=\"height:750px; width:750px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://salt.tikicdn.com/ts/tmp/d9/56/ed/2e3d6b3ca2dc9e7eb6d57a3e0f027c78.jpg\" style=\"height:750px; width:750px\" /></p>\r\n', '1611338061.jpg'),
(70, 'Trong túi vải đeo chéo  ', 18, 169000, '<p><strong>Thương hiệu</strong>: No Brand</p>\r\n\r\n<p><strong>Địa điểm ứng dụng</strong>: LEISURE</p>\r\n\r\n<p><strong>Xu Hướng Nữ</strong>: H&agrave;n Quốc</p>\r\n\r\n<p><strong>Loại bảo h&agrave;nh</strong>: No Warranty</p>\r\n\r\n<p><strong>Material</strong>: Vải Canvas</p>\r\n\r\n<p><strong>Dạng t&uacute;i</strong>: T&uacute;i đeo vai</p>\r\n\r\n<p><strong>Mẫu/ Chi Tiết</strong>: Car&ocirc; kết hợp</p>\r\n\r\n<p><img alt=\"\" src=\"https://laz-img-sg.alicdn.com/p/96f679ab46d83c8472e332f7f00ea845.jpg\" style=\"height:800px; width:800px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://laz-img-sg.alicdn.com/p/a39e84ebcdde37bbc8c2106d6599ca86.jpg\" style=\"height:800px; width:800px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://laz-img-sg.alicdn.com/p/f07bbb35d1744692af7fdf182a3e80e5.jpg\" style=\"height:400px; width:400px\" /></p>\r\n', '1611338344.jpg ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don`
--

CREATE TABLE `hoa_don` (
  `ma` int(11) NOT NULL,
  `ma_khach_hang` int(11) DEFAULT NULL,
  `ten_nguoi_nhan` varchar(150) CHARACTER SET utf32 NOT NULL,
  `dia_chi_nguoi_nhan` text CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai_nguoi_nhan` varchar(12) NOT NULL,
  `trang_thai` int(5) NOT NULL,
  `thoi_gian_mua` datetime NOT NULL,
  `ghi_chu` text CHARACTER SET utf32 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `hoa_don`
--

INSERT INTO `hoa_don` (`ma`, `ma_khach_hang`, `ten_nguoi_nhan`, `dia_chi_nguoi_nhan`, `so_dien_thoai_nguoi_nhan`, `trang_thai`, `thoi_gian_mua`, `ghi_chu`) VALUES
(1, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 20:45:45', ''),
(2, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 20:50:45', ''),
(3, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 20:52:32', ''),
(5, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 22:37:22', ''),
(8, NULL, 'Đức', 'Thanh Hoa', '0968845131', 3, '2021-01-17 22:41:40', ''),
(9, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 2, '2021-01-18 05:46:06', ''),
(10, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 2, '2021-01-20 18:00:48', ''),
(11, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 2, '2021-01-20 18:01:08', ''),
(12, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-20 18:03:35', 'chào'),
(13, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-20 18:08:03', ''),
(14, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-20 18:14:14', ''),
(15, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 2, '2021-01-20 18:14:21', ''),
(16, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-21 01:34:49', ''),
(17, 3, 'Ductran', 'Ninh Bình', '0968845131', 3, '2021-01-21 02:19:22', ''),
(18, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 2, '2021-01-22 18:57:05', ''),
(19, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 3, '2021-01-22 19:04:09', ''),
(20, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 3, '2021-01-22 20:38:40', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don_chi_tiet`
--

CREATE TABLE `hoa_don_chi_tiet` (
  `ma_hoa_don` int(11) NOT NULL,
  `ma_do_dung` int(11) NOT NULL,
  `so_luong` int(11) NOT NULL,
  `gia` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `hoa_don_chi_tiet`
--

INSERT INTO `hoa_don_chi_tiet` (`ma_hoa_don`, `ma_do_dung`, `so_luong`, `gia`) VALUES
(2, 1, 2, 19000),
(2, 2, 1, 19000),
(3, 5, 1, 19000),
(3, 1, 1, 19000),
(5, 5, 1, 19000),
(5, 1, 1, 19000),
(5, 1, 1, 19000),
(8, 1, 1, 19000),
(9, 1, 1, 19000),
(10, 2, 1, 19000),
(10, 1, 1, 19000),
(11, 1, 2, 19000),
(12, 2, 1, 19000),
(13, 1, 1, 19000),
(14, 1, 1, 19000),
(15, 1, 1, 19000),
(16, 1, 1, 19000),
(17, 1, 1, 19000),
(18, 5, 2, 19000),
(19, 1, 1, 19000),
(20, 1, 2, 19000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khach_hang`
--

CREATE TABLE `khach_hang` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` varchar(5) CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai` varchar(12) CHARACTER SET utf8 NOT NULL,
  `email` varchar(150) NOT NULL,
  `mat_khau` varchar(150) CHARACTER SET utf8 NOT NULL,
  `dia_chi` text CHARACTER SET utf8 NOT NULL,
  `tinh_trang` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `khach_hang`
--

INSERT INTO `khach_hang` (`ma`, `ten`, `ngay_sinh`, `gioi_tinh`, `so_dien_thoai`, `email`, `mat_khau`, `dia_chi`, `tinh_trang`) VALUES
(1, 'TVĐ', '1996-07-02', 'Nam', '0968845131', 'ductran0207.bkhn@gmail.com', '123', 'Thanh Hóa', 0),
(3, 'Đức CT', '2001-06-05', 'Nam', '0968845131', 'maiduyen@gmail.com', '123', 'Ninh Bình', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lien_he`
--

CREATE TABLE `lien_he` (
  `ma` int(11) NOT NULL,
  `noi_dung` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `lien_he`
--

INSERT INTO `lien_he` (`ma`, `noi_dung`) VALUES
(1, '<h1>Giới thiệu</h1>\r\n\r\n<p>► Địa chỉ: số 17, Tạ Quang Bửu</p>\r\n\r\n<p>► Email: ductran0207.bkhn@gmail.com</p>\r\n\r\n<p>►Số điện thoại: 0999.999.999</p>\r\n'),
(2, '<h1>Li&ecirc;n kết</h1>\r\n\r\n<p>Một c&aacute;i g&igrave; đ&oacute;</p>\r\n\r\n<p>Rất hay ho</p>\r\n\r\n<p>V&agrave; n&agrave;y nọ</p>\r\n'),
(4, '<h1>B&ecirc;n phải</h1>\r\n\r\n<h1><span style=\"font-size:13px\">Cute h&ocirc;ng</span></h1>\r\n');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_do_dung`
--

CREATE TABLE `loai_do_dung` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf32 NOT NULL,
  `ma_loai_cha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `loai_do_dung`
--

INSERT INTO `loai_do_dung` (`ma`, `ten`, `ma_loai_cha`) VALUES
(1, 'Bút viết', NULL),
(2, 'Sách vở', NULL),
(3, 'Đồ dùng văn phòng', NULL),
(4, 'Đồ dùng khác', NULL),
(5, 'Bút bi', 1),
(6, 'Bút máy', 1),
(7, 'Vở kẻ ngang', 2),
(8, 'Sách tham khảo', 2),
(9, 'Cặp tài liệu', 3),
(10, 'Mực in', 3),
(11, 'Bảng học sinh', 4),
(12, 'Tẩy gôm', 4),
(17, 'Phụ kiện', NULL),
(18, 'Túi xách', 17),
(19, 'Cặp đeo', 17),
(20, 'Bút màu', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_ngang`
--

CREATE TABLE `menu_ngang` (
  `ma` int(11) NOT NULL,
  `ten` varchar(50) CHARACTER SET utf8 NOT NULL,
  `noi_dung` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `menu_ngang`
--

INSERT INTO `menu_ngang` (`ma`, `ten`, `noi_dung`) VALUES
(2, 'Trang chủ', ''),
(5, 'Sản phẩm', ''),
(8, 'Liên hệ', '<h1>Li&ecirc;n hệ ch&uacute;ng tớ nh&eacute;.</h1>\r\n'),
(9, 'Giới thiệu', '<h1>Giới thiệu</h1>\r\n\r\n<p>C&ocirc;ng ty&nbsp;Ba Nhất&nbsp;được th&agrave;nh lập bằng niềm đam m&ecirc; của tuổi trẻ nghị lực v&agrave; quyết t&acirc;m mang đến cho những kh&aacute;ch h&agrave;ng của m&igrave;nh những sản phẩm văn ph&ograve;ng phẩm gi&aacute; rẻ c&ugrave;ng với chất lượng được đảm bảo tốt nhất. &nbsp;C&oacute; thể n&oacute;i đ&oacute; l&agrave; mục ti&ecirc;u v&agrave; cũng l&agrave; kết quả đ&atilde; l&agrave;m được của văn ph&ograve;ng phẩm Ba Nhất.</p>\r\n\r\n<p>C&oacute; thể n&oacute;i, một trong những nguy&ecirc;n nh&acirc;n gi&uacute;p Văn ph&ograve;ng phẩm Ba Nhất lớn mạnh như hiện nay đ&oacute; ch&iacute;nh l&agrave; những hiểu biết chuy&ecirc;n s&acirc;u về m&ocirc;i trường văn ph&ograve;ng, c&ocirc;ng sở.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Xuất ph&aacute;t từ một nh&acirc;n vi&ecirc;n văn ph&ograve;ng - người s&aacute;ng lập ra Văn ph&ograve;ng phẩm Ba Nhất thấu hiểu mọi y&ecirc;u cầu v&agrave; mong muốn về c&aacute;c dụng cụ văn ph&ograve;ng phẩm tiện &iacute;ch v&agrave; chất lượng tốt. Những ai đ&atilde; từng l&agrave;m việc v&agrave; gắn b&oacute; với c&ocirc;ng việc b&agrave;n giấy mới thấu hiểu được rằng: Văn ph&ograve;ng phẩm đầy đủ v&agrave; chất lượng cao kh&ocirc;ng chỉ phục vụ tốt cho c&ocirc;ng việc m&agrave; c&ograve;n gi&uacute;p nh&acirc;n vi&ecirc;n văn ph&ograve;ng c&oacute; cảm gi&aacute;c thoải m&aacute;i, dễ chịu hơn đồng thời ho&agrave;n th&agrave;nh c&ocirc;ng việc của m&igrave;nh nhanh hơn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tuy nhi&ecirc;n, vấn đề đặt ra ngay sau chất lượng tốt ch&iacute;nh l&agrave; gi&aacute; th&agrave;nh sản phẩm. Điều hiển nhi&ecirc;n rằng, nếu c&oacute; được nguồn cung cấp gi&aacute; th&agrave;nh văn ph&ograve;ng phẩm gi&aacute; rẻ sẽ gi&uacute;p cho một c&ocirc;ng ty tiết kiệm kh&ocirc;ng &iacute;t t&agrave;i ch&iacute;nh. Thấu hiểu những mong muốn đ&oacute;, Văn ph&ograve;ng phẩm Ba Nhất đặt ra mục ti&ecirc;u cho m&igrave;nh phương ch&acirc;m, mục ti&ecirc;u kinh doanh l&agrave; :&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&ldquo;&nbsp;<strong>NHANH NHẤT &ndash; CHẤT LƯỢNG NHẤT &ndash; TIẾT KIỆM NHẤT</strong>&rdquo;.</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với triết l&yacute; kinh doanh n&agrave;y, Ba Nhất đ&atilde; trở th&agrave;nh địa chỉ cung ứng văn ph&ograve;ng phẩm online v&agrave; trực tiếp với gi&aacute; cả rẻ nhất c&ugrave;ng chất lượng tốt trong lĩnh vực văn ph&ograve;ng phẩm gi&aacute; rẻ tại TpHCM.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Đặc trưng những sản phẩm của Văn ph&ograve;ng phẩm Ba Nhất</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>C&ocirc;ng Ty Ba Nhất l&agrave; một trong những địa chỉ cung cấp văn ph&ograve;ng phẩm gi&aacute; rẻ tại TpHCM, với nhiều năm kinh doanh trong lĩnh vực văn ph&ograve;ng phẩm, sản phẩm của ch&uacute;ng t&ocirc;i lu&ocirc;n c&oacute; những &nbsp;y&ecirc;u cầu sau:&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Sản phẩm chất lượng nhất</p>\r\n	</li>\r\n	<li>\r\n	<p>Kiểu d&aacute;ng, chủng loại đa dạng nhất</p>\r\n	</li>\r\n	<li>\r\n	<p>B&aacute;o gi&aacute; văn ph&ograve;ng phẩm được cập nhật nhanh &ndash; ch&iacute;nh x&aacute;c nhất&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>Dịch vụ hậu m&atilde;i tuyệt vời nhất.</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2>Tại sao Văn ph&ograve;ng phẩm Ba Nhất l&agrave;m được điều đ&oacute;?</h2>\r\n\r\n<p>Chắc hẳn khi đọc những điều tr&ecirc;n qu&yacute; kh&aacute;ch sẽ thắc mắc liệu đ&oacute; c&oacute; phải l&agrave; sự thật hay chỉ l&agrave; những lời PR, quảng c&aacute;o khuếch trương t&ecirc;n tuổi như bao nhi&ecirc;u c&ocirc;ng ty văn ph&ograve;ng phẩm tại TPHCM .</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Để giải tỏa những băn khoăn đ&oacute; ch&uacute;ng t&ocirc;i xin chứng minh khả năng kinh doanh của ch&uacute;ng t&ocirc;i thay lời cam kết về chất lượng v&agrave; gi&aacute; th&agrave;nh văn ph&ograve;ng phẩm.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&ldquo;Chất lượng nhất&rdquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ch&uacute;ng t&ocirc;i chuy&ecirc;n cung cấp c&aacute;c mặt h&agrave;ng văn ph&ograve;ng phẩm được sản xuất tr&ecirc;n d&acirc;y chuyền c&ocirc;ng nghệ hiện đại của c&aacute;c nước h&agrave;ng đầu ch&acirc;u &Acirc;u như Đức, Mỹ v&agrave; ch&acirc;u &Aacute; như Nhật Bản, Ấn Độ... v&agrave; c&aacute;c sản phẩm chất lượng cao trong nước như: Thi&ecirc;n Long, Bến Ngh&eacute;, Hồng H&agrave;&hellip;Với những sản phẩm nhập khẩu ho&agrave;n to&agrave;n ch&uacute;ng t&ocirc;i cam kết c&oacute; giấy tờ chứng nhận xuất xứ, với những sản phẩm trong nước Văn ph&ograve;ng phẩm Ba Nhất cam kết nhập h&agrave;ng ch&iacute;nh h&atilde;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với những sản phẩm chất lượng tốt nhất trong nước v&agrave; quốc tế ch&uacute;ng t&ocirc;i tự h&agrave;o g&oacute;p phần gi&uacute;p bạn ho&agrave;n th&agrave;nh c&ocirc;ng việc một c&aacute;ch nhanh nhất v&agrave; hiệu quả nhất.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&ldquo;Tiết Kiệm Nhất&rdquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với c&aacute;c mặt h&agrave;ng văn ph&ograve;ng phẩm trong nước, Văn ph&ograve;ng phẩm Ba Nhất l&agrave; đại l&yacute; c&ograve;n với c&aacute;c sản phẩm nhập khẩu ch&uacute;ng l&agrave; nh&agrave; ph&acirc;n phối trực tiếp n&ecirc;n c&oacute; nhiều ch&iacute;nh s&aacute;ch ưu đ&atilde;i về gi&aacute;. Th&ecirc;m v&agrave;o đ&oacute;, với phương ch&acirc;m kinh doanh&nbsp;&ldquo; H&agrave;ng Chất Lượng &ndash; Gi&aacute; B&igrave;nh D&acirc;n&rdquo;&nbsp;C&ocirc;ng Ty Ba Nhất lu&ocirc;n mang đến cho kh&aacute;ch h&agrave;ng mức chiết khấu cao nhất, đảm bảo gi&aacute; tốt nhất đến mọi kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&ldquo;Nhanh nhất&rdquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Văn ph&ograve;ng phẩm Ba Nhất cam kết lu&ocirc;n c&oacute; lượng h&agrave;ng lớn, dự trữ đầy đủ c&aacute;c mặt h&agrave;ng văn ph&ograve;ng phẩm trong nước lẫn ngo&agrave;i nước đảm bảo cung ứng đầy đủ đơn h&agrave;ng lớn nhỏ của mọi kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ngo&agrave;i ra,&nbsp;<strong>Văn ph&ograve;ng phẩm Ba Nhất</strong>&nbsp;cũng c&oacute; đội ngũ giao h&agrave;ng cơ động, nhiệt t&igrave;nh đảm bảo giao h&agrave;ng nhanh, đ&uacute;ng địa chỉ ở mọi loại địa h&igrave;nh. Với những địa chỉ l&agrave; c&ocirc;ng ty gần ngay đường lớn, với số lượng đơn h&agrave;ng lớn ch&uacute;ng t&ocirc;i c&oacute; đội ngũ xe chở h&agrave;ng bằng &ocirc; t&ocirc;, với những đơn h&agrave;ng nhỏ ch&uacute;ng t&ocirc;i c&oacute; đội ngũ giao h&agrave;ng bằng xe m&aacute;y đảm bảo trả h&agrave;ng nhanh v&agrave; đ&uacute;ng địa chỉ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với h&igrave;nh thức thanh to&aacute;n phong ph&uacute;: chuyển khoản, thanh to&aacute;n trực tiếp, đặt cọc&hellip; Văn ph&ograve;ng phẩm Ba Nhất cam kết giao h&agrave;ng ngay trong ng&agrave;y sau khi nhận được đơn đặt h&agrave;ng của qu&yacute; kh&aacute;ch. Yếu tố n&agrave;y c&agrave;ng củng cố th&ecirc;m phương ch&acirc;m Nhanh nhất của ch&uacute;ng t&ocirc;i.</p>\r\n');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ma`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `background`
--
ALTER TABLE `background`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `do_dung`
--
ALTER TABLE `do_dung`
  ADD PRIMARY KEY (`ma`),
  ADD KEY `ma_loai_do_dung` (`ma_loai_do_dung`);

--
-- Chỉ mục cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`ma`),
  ADD KEY `ma_khach_hang` (`ma_khach_hang`);

--
-- Chỉ mục cho bảng `hoa_don_chi_tiet`
--
ALTER TABLE `hoa_don_chi_tiet`
  ADD KEY `ma_hoa_don` (`ma_hoa_don`),
  ADD KEY `ma_do_dung` (`ma_do_dung`);

--
-- Chỉ mục cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  ADD PRIMARY KEY (`ma`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `loai_do_dung`
--
ALTER TABLE `loai_do_dung`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `menu_ngang`
--
ALTER TABLE `menu_ngang`
  ADD PRIMARY KEY (`ma`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `background`
--
ALTER TABLE `background`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `do_dung`
--
ALTER TABLE `do_dung`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `loai_do_dung`
--
ALTER TABLE `loai_do_dung`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `menu_ngang`
--
ALTER TABLE `menu_ngang`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

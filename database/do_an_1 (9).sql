-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 26, 2021 lúc 06:32 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `do_an_1`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` varchar(5) CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai` varchar(12) NOT NULL,
  `email` varchar(150) CHARACTER SET utf8 NOT NULL,
  `dia_chi` text NOT NULL,
  `can_cuoc_cong_dan` varchar(12) NOT NULL,
  `mat_khau` varchar(150) CHARACTER SET utf8 NOT NULL,
  `cap_do` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`ma`, `ten`, `ngay_sinh`, `gioi_tinh`, `so_dien_thoai`, `email`, `dia_chi`, `can_cuoc_cong_dan`, `mat_khau`, `cap_do`) VALUES
(1, 'Đức CT', '1996-07-02', 'Nam', '0968845131', 'ductran0207.bkhn@gmail.com', 'Thanh Hóa', '174656898', '123', 1),
(4, 'CHỤP ẢNH', '1996-07-02', 'Nam', '0968845131', 'ductran@gmail.com', 'Thanh Hóa', '123456', '123', 0),
(5, 'Đức CT', '1996-07-02', 'Nam', '0968845131', 'ductv0207@gmail.com', 'Thanh Hóa', '123', '123', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `background`
--

CREATE TABLE `background` (
  `ma` int(11) NOT NULL,
  `anh` varchar(150) CHARACTER SET utf8 NOT NULL,
  `loai_anh` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `background`
--

INSERT INTO `background` (`ma`, `anh`, `loai_anh`) VALUES
(1, '1611173222.jpg', 1),
(3, '1611173244.jpeg', 1),
(4, '1611173252.jpg', 1),
(12, '1611330869.jpg', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `do_dung`
--

CREATE TABLE `do_dung` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ma_loai_do_dung` int(11) NOT NULL,
  `gia` float NOT NULL,
  `mo_ta` text CHARACTER SET utf8 NOT NULL,
  `anh` varchar(150) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `do_dung`
--

INSERT INTO `do_dung` (`ma`, `ten`, `ma_loai_do_dung`, `gia`, `mo_ta`, `anh`) VALUES
(1, 'Bút bi', 5, 19000, 'Bút bi dành cho học sinh - sinh viên & văn phòng', '1.jpg'),
(2, 'Bút bi 2', 6, 19000, 'Bút bi dành cho học sinh - sinh viên & văn phòng', '2.jpg'),
(3, 'Sách tham khảo', 7, 19000, 'Sách dành cho học sinh - sinh viên & văn phòng', '3.jpg'),
(4, 'Vở kẻ ngang 1', 8, 19000, 'Vở ghi dành cho học sinh - sinh viên & văn phòng', '4.jpg'),
(5, 'Cặp tài liệu', 9, 19000, 'Cặp dành cho học sinh - sinh viên & văn phòng', '5.jpg'),
(6, 'Cặp tài liệu 2', 10, 19000, 'Cặp dành cho học sinh - sinh viên & văn phòng', '6.jpg'),
(7, 'Bút xóa', 11, 19000, 'Xóa dành cho học sinh - sinh viên & văn phòng', '7.jpg'),
(8, 'Bút xóa  ', 11, 2100, '<p>X&oacute;a d&agrave;nh cho học sinh - sinh vi&ecirc;n &amp; văn ph&ograve;ng</p>\r\n', '1611323139.jpg'),
(11, 'Bút Thiên Long ', 5, 3900, '<p><strong>Văn Ph&ograve;ng Phẩm H&agrave; Nội</strong>&nbsp;được th&agrave;nh lập từ năm 2004 với t&ecirc;n gọi:&nbsp;<em><strong>C&ocirc;ng Ty TNHH TM v&agrave; DV Chuyển Ph&aacute;t Nhanh H&agrave; Nội</strong></em>, ng&agrave;nh nghề ch&iacute;nh l&agrave; chuyển ph&aacute;t nhanh v&agrave; văn ph&ograve;ng phẩm. Năm 2008 đổi t&ecirc;n th&agrave;nh C&ocirc;ng ty TNHH Văn ph&ograve;ng phẩm H&agrave; Nội với mục ti&ecirc;u đề ra l&agrave; kinh doanh c&aacute;c mặt h&agrave;ng văn ph&ograve;ng phẩm với dịch vụ cung cấp văn ph&ograve;ng phẩm trọn g&oacute;i đến tận tay người ti&ecirc;u d&ugrave;ng.</p>\r\n\r\n<p><strong>Văn Ph&ograve;ng phẩm H&agrave; Nội</strong>&nbsp;phẩn đấu trở th&agrave;nh đơn vị cung cấp văn ph&ograve;ng phẩm h&agrave;ng đầu tại H&agrave; Nội, c&aacute;c tỉnh ph&iacute;a Bắc v&agrave; tr&ecirc;n cả nước. Với gần 10 năm kinh nghiệm, trải qua qu&aacute; tr&igrave;nh nỗ lực kh&ocirc;ng ngừng, Văn ph&ograve;ng phẩm H&agrave; Nội đ&atilde; v&agrave; đang tr&ecirc;n đ&agrave; ph&aacute;t triển ng&agrave;y c&agrave;ng lớn mạnh trong lĩnh vực cung cấp văn ph&ograve;ng phẩm tại Việt Nam. Sản phẩm v&agrave; dịch vụ của ch&uacute;ng t&ocirc;i đ&atilde; v&agrave; đang tạo được uy t&iacute;n tr&ecirc;n thị trường Việt Nam đặc biệt l&agrave; tr&ecirc;n thị trường H&agrave; Nội.</p>\r\n\r\n<p><strong>Văn ph&ograve;ng phẩm H&agrave; Nội</strong>&nbsp;với đội ngũ nh&acirc;n vi&ecirc;n trẻ, năng động, s&aacute;ng tạo c&oacute; kinh nghiệm trong lĩnh vực Văn ph&ograve;ng phẩm c&ugrave;ng với t&aacute;c phong l&agrave;m việc chuy&ecirc;n nghiệp, ch&uacute;ng t&ocirc;i cam kết mang đến cho kh&aacute;ch h&agrave;ng kh&ocirc;ng chỉ những sản phẩm, dịch vụ về Văn ph&ograve;ng phẩm m&agrave; c&ograve;n mang đến cho Kh&aacute;ch h&agrave;ng sự tận t&acirc;m ch&iacute;nh bản th&acirc;n v&agrave; cả hiệu quả trong c&ocirc;ng việc.</p>\r\n\r\n<p><strong>Văn ph&ograve;ng phẩm H&agrave; Nội</strong>&nbsp;kh&ocirc;ng ngừng cải tiến sản phẩm dịch vụ để mang đến cho kh&aacute;ch h&agrave;ng những sản phẩm chất lượng cao gi&aacute; cả ph&ugrave; hợp. Văn ph&ograve;ng phẩm H&agrave; Nội thường xuy&ecirc;n n&acirc;ng cao kỹ năng b&aacute;n h&agrave;ng v&agrave; phục vụ kh&aacute;ch h&agrave;ng, tổ chức đ&agrave;o tạo đội ngũ b&aacute;n h&agrave;ng v&agrave; nh&acirc;n vi&ecirc;n giao h&agrave;ng với mục ti&ecirc;u mang đến cho kh&aacute;ch h&agrave;ng dịch vụ tốt nhất.</p>\r\n', '1611315858.png ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don`
--

CREATE TABLE `hoa_don` (
  `ma` int(11) NOT NULL,
  `ma_khach_hang` int(11) DEFAULT NULL,
  `ten_nguoi_nhan` varchar(150) CHARACTER SET utf32 NOT NULL,
  `dia_chi_nguoi_nhan` text CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai_nguoi_nhan` varchar(12) NOT NULL,
  `trang_thai` int(5) NOT NULL,
  `thoi_gian_mua` datetime NOT NULL,
  `ghi_chu` text CHARACTER SET utf32 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `hoa_don`
--

INSERT INTO `hoa_don` (`ma`, `ma_khach_hang`, `ten_nguoi_nhan`, `dia_chi_nguoi_nhan`, `so_dien_thoai_nguoi_nhan`, `trang_thai`, `thoi_gian_mua`, `ghi_chu`) VALUES
(1, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 20:45:45', ''),
(2, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 20:50:45', ''),
(3, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 20:52:32', ''),
(5, 1, 'Đức CT', 'Thanh Hóa', '0968845131', 3, '2021-01-17 22:37:22', ''),
(8, NULL, 'Đức', 'Thanh Hoa', '0968845131', 2, '2021-01-17 22:41:40', ''),
(9, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 2, '2021-01-18 05:46:06', ''),
(10, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 2, '2021-01-20 18:00:48', ''),
(11, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 2, '2021-01-20 18:01:08', ''),
(12, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-20 18:03:35', 'chào'),
(13, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-20 18:08:03', ''),
(14, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-20 18:14:14', ''),
(15, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 2, '2021-01-20 18:14:21', ''),
(16, 3, 'Phan Mai Duyên', 'Ninh Bình', '0968845131', 3, '2021-01-21 01:34:49', ''),
(17, 3, 'Ductran', 'Ninh Bình', '0968845131', 3, '2021-01-21 02:19:22', ''),
(18, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 1, '2021-01-22 18:57:05', ''),
(19, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 3, '2021-01-22 19:04:09', ''),
(20, 1, 'TVĐ', 'Thanh Hóa', '0968845131', 1, '2021-01-22 20:38:40', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don_chi_tiet`
--

CREATE TABLE `hoa_don_chi_tiet` (
  `ma_hoa_don` int(11) NOT NULL,
  `ma_do_dung` int(11) NOT NULL,
  `so_luong` int(11) NOT NULL,
  `gia` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `hoa_don_chi_tiet`
--

INSERT INTO `hoa_don_chi_tiet` (`ma_hoa_don`, `ma_do_dung`, `so_luong`, `gia`) VALUES
(2, 1, 2, 19000),
(2, 2, 1, 19000),
(3, 5, 1, 19000),
(3, 1, 1, 19000),
(5, 5, 1, 19000),
(5, 1, 1, 19000),
(5, 1, 1, 19000),
(8, 1, 1, 19000),
(9, 1, 1, 19000),
(10, 2, 1, 19000),
(10, 1, 1, 19000),
(11, 1, 2, 19000),
(12, 2, 1, 19000),
(13, 1, 1, 19000),
(14, 1, 1, 19000),
(15, 1, 1, 19000),
(16, 1, 1, 19000),
(17, 1, 1, 19000),
(18, 5, 2, 19000),
(19, 1, 1, 19000),
(20, 1, 2, 19000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khach_hang`
--

CREATE TABLE `khach_hang` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` varchar(5) CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai` varchar(12) CHARACTER SET utf8 NOT NULL,
  `email` varchar(150) NOT NULL,
  `mat_khau` varchar(150) CHARACTER SET utf8 NOT NULL,
  `dia_chi` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `khach_hang`
--

INSERT INTO `khach_hang` (`ma`, `ten`, `ngay_sinh`, `gioi_tinh`, `so_dien_thoai`, `email`, `mat_khau`, `dia_chi`) VALUES
(1, 'TVĐ', '1996-07-02', 'Nam', '0968845131', 'ductran0207.bkhn@gmail.com', '123', 'Thanh Hóa'),
(3, 'Đức CT', '2001-06-05', 'Nữ', '0968845131', 'maiduyen@gmail.com', '123', 'Ninh Bình');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lien_he`
--

CREATE TABLE `lien_he` (
  `ma` int(11) NOT NULL,
  `noi_dung` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `lien_he`
--

INSERT INTO `lien_he` (`ma`, `noi_dung`) VALUES
(1, '<h1>Giới thiệu</h1>\r\n\r\n<p>► Địa chỉ: số 17, Tạ Quang Bửu</p>\r\n\r\n<p>► Email: ductran0207.bkhn@gmail.com</p>\r\n\r\n<p>►Số điện thoại: 0999.999.999</p>\r\n'),
(2, '<h1>Li&ecirc;n kết</h1>\r\n\r\n<p>Một c&aacute;i g&igrave; đ&oacute;</p>\r\n\r\n<p>Rất hay ho</p>\r\n\r\n<p>V&agrave; n&agrave;y nọ</p>\r\n'),
(4, '<h1>B&ecirc;n phải</h1>\r\n\r\n<h1><span style=\"font-size:13px\">Cute h&ocirc;ng</span></h1>\r\n');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_do_dung`
--

CREATE TABLE `loai_do_dung` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf32 NOT NULL,
  `ma_loai_cha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `loai_do_dung`
--

INSERT INTO `loai_do_dung` (`ma`, `ten`, `ma_loai_cha`) VALUES
(1, 'Bút viết', NULL),
(2, 'Sách vở', NULL),
(3, 'Đồ dùng văn phòng', NULL),
(4, 'Đồ dùng khác', NULL),
(5, 'Bút bi', 1),
(6, 'Bút máy', 1),
(7, 'Vở kẻ ngang', 2),
(8, 'Sách tham khảo', 2),
(9, 'Cặp tài liệu', 3),
(10, 'Mực in', 3),
(11, 'Bảng học sinh', 4),
(12, 'Tẩy gôm', 4),
(17, 'Phụ kiện', NULL),
(18, 'Túi xách', 17),
(19, 'Cặp đeo', 17),
(20, 'Bút màu', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_ngang`
--

CREATE TABLE `menu_ngang` (
  `ma` int(11) NOT NULL,
  `ten` varchar(50) CHARACTER SET utf8 NOT NULL,
  `noi_dung` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `menu_ngang`
--

INSERT INTO `menu_ngang` (`ma`, `ten`, `noi_dung`) VALUES
(2, 'Trang chủ', ''),
(3, 'Giới thiệu', '<p><img alt=\"\" src=\"https://vanphongphamhaiphong.vn/images/tintuc/790780_24656_van-phong-pham-htp15.jpg\" style=\"height:197px; width:450px\" />C&ocirc;ng ty <strong>văn ph&ograve;ng phẩm&nbsp;D&amp;D</strong>&nbsp;được th&agrave;nh lập bằng niềm đam m&ecirc; của tuổi trẻ nghị lực v&agrave; quyết t&acirc;m mang đến cho những kh&aacute;ch h&agrave;ng của m&igrave;nh những sản phẩm văn ph&ograve;ng phẩm gi&aacute; rẻ c&ugrave;ng với chất lượng được đảm bảo tốt nhất. &nbsp;C&oacute; thể n&oacute;i đ&oacute; l&agrave; mục ti&ecirc;u v&agrave; cũng l&agrave; kết quả đ&atilde; l&agrave;m được của <strong>văn ph&ograve;ng phẩm&nbsp;D&amp;D</strong>.</p>\r\n\r\n<p>C&oacute; thể n&oacute;i, một trong những nguy&ecirc;n nh&acirc;n gi&uacute;p V<strong>ăn ph&ograve;ng phẩm&nbsp;D&amp;D</strong> lớn mạnh như hiện nay đ&oacute; ch&iacute;nh l&agrave; những hiểu biết chuy&ecirc;n s&acirc;u về m&ocirc;i trường văn ph&ograve;ng, c&ocirc;ng sở.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Xuất ph&aacute;t từ một nh&acirc;n vi&ecirc;n văn ph&ograve;ng - người s&aacute;ng lập ra V<strong>ăn ph&ograve;ng phẩm&nbsp;D&amp;D</strong> thấu hiểu mọi y&ecirc;u cầu v&agrave; mong muốn về c&aacute;c dụng cụ văn ph&ograve;ng phẩm tiện &iacute;ch v&agrave; chất lượng tốt. Những ai đ&atilde; từng l&agrave;m việc v&agrave; gắn b&oacute; với c&ocirc;ng việc b&agrave;n giấy mới thấu hiểu được rằng: Văn ph&ograve;ng phẩm đầy đủ v&agrave; chất lượng cao kh&ocirc;ng chỉ phục vụ tốt cho c&ocirc;ng việc m&agrave; c&ograve;n gi&uacute;p nh&acirc;n vi&ecirc;n văn ph&ograve;ng c&oacute; cảm gi&aacute;c thoải m&aacute;i, dễ chịu hơn đồng thời ho&agrave;n th&agrave;nh c&ocirc;ng việc của m&igrave;nh nhanh hơn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tuy nhi&ecirc;n, vấn đề đặt ra ngay sau chất lượng tốt ch&iacute;nh l&agrave; gi&aacute; th&agrave;nh sản phẩm. Điều hiển nhi&ecirc;n rằng, nếu c&oacute; được nguồn cung cấp gi&aacute; th&agrave;nh văn ph&ograve;ng phẩm gi&aacute; rẻ sẽ gi&uacute;p cho một c&ocirc;ng ty tiết kiệm kh&ocirc;ng &iacute;t t&agrave;i ch&iacute;nh. Thấu hiểu những mong muốn đ&oacute;, Văn ph&ograve;ng phẩm D&amp;D&nbsp;đặt ra mục ti&ecirc;u cho m&igrave;nh phương ch&acirc;m, mục ti&ecirc;u kinh doanh l&agrave; :&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&ldquo;&nbsp;<strong>NHANH NHẤT &ndash; CHẤT LƯỢNG NHẤT &ndash; TIẾT KIỆM NHẤT</strong>&rdquo;.</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với triết l&yacute; kinh doanh n&agrave;y, D&amp;D đ&atilde; trở th&agrave;nh địa chỉ cung ứng văn ph&ograve;ng phẩm online v&agrave; trực tiếp với gi&aacute; cả rẻ nhất c&ugrave;ng chất lượng tốt trong lĩnh vực văn ph&ograve;ng phẩm gi&aacute; rẻ tại H&agrave; Nội.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Đặc trưng những sản phẩm của Văn ph&ograve;ng phẩm D&amp;D</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>C&ocirc;ng Ty D&amp;D&nbsp;l&agrave; một trong những địa chỉ cung cấp văn ph&ograve;ng phẩm gi&aacute; rẻ tạiH&agrave; Nội, với nhiều năm kinh doanh trong lĩnh vực văn ph&ograve;ng phẩm, sản phẩm của ch&uacute;ng t&ocirc;i lu&ocirc;n c&oacute; những &nbsp;y&ecirc;u cầu sau:&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Sản phẩm chất lượng nhất</p>\r\n	</li>\r\n	<li>\r\n	<p>Kiểu d&aacute;ng, chủng loại đa dạng nhất</p>\r\n	</li>\r\n	<li>\r\n	<p>B&aacute;o gi&aacute; văn ph&ograve;ng phẩm được cập nhật nhanh &ndash; ch&iacute;nh x&aacute;c nhất&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>Dịch vụ hậu m&atilde;i tuyệt vời nhất.</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2>Tại sao Văn ph&ograve;ng phẩm D&amp;D l&agrave;m được điều đ&oacute;?</h2>\r\n\r\n<p>Chắc hẳn khi đọc những điều tr&ecirc;n qu&yacute; kh&aacute;ch sẽ thắc mắc liệu đ&oacute; c&oacute; phải l&agrave; sự thật hay chỉ l&agrave; những lời PR, quảng c&aacute;o khuếch trương t&ecirc;n tuổi như bao nhi&ecirc;u c&ocirc;ng ty văn ph&ograve;ng phẩm tại H&agrave; Nội.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Để giải tỏa những băn khoăn đ&oacute; ch&uacute;ng t&ocirc;i xin chứng minh khả năng kinh doanh của ch&uacute;ng t&ocirc;i thay lời cam kết về chất lượng v&agrave; gi&aacute; th&agrave;nh văn ph&ograve;ng phẩm.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&ldquo;Chất lượng nhất&rdquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ch&uacute;ng t&ocirc;i chuy&ecirc;n cung cấp c&aacute;c mặt h&agrave;ng văn ph&ograve;ng phẩm được sản xuất tr&ecirc;n d&acirc;y chuyền c&ocirc;ng nghệ hiện đại của c&aacute;c nước h&agrave;ng đầu ch&acirc;u &Acirc;u như Đức, Mỹ v&agrave; ch&acirc;u &Aacute; như Nhật Bản, Ấn Độ... v&agrave; c&aacute;c sản phẩm chất lượng cao trong nước như: Thi&ecirc;n Long, Bến Ngh&eacute;, Hồng H&agrave;&hellip;Với những sản phẩm nhập khẩu ho&agrave;n to&agrave;n ch&uacute;ng t&ocirc;i cam kết c&oacute; giấy tờ chứng nhận xuất xứ, với những sản phẩm trong nước Văn ph&ograve;ng phẩm Ba Nhất cam kết nhập h&agrave;ng ch&iacute;nh h&atilde;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với những sản phẩm chất lượng tốt nhất trong nước v&agrave; quốc tế ch&uacute;ng t&ocirc;i tự h&agrave;o g&oacute;p phần gi&uacute;p bạn ho&agrave;n th&agrave;nh c&ocirc;ng việc một c&aacute;ch nhanh nhất v&agrave; hiệu quả nhất.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&ldquo;Tiết Kiệm Nhất&rdquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với c&aacute;c mặt h&agrave;ng văn ph&ograve;ng phẩm trong nước, Văn ph&ograve;ng phẩm D&amp;D l&agrave; đại l&yacute; c&ograve;n với c&aacute;c sản phẩm nhập khẩu ch&uacute;ng l&agrave; nh&agrave; ph&acirc;n phối trực tiếp n&ecirc;n c&oacute; nhiều ch&iacute;nh s&aacute;ch ưu đ&atilde;i về gi&aacute;. Th&ecirc;m v&agrave;o đ&oacute;, với phương ch&acirc;m kinh doanh&nbsp;&ldquo; H&agrave;ng Chất Lượng &ndash; Gi&aacute; B&igrave;nh D&acirc;n&rdquo;&nbsp;C&ocirc;ng Ty Ba Nhất lu&ocirc;n mang đến cho kh&aacute;ch h&agrave;ng mức chiết khấu cao nhất, đảm bảo gi&aacute; tốt nhất đến mọi kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&ldquo;Nhanh nhất&rdquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Văn ph&ograve;ng phẩm D&amp;D cam kết lu&ocirc;n c&oacute; lượng h&agrave;ng lớn, dự trữ đầy đủ c&aacute;c mặt h&agrave;ng văn ph&ograve;ng phẩm trong nước lẫn ngo&agrave;i nước đảm bảo cung ứng đầy đủ đơn h&agrave;ng lớn nhỏ của mọi kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ngo&agrave;i ra,&nbsp;<strong>Văn ph&ograve;ng phẩm D&amp;D&nbsp;</strong>cũng c&oacute; đội ngũ giao h&agrave;ng cơ động, nhiệt t&igrave;nh đảm bảo giao h&agrave;ng nhanh, đ&uacute;ng địa chỉ ở mọi loại địa h&igrave;nh. Với những địa chỉ l&agrave; c&ocirc;ng ty gần ngay đường lớn, với số lượng đơn h&agrave;ng lớn ch&uacute;ng t&ocirc;i c&oacute; đội ngũ xe chở h&agrave;ng bằng &ocirc; t&ocirc;, với những đơn h&agrave;ng nhỏ ch&uacute;ng t&ocirc;i c&oacute; đội ngũ giao h&agrave;ng bằng xe m&aacute;y đảm bảo trả h&agrave;ng nhanh v&agrave; đ&uacute;ng địa chỉ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Với h&igrave;nh thức thanh to&aacute;n phong ph&uacute;: chuyển khoản, thanh to&aacute;n trực tiếp, đặt cọc&hellip; Văn ph&ograve;ng phẩm D&amp;D cam kết giao h&agrave;ng ngay trong ng&agrave;y sau khi nhận được đơn đặt h&agrave;ng của qu&yacute; kh&aacute;ch. Yếu tố n&agrave;y c&agrave;ng củng cố th&ecirc;m phương ch&acirc;m Nhanh nhất của ch&uacute;ng t&ocirc;i.</p>\r\n'),
(5, 'Sản phẩm', ''),
(8, 'Liên hệ', '<h1>Li&ecirc;n hệ ch&uacute;ng tớ nh&eacute;.</h1>\r\n');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ma`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `background`
--
ALTER TABLE `background`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `do_dung`
--
ALTER TABLE `do_dung`
  ADD PRIMARY KEY (`ma`),
  ADD KEY `ma_loai_do_dung` (`ma_loai_do_dung`);

--
-- Chỉ mục cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`ma`),
  ADD KEY `ma_khach_hang` (`ma_khach_hang`);

--
-- Chỉ mục cho bảng `hoa_don_chi_tiet`
--
ALTER TABLE `hoa_don_chi_tiet`
  ADD KEY `ma_hoa_don` (`ma_hoa_don`),
  ADD KEY `ma_do_dung` (`ma_do_dung`);

--
-- Chỉ mục cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  ADD PRIMARY KEY (`ma`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `loai_do_dung`
--
ALTER TABLE `loai_do_dung`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `menu_ngang`
--
ALTER TABLE `menu_ngang`
  ADD PRIMARY KEY (`ma`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `background`
--
ALTER TABLE `background`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `do_dung`
--
ALTER TABLE `do_dung`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `loai_do_dung`
--
ALTER TABLE `loai_do_dung`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `menu_ngang`
--
ALTER TABLE `menu_ngang`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `do_dung`
--
ALTER TABLE `do_dung`
  ADD CONSTRAINT `do_dung_ibfk_1` FOREIGN KEY (`ma_loai_do_dung`) REFERENCES `loai_do_dung` (`ma`);

--
-- Các ràng buộc cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD CONSTRAINT `hoa_don_ibfk_1` FOREIGN KEY (`ma_khach_hang`) REFERENCES `khach_hang` (`ma`);

--
-- Các ràng buộc cho bảng `hoa_don_chi_tiet`
--
ALTER TABLE `hoa_don_chi_tiet`
  ADD CONSTRAINT `hoa_don_chi_tiet_ibfk_1` FOREIGN KEY (`ma_hoa_don`) REFERENCES `hoa_don` (`ma`),
  ADD CONSTRAINT `hoa_don_chi_tiet_ibfk_2` FOREIGN KEY (`ma_do_dung`) REFERENCES `do_dung` (`ma`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

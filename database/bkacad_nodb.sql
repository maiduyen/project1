-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 25, 2021 lúc 09:34 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+07:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bkacad`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` varchar(5) CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai` varchar(12) NOT NULL,
  `email` varchar(150) CHARACTER SET utf8 NOT NULL,
  `dia_chi` text NOT NULL,
  `can_cuoc_cong_dan` varchar(12) NOT NULL,
  `mat_khau` varchar(150) CHARACTER SET utf8 NOT NULL,
  `cap_do` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `background`
--

CREATE TABLE `background` (
  `ma` int(11) NOT NULL,
  `anh` varchar(150) CHARACTER SET utf8 NOT NULL,
  `loai_anh` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `do_dung`
--

CREATE TABLE `do_dung` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ma_loai_do_dung` int(11) NOT NULL,
  `gia` float NOT NULL,
  `mo_ta` text CHARACTER SET utf8 NOT NULL,
  `anh` varchar(150) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don`
--

CREATE TABLE `hoa_don` (
  `ma` int(11) NOT NULL,
  `ma_khach_hang` int(11) DEFAULT NULL,
  `ten_nguoi_nhan` varchar(150) CHARACTER SET utf32 NOT NULL,
  `dia_chi_nguoi_nhan` text CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai_nguoi_nhan` varchar(12) NOT NULL,
  `trang_thai` int(5) NOT NULL,
  `thoi_gian_mua` datetime NOT NULL,
  `ghi_chu` text CHARACTER SET utf32 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don_chi_tiet`
--

CREATE TABLE `hoa_don_chi_tiet` (
  `ma_hoa_don` int(11) NOT NULL,
  `ma_do_dung` int(11) NOT NULL,
  `so_luong` int(11) NOT NULL,
  `gia` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khach_hang`
--

CREATE TABLE `khach_hang` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf8 NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` varchar(5) CHARACTER SET utf8 NOT NULL,
  `so_dien_thoai` varchar(12) CHARACTER SET utf8 NOT NULL,
  `email` varchar(150) NOT NULL,
  `mat_khau` varchar(150) CHARACTER SET utf8 NOT NULL,
  `dia_chi` text CHARACTER SET utf8 NOT NULL,
  `tinh_trang` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lien_he`
--

CREATE TABLE `lien_he` (
  `ma` int(11) NOT NULL,
  `noi_dung` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_do_dung`
--

CREATE TABLE `loai_do_dung` (
  `ma` int(11) NOT NULL,
  `ten` varchar(150) CHARACTER SET utf32 NOT NULL,
  `ma_loai_cha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_ngang`
--

CREATE TABLE `menu_ngang` (
  `ma` int(11) NOT NULL,
  `ten` varchar(50) CHARACTER SET utf8 NOT NULL,
  `noi_dung` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ma`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `background`
--
ALTER TABLE `background`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `do_dung`
--
ALTER TABLE `do_dung`
  ADD PRIMARY KEY (`ma`),
  ADD KEY `ma_loai_do_dung` (`ma_loai_do_dung`);

--
-- Chỉ mục cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`ma`),
  ADD KEY `ma_khach_hang` (`ma_khach_hang`);

--
-- Chỉ mục cho bảng `hoa_don_chi_tiet`
--
ALTER TABLE `hoa_don_chi_tiet`
  ADD KEY `ma_hoa_don` (`ma_hoa_don`),
  ADD KEY `ma_do_dung` (`ma_do_dung`);

--
-- Chỉ mục cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  ADD PRIMARY KEY (`ma`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `loai_do_dung`
--
ALTER TABLE `loai_do_dung`
  ADD PRIMARY KEY (`ma`);

--
-- Chỉ mục cho bảng `menu_ngang`
--
ALTER TABLE `menu_ngang`
  ADD PRIMARY KEY (`ma`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `background`
--
ALTER TABLE `background`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `do_dung`
--
ALTER TABLE `do_dung`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `loai_do_dung`
--
ALTER TABLE `loai_do_dung`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `menu_ngang`
--
ALTER TABLE `menu_ngang`
  MODIFY `ma` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

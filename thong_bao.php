<!--Thành công-->
<?php if(isset($_SESSION['flash'])) { ?>
	<div class="success">
		<p>
			<?php echo $_SESSION['flash'];
			unset($_SESSION['flash']); ?>
		</p>
	</div>
<?php } ?>

<!--Lỗi đăng nhập sai -->
<?php if(isset($_GET['error_login'])) { ?>
	<script type="text/javascript">
		window.onload = display_login();
	</script>
<?php } ?>
<!--Lỗi đăng ký sai -->
<?php if(isset($_GET['error_register'])) { ?>
	<script type="text/javascript">
		window.onload = display_register();
	</script>
<?php } ?>
<!--Lỗi 404 sai đường dẫn -->

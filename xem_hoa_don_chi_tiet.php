<?php session_start(); ?>
<?php if (isset($_SESSION['ma_kh'])) { ?>
	<?php if (isset($_GET['ma'])) { ?>
	
	<?php include "title_chung.php" ?>
	<?php include "include/header.php" ?>
	<?php include "include/back-to-top.php" ?>
	<?php include "thong_bao.php" ?>
	<?php 
	$ma = $_GET['ma'];
	$ma_khach_hang = $_SESSION['ma_kh'];
	include 'connect.php';
	//kiểm tra khách hàng không xem hóa đơn của người khác
	$sql = "SELECT * from hoa_don where ma_khach_hang = '$ma_khach_hang' and ma = '$ma'";
	$result = mysqli_query($connect, $sql);
	$count = mysqli_num_rows($result);
	if($count != 1){
		echo "<script>alert('Đang cập nhật dữ liệu')</script>";
		echo "<script>window.location.assign('xem_hoa_don.php')</script>";
		exit();
	}
	$sql = "select
	hoa_don_chi_tiet.*,
	do_dung.ten,
	do_dung.anh
	from hoa_don_chi_tiet
	join do_dung on do_dung.ma = hoa_don_chi_tiet.ma_do_dung where ma_hoa_don='$ma'";
	$result = mysqli_query($connect, $sql);
	$count = mysqli_num_rows($result);
	$thu_muc_anh = 'image/product/';
	$tong_tien_tat_ca=0;
	?>
	<a style="text-decoration: none;
    color: #fff;
    color: #e500b2;
    font-size: 18px;
    font-weight: bold;cursor: pointer;margin:15px 10px 10px 15px;" href="xem_hoa_don.php">
    	Về hóa đơn
	</a>
	<?php if ($count > 0){ ?>
		<table style="border: 1px solid black;width: 95%;border-collapse: collapse;text-align: center;margin: 0 auto;">
		
		<h1 align="center">Hóa đơn chi tiết</h1>
		<tr  class="tieu_de_hoa_don">
			<th>Tên sản phẩm</th>
			<th>Ảnh</th>
			<th>Số lượng</th>
			<th>Giá</th>
			<th>Tổng</th>
		</tr>
		<?php foreach ($result as $each): ?>
			<tr>
				<td>
					<?php echo $each['ten']; ?>
				</td>
				<td>
					<img height="100px" src="<?php echo $thu_muc_anh . $each['anh'] ?>">
				</td>
				<td>
					<?php echo $each['so_luong']; ?>
				</td>
				<td>
					<?php echo number_format($each['gia'],0,",",".").' đ'?>
				</td>
				<td>
					<?php $tong = $each['so_luong'] *$each['gia'];
					echo number_format($tong,0,",",".").' đ'?>
					<?php $tong_tien_tat_ca+= $each['so_luong'] *$each['gia'] ?>
				</td>
			</tr>
			
		<?php endforeach ?>
	</table>
	<h2 style="margin-left: 15px;color: red;">
		Tổng tiền tất cả: <?php echo number_format($tong_tien_tat_ca,0,",",".").' đ'?>
	</h2>
	<?php
			include "connect.php";
			$sql = "SELECT * from hoa_don WHERE ma = '$ma' ";
			$result = mysqli_query($connect, $sql);
			$each = mysqli_fetch_array($result);
			$trang_thai = $each['trang_thai'];
	 	?>
		<p style="float: left;width: 60%;margin-top: 15px;margin-left: 15px;">
		
				<span>
					<b>Người nhận: </b><?php echo $each['ten_nguoi_nhan']; ?>
				</span>
				<br>
				<span>
					<b>Địa chỉ: </b><?php echo $each['dia_chi_nguoi_nhan']; ?>
				</span>
				<br>
				<span>
					<b>Số điện thoại: </b><?php echo $each['so_dien_thoai_nguoi_nhan']; ?>
				</span>
				<br>
				<span>
					<b>Tình trạng: </b>
					<?php 
						$trang_thai = $each['trang_thai'] ;
						if($trang_thai==1){
							echo "Đang chờ xử lý";
						}else if($trang_thai==2){
							echo "Đã duyệt";
						}else{
							echo "Đã hủy";
					} ?>
				</span>
				<br>
				<span>
					<b>Ghi chú: </b><?php echo $each['ghi_chu']; ?>
				</span>
		</p>
		<?php if($trang_thai == 1){ ?>
				
					<a onclick="return confirm('Bạn muốn hủy đơn hàng này chứ?')" style="float: right;width:100px;margin:0px 50px 10px 10px;color: red;cursor: pointer;" href="huy_don_hang.php?ma=<?php echo $ma ?>">Hủy đơn hàng</a>
				
			<?php }  ?>
	<?php } else{
				echo "<h1>Đơn hàng này trống.</h1>";
			} ?>
	
	<?php include "include/footer.php" ?>
	<?php } else { ?>
		<?php header("location:xem_hoa_don.php"); ?>
<?php } } else {
	// header("location:404page.php");
	header("location:index.php?error_login=Bạn phải đăng nhập để tiếp tục.");
}
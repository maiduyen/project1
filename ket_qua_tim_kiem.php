<?php session_start(); ?>
<!DOCTYPE html>
<html>
<?php include "title_chung.php" ?>
<body>
	
	<?php include "include/header.php" ?>
	
	<?php 
	$thu_muc_anh = 'image/product/';
	include 'connect.php';
	$tim_kiem = '';
	if (isset($_GET['tim_kiem'])) {
		$tim_kiem = addslashes($_GET['tim_kiem']);
	}

		//lấy sản phẩm phù hợp với loại sản phẩm và tìm kiếm
	$sql = "SELECT do_dung.*,loai_do_dung.ten as 'ten_loai_do_dung' from do_dung 
	join loai_do_dung on 
	loai_do_dung.ma = do_dung.ma_loai_do_dung where do_dung.ten like '%$tim_kiem%' or loai_do_dung.ten like '%$tim_kiem%'";
	$result = mysqli_query($connect,$sql);
		//đếm số sản phẩm đang có
	$tong_so_san_pham = mysqli_num_rows($result);

	$so_san_pham_1_trang = 8;
	//tinh số trang hiển thị
	$tong_so_trang = ceil($tong_so_san_pham / $so_san_pham_1_trang);
	$trang_hien_tai = 1;
	if(!empty($_GET['trang'])){
		if($_GET['trang'] > 0){
			$trang_hien_tai = $_GET['trang'];
		} else{
			$trang_hien_tai = 1;
		}
	}
	$so_san_pham_can_bo_qua = ($trang_hien_tai-1) * $so_san_pham_1_trang;
	//hiển thị sản phẩm trên từng trang một
	$sql = "$sql limit $so_san_pham_1_trang offset $so_san_pham_can_bo_qua";
	$result = mysqli_query($connect,$sql);
	$count = mysqli_num_rows($result);

	?>

	<div id="div_san_pham">
		<h1 class="tieu_de_tim_kiem">Kết quả tìm kiếm: <?php echo $tim_kiem; ?>
	</h1>
	<?php if($count > 0) {?>
		<div class="grid-container" style="width: 95% !important;margin: 0 auto;">
			<?php foreach ($result as $each): ?>
				<div class="grid-item">
					<p style="background: url('<?php echo $thu_muc_anh . $each['anh'] ?>');background-size: cover;background-repeat: no-repeat;background-position: center;background-color: #F6ADE7;height: 300px;max-width: 300px;">
						<a class="link_anh_san_pham" href="chi_tiet_san_pham.php?ma=<?php echo $each['ma'] ?>">
						</a>
					</p>

					<a href="chi_tiet_san_pham.php?ma=<?php echo $each['ma'] ?>"><?php echo $each['ten']; ?></a>
					<p>
						<?php echo number_format($each['gia'],0,",",".").' đ'?>

					</p>
					<a class="dat_hang"  href="them_vao_gio_hang.php?ma=<?php echo $each['ma'] ?>">
						Thêm vào giỏ hàng
					</a>
				</div>
			<?php endforeach ?>
		</div>
		<!--Phân trang -->
		<p align="center" class="so_trang">
			<?php if($trang_hien_tai > 3){
				$trang_dau_tien = 1; ?>
				<a href="?trang=<?php echo $trang_dau_tien ?>&tim_kiem=<?php echo $tim_kiem;?>#div_san_pham">First</a>
			<?php }
			if($trang_hien_tai >1 ){
				$trang_truoc = $trang_hien_tai -1; ?>
				<a href="?trang=<?php echo $trang_truoc ?>&tim_kiem=<?php echo $tim_kiem;?>#div_san_pham">Prev</a>
			<?php } ?>
			<?php for($i =1; $i <= $tong_so_trang; $i++){ ?>
				<?php if($i != $trang_hien_tai){ ?>
					<?php if($i > $trang_hien_tai -3 && $i < $trang_hien_tai +3){ ?>
						<a href="?trang=<?php echo $i ?>&tim_kiem=<?php echo $tim_kiem;?>#div_san_pham"><?php echo $i ?></a>
					<?php } ?>
				<?php } else{ ?>
					<strong class="active"><?php echo $i ?></strong>
				<?php } ?>
			<?php } ?>
			<?php if($trang_hien_tai <= $tong_so_trang -1){
				$trang_tiep = $trang_hien_tai +1; ?>
				<a href="?trang=<?php echo $trang_tiep ?>&tim_kiem=<?php echo $tim_kiem;?>#div_san_pham">Next</a>
			<?php }
			if ($trang_hien_tai < $tong_so_trang - 3) {
				$trang_cuoi = $tong_so_trang; ?>
				<a href="?trang=<?php echo $trang_cuoi ?>&tim_kiem=<?php echo $tim_kiem;?>#div_san_pham">Last</a>
			<?php } ?>
		</p>
		<!--End-->
	<?php }else{
		echo "<h1>Sản phẩm đang cập nhật...</h1>";
	} ?>
</div>

<?php mysqli_close($connect) ?>
<?php include "include/back-to-top.php" ?>
<?php include "include/footer.php" ?>
</body>
</html>
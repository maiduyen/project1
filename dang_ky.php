<div id="id02" class="modal" >
  <form class="modal-content animate" method="post" action="xu_ly_dang_ky.php">
    <div class="imgcontainer">
      <?php if (isset($_GET['error_register'])) { ?>
        <p id="error_register">
          <?php echo $_GET['error_register']; ?>
        </p>
      <?php } ?>
      <h1 align="center">Đăng ký</h1>
      <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close">&times;</span>
    </div>

    <div class="container">
      <label for="ten">
        <b>Họ và tên</b>
      </label>
      <span class="error" id="error_ten"></span>
      <input id="ten" type="text" name="ten" placeholder="Họ và tên">
      <label for="ngay_sinh">
        <b>Ngày sinh</b>
      </label>
      <span class="error" id="error_ngay_sinh"></span>
      <input id="ngay_sinh" type="date" name="ngay_sinh">
      <label for="gioi_tinh">
        <b>Giới tính</b>
        
        <span class="error" id="error_gioi_tinh"></span>
        <br>
      <input type="radio" name="gioi_tinh" value="Nam">Nam
      <input type="radio" name="gioi_tinh" value="Nữ">Nữ
      <br>
    </label>
    <label for="so_dien_thoai">
      <b>Số điện thoại</b>
    </label>
    <span class="error" id="error_so_dien_thoai"></span>
    <input id="so_dien_thoai" type="text" name="so_dien_thoai" placeholder="Nhập số điện thoại">
    <label for="email">
      <b>Email</b>
    </label>
    <span class="error" id="error_email"></span>
    <input id="email" type="email" placeholder="Email của bạn" name="email">
    <label for=dia_chi>
      <b>Địa chỉ</b>
    </label>
    <span class="error" id="error_dia_chi"></span>
    <input id="dia_chi" type="text" name="dia_chi" placeholder="Nhập địa chỉ">
    <label for="mat_khau">
      <b>Mật khẩu</b>
    </label>
    <span class="error" id="error_mat_khau"></span>
    <input id="mat_khau" type="password" placeholder="Nhập mật khẩu" name="mat_khau">
    <label for="repsw">
      <b>Nhập lại mật khẩu</b>
    </label>
    <span class="error" id="error_nhap_lai_mat_khau"></span>
    <input id="nhap_lai_mat_khau" type="password" placeholder="Nhập lại mật khẩu" name="nhap_lai_mat_khau">
    <br>
    <input type="hidden" name="tinh_trang" value="0">
    <button type="submit" onclick="return kiem_tra_dang_ky()">Đăng ký</button>
  </div>
</form>
</div>
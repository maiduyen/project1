<?php include '../common/index.php' ?>
<?php if(($_SESSION['cap_do'])!= 1){
	header("location:../index.php?error=Bạn không có quyền vào đây.");
}
?>
<div class="admin_view">
	<a onclick="history.go(-1)" style="cursor: pointer;color: blue;float: left;">Trang trước</a>
	<br><br>
	<?php if (!empty($_GET['ma_loai'])) { ?>
		<?php 
		$ma_loai = $_GET['ma_loai'];
		if($ma_loai == 'cha'){ ?>
			<form method="post" action="process_insert.php?ma_loai=<?php echo $ma_loai; ?>">
				<h1>Thêm loại đồ dùng cấp 1</h1>
				
				<label for="ten">
					<b>Tên loại đồ dùng cấp 1: </b>
				</label>
				<span class="error" id="error_ten"></span>
				<br>
				<input id="ten" type="text" name="ten" placeholder="Nhập tên"><br>

				<button type="submit" class="btn" onclick="return kiem_tra_them_cha()">Thêm</button>
			</form>

		<?php } else if($ma_loai == 'con'){ ?>
			<?php 
			include_once "../../connect.php";
			$sql_loai_do_dung_cha = "SELECT * from loai_do_dung where ma_loai_cha is Null";
			$result_loai_do_dung_cha = mysqli_query($connect,$sql_loai_do_dung_cha);
			?>
			<form method="post" action="process_insert.php?ma_loai=<?php echo $ma_loai; ?>">
				<h1>Thêm loại đồ dùng cấp 2</h1>

				<label for="ten">
					<b>Tên loại đồ dùng cấp 2: </b>
				</label>
				<span class="error" id="error_ten"></span>
				<br>
				<input id="ten" type="text" name="ten" placeholder="Nhập tên">
				<br>
				<select name="ma_loai_cha" id="ma_loai_cha" style="width: 200px;">
					<option value="0">Chọn loại đồ dùng</option>
					<?php foreach ($result_loai_do_dung_cha as $each_loai_do_dung_cha) : ?>
						<option name="ma_loai_do_dung" value="<?php echo $each_loai_do_dung_cha['ma'] ?>"><?php echo $each_loai_do_dung_cha['ten'] ?>

					</option>
					
				<?php endforeach ?>
				</select>
				<span class="error" id="error_ma_loai_do_dung"></span>
			<br>
			<button type="submit" class="btn" onclick="return kiem_tra_them_con()">Thêm</button>
		</form>
	<?php } else{
		header("location:index.php");
	} ?>
<?php } else{
	header("location:index.php");
} ?>
</div>
<script type="text/javascript">
	function kiem_tra_them_cha(){
		var kiem_tra_loi = false;
	    //ten
	    var ten = document.getElementById('ten').value;
	    var ten_regex = /^[A-Za-z\d\sáàảãạăâẩẳắằấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
	    if(ten_regex.test(ten)){
	    	document.getElementById('error_ten').innerHTML = '';
	    }
	    else{
	    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên hợp lệ.';
	    	kiem_tra_loi = true;
	    }
	    if (kiem_tra_loi == true) {
	    	return false;
	    }
	}
	function kiem_tra_them_con(){
		var kiem_tra_loi = false;
	    //ten
	    var ten = document.getElementById('ten').value;
	    var ten_regex = /^[A-Za-z\d\sáàảãạăâẳẩắằấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ýỳỷỹ]+$/;
	    if(ten_regex.test(ten)){
	    	document.getElementById('error_ten').innerHTML = '';
	    }
	    else{
	    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên hợp lệ.';
	    	kiem_tra_loi = true;
	    }
	    //ma loai cha
	    var ma_loai_do_dung = document.getElementsByName('ma_loai_do_dung');
	    var check_ma_loai_do_dung = false;
		   for(var i=0;i<ma_loai_do_dung.length;i++){
		   	if(ma_loai_do_dung[i].selected){
		   		check_ma_loai_do_dung = true;
		   	}
		   }
		   if(check_ma_loai_do_dung == true){
		   	document.getElementById('error_ma_loai_do_dung').innerHTML = '';
		   }
		   else{
		   	document.getElementById('error_ma_loai_do_dung').innerHTML = 'Vui lòng chọn loại ';
		   	kiem_tra_loi = true;
		   }
	    if (kiem_tra_loi == true) {
	    	return false;
	    }
	}
</script>
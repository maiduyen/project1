<?php include '../common/index.php' ?>
<?php if(($_SESSION['cap_do'])!= 1){
	header("location:../index.php?error=Bạn không có quyền vào đây.");
} ?>
<?php if (isset($_GET['ma'])) { ?>
<?php 
$ma = $_GET['ma'];
include '../../connect.php';
//kiểm tra mã tồn tại trong csdl
$sql_sp = "SELECT * from loai_do_dung where ma = '$ma'";
$result_sp = mysqli_query($connect,$sql_sp);
$count = mysqli_num_rows($result_sp);
	if($count != 1){
		echo "<script>alert('Sản phẩm này đang cập nhật...');window.location.assign('index.php');</script>";
		exit();
	}
$each_sp = mysqli_fetch_array($result_sp);

?>
<div class="admin_view">
	<a onclick="history.go(-1)" style="cursor: pointer;color: blue;float: left;">Trang trước</a>
	<br>
	<?php if (!empty($_GET['ma_loai'])) { ?>
		<?php 
			include_once "../../connect.php";
			$sql_loai_do_dung_cha = "SELECT * from loai_do_dung where ma_loai_cha is Null";
			$result_loai_do_dung_cha = mysqli_query($connect,$sql_loai_do_dung_cha);
			?>
		<?php 
		$ma_loai = $_GET['ma_loai'];
		if($ma_loai == 'cha'){ ?>
			<form method="post" action="proccess_update.php?ma_loai=<?php echo $ma_loai; ?>">
				<!-- <h1>Thêm loại đồ dùng cha</h1> -->
				<input name="ma"  type="hidden" value="<?php echo $ma ?>">
				<label for="ten">
					<b>Tên loại đồ dùng cấp 1: </b>
				</label>
				<span class="error" id="error_ten"></span>
				<br>
				<input id="ten" type="text" name="ten" value="<?php echo $each_sp['ten'] ?>"><br>

				<button type="submit" class="btn" onclick="return kiem_tra_sua_cha()">Sửa</button>
			</form>

		<?php } else if($ma_loai == 'con'){ ?>
			
			<form method="post" action="proccess_update.php?ma_loai=<?php echo $ma_loai; ?>">
				<!-- <h1>Thêm loại đồ dùng con</h1> -->
				<input name="ma"  type="hidden" value="<?php echo $ma ?>">
				<label for="ten">
					<b>Tên loại đồ dùng cấp 2: </b>
				</label>
				<span class="error" id="error_ten"></span>
				<br>
				<input id="ten" type="text" name="ten" value="<?php echo $each_sp['ten'] ?>">
				<br>
				<label for="ma_loai_cha"><b>Loại: </b></label>
				<br>
				<select name="ma_loai_cha" id="ma_loai_cha" style="width: 200px;">
					<option value="0">Chọn loại đồ dùng</option>
					<?php foreach ($result_loai_do_dung_cha as $each_loai_do_dung_cha) : ?>
						
						<option name="ma_loai_do_dung" value="<?php echo $each_loai_do_dung_cha['ma'] ?>" <?php if($each_sp['ma_loai_cha'] == $each_loai_do_dung_cha['ma']) echo 'selected'; ?> ><?php echo $each_loai_do_dung_cha['ten'] ?>

					</option>
					
				<?php endforeach ?>
				</select>
				<span class="error" id="error_ma_loai_do_dung"></span>
			<br>
			<button type="submit" class="btn" onclick="return kiem_tra_sua_con()">Sửa</button>
		</form>
	<?php } else{
		header("location:index.php");
	} ?>
<?php } else{
	header("location:index.php");
} ?>
</div>
<?php } ?>
<script type="text/javascript">
	function kiem_tra_sua_cha(){
		var kiem_tra_loi = false;
	    //ten
	    var ten = document.getElementById('ten').value;
	    var ten_regex = /^[A-Za-z\d\sáàảãạăâẳắằẩấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
	    if(ten_regex.test(ten)){
	    	document.getElementById('error_ten').innerHTML = '';
	    }
	    else{
	    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên hợp lệ.';
	    	kiem_tra_loi = true;
	    }
	    if (kiem_tra_loi == true) {
	    	return false;
	    }
	}
	function kiem_tra_sua_con(){
		var kiem_tra_loi = false;
	    //ten
	    var ten = document.getElementById('ten').value;
	    var ten_regex = /^[A-Za-z\d\sáàảãạăâẩẳắằấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
	    if(ten_regex.test(ten)){
	    	document.getElementById('error_ten').innerHTML = '';
	    }
	    else{
	    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên hợp lệ.';
	    	kiem_tra_loi = true;
	    }
	    //ma loai cha
	    var ma_loai_do_dung = document.getElementsByName('ma_loai_do_dung');
	    var check_ma_loai_do_dung = false;
		   for(var i=0;i<ma_loai_do_dung.length;i++){
		   	if(ma_loai_do_dung[i].selected){
		   		check_ma_loai_do_dung = true;
		   	}
		   }
		   if(check_ma_loai_do_dung == true){
		   	document.getElementById('error_ma_loai_do_dung').innerHTML = '';
		   }
		   else{
		   	document.getElementById('error_ma_loai_do_dung').innerHTML = 'Vui lòng chọn loại ';
		   	kiem_tra_loi = true;
		   }
	    if (kiem_tra_loi == true) {
	    	return false;
	    }
	}
</script>
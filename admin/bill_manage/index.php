<?php include "../common/index.php" ?>
<style type="text/css">
	.button {
		text-align: center;
		background-color: #4CAF50;
		border: none;
		color: white;
		/*padding: 15px 32px;*/
		padding: 0px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 15px;
		/*margin: 4px 2px;*/
		cursor: pointer;
	}
	.button_huy {
		text-align: center;
		background-color: red;
		border: none;
		color: white;
		/*padding: 15px 32px;*/
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 15px;
		/*margin: 4px 2px;*/
		cursor: pointer;
	}
	.tieu_de{
		border: 1px;
		background-color: #34ebd8;
		font-size: 20px;

	}
	.tieu_de_nho{
		font-size: 18px;
		font-weight: bold;
		height: 25px;
	}
</style>

<?php 
include '../../connect.php';
	//tìm kiếm
$tim_kiem = '';
if (isset($_GET['tim_kiem'])) {
	$tim_kiem = addslashes($_GET['tim_kiem']);
}
$sql="SELECT * from hoa_don where ten_nguoi_nhan like '%$tim_kiem%' or so_dien_thoai_nguoi_nhan like '%$tim_kiem%' or dia_chi_nguoi_nhan like '%$tim_kiem%' order by trang_thai asc, thoi_gian_mua desc";
$result = mysqli_query($connect, $sql);
$tong_so_hoa_don = mysqli_num_rows($result);
$so_hoa_don_1_trang = 8;
	//tinh số trang hiển thị
$tong_so_trang = ceil($tong_so_hoa_don / $so_hoa_don_1_trang);
$trang_hien_tai = 1;
if(isset($_GET['trang'])){
	$trang_hien_tai = $_GET['trang'];
}
$so_hoa_don_can_bo_qua = ($trang_hien_tai-1) * $so_hoa_don_1_trang;
	//hiển thị sản phẩm trên từng trang một
$sql = "$sql limit $so_hoa_don_1_trang offset $so_hoa_don_can_bo_qua";
$result = mysqli_query($connect,$sql);
$count = mysqli_num_rows($result);
?>

<div class="admin_view">

	<h1 align="center">Hóa đơn</h1>
	<form>
		Tìm kiếm
		<input placeholder="Tìm tên, số điện thoại..." type="search" name="tim_kiem" class="search" value="<?php echo($tim_kiem) ?>">
		<a href="index.php">Tất cả</a>
	</form>
	<?php if($count > 0 ) { ?>
	<!--Phân trang -->
	<?php include "../common/phan_trang.php"; ?>
	<!--End -->	
	<table style="border: 1px solid gray; width: 100%;margin-top: 10px;">
		<tr valign="top">
			<th class="tieu_de" rowspan="2">Thời gian</th>
			<th class="tieu_de" rowspan="2">Tình trạng</th>
			<th class="tieu_de" colspan="3">Thông tin người nhận</th>
			<th class="tieu_de" colspan="3">Thông tin người đặt</th>
			<th class="tieu_de" rowspan="2">Sửa tình trạng</th>
			<th class="tieu_de" rowspan="2">Ghi chú</th>
			<th class="tieu_de" rowspan="2">Xem</th>
		</tr>
		<tr>
			<td class="tieu_de_nho">Tên người nhận</td>
			<td class="tieu_de_nho">SĐT người nhận</td>
			<td class="tieu_de_nho">Địa chỉ người nhận</td>
			<td class="tieu_de_nho">Tên người đặt</td>
			<td class="tieu_de_nho">SĐT người đặt</td>
			<td class="tieu_de_nho">Địa chỉ người đặt</td>
		</tr>
		<?php foreach ($result as $each): ?>
			<tr>
				<tr rowspan="2">
					<td>
						<?php echo date_format(date_create($each['thoi_gian_mua']),'d-m-Y H:i:s') ?>
					</td>
					<td>
						<?php
						$trang_thai = $each['trang_thai'] ;
						if($trang_thai==1){
							echo "Đang chờ xử lý";
						}else if($trang_thai==2){
							echo "Đã duyệt";
						}else{
							echo "Đã hủy";
						}
						?>
					</td>
					<td>
						<?php echo $each['ten_nguoi_nhan']; ?>
					</td>
					<td>
						<?php echo $each['so_dien_thoai_nguoi_nhan']; ?>
					</td>
					<td>
						<?php echo $each['dia_chi_nguoi_nhan']; ?>
					</td>
					<?php 
					$ma_nguoi_dat = $each['ma_khach_hang'];
					$sql_nguoi_dat = "select *from khach_hang where ma = '$ma_nguoi_dat'";
					$result_nguoi_dat = mysqli_query($connect, $sql_nguoi_dat);
					$each_nguoi_dat = mysqli_fetch_array($result_nguoi_dat);
					?>
					<td>
						<?php echo $each_nguoi_dat['ten'] ?>
					</td>
					<td>
						<?php echo $each_nguoi_dat['so_dien_thoai'] ?>
					</td>
					<td>
						<?php echo $each_nguoi_dat['dia_chi'] ?>
					</td>
					<td>
						<?php if ($each['trang_thai']==1) { ?>
							<a onclick="return confirm('Bạn muốn duyệt hóa đơn này chứ?')"  style="padding-right: 10px;" class="button" href="thay_doi_trang_thai.php?ma=<?php echo $each['ma'] ?>&trang_thai=2">
								Duyệt

							</a>
							<a onclick="return confirm('Bạn muốn hủy hóa đơn này chứ?')" class="button_huy" href="thay_doi_trang_thai.php?ma=<?php echo $each['ma'] ?>&trang_thai=3">
								Hủy
								<!-- <i class="fas fa-times-circle"></i> -->
							</a>

						<?php } ?>
					</td>
					<td>
						<?php echo $each['ghi_chu'] ?>
					</td>
					<td>
						<a class="button" href="xem_hoa_don_chi_tiet.php?ma=<?php echo $each['ma'] ?>">
							Xem
						</a>
					</td>
				</tr>
			</tr>
		<?php endforeach ?>
	</table>
<?php } else{
	echo "<h1 align='center'>Chưa có dữ liệu</h1>";
} ?>
</div>
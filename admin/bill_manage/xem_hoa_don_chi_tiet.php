<?php include "../common/index.php" ?>
<?php if (!empty($_GET['ma'])) { ?>
	<?php
	$ma = $_GET['ma'];
	include '../../connect.php';
	$sql = "SELECT
	hoa_don_chi_tiet.*,
	do_dung.ten,
	do_dung.anh
	from hoa_don_chi_tiet
	join do_dung on do_dung.ma = hoa_don_chi_tiet.ma_do_dung where ma_hoa_don='$ma'";
	$result = mysqli_query($connect, $sql);
	$sql_count = "SELECT * from hoa_don where ma = '$ma'";
	$result_count = mysqli_query($connect,$sql_count);
	$count = mysqli_num_rows($result_count);
	$thu_muc_anh = '../../image/product/';
	$tong_tien_tat_ca=0;
	?>
	<div class="admin_view">
		<?php if($count == 1){ ?>
			<a onclick="history.go(-1)" style="cursor: pointer;color: blue;">Trang trước</a>
			<table border="1px" width="100%">
				<tr>
					<th>Tên sản phẩm</th>
					<th>Ảnh</th>
					<th>Số lượng</th>
					<th>Giá</th>
					<th>Tổng</th>
				</tr>
				<?php foreach ($result as $each): ?>
					<tr>
						<td>
							<?php echo $each['ten']; ?>
						</td>
						<td>
							<img height="100px" src="<?php echo $thu_muc_anh . $each['anh'] ?>">
						</td>
						<td>
							<?php echo $each['so_luong']; ?>
						</td>
						<td>
							<?php echo number_format($each['gia'],0,",",".")?>
						</td>
						<td>
							<?php $tong = $each['so_luong'] *$each['gia'];
							echo number_format($tong,0,",",".")?>
							<?php $tong_tien_tat_ca+= $each['so_luong'] *$each['gia'] ?>
						</td>
					</tr>

				<?php endforeach ?>
			</table>
			<h3>
				Tổng tiền tất cả: <?php echo number_format($tong_tien_tat_ca,0,",",".")?>
			</h3>
		<?php } else{
			echo "<h1 align='center'>Dữ liệu đang cập nhật</h1>";
			echo "<a onclick='history.go(-1)' style='cursor: pointer;color: blue;''>";
			echo "Trang trước";
			echo "</a>";

		} ?>
	</div>
<?php } else {
	header("location:../common/404page.php");
		// echo "<script>window.location.assign('index.php')</script>";
}

<?php include '../common/index.php' ?>
<?php if(($_SESSION['cap_do'])!= 1){
 		header("location:../index.php?error=Bạn không có quyền vào đây.");
 	}
?>
<div class="admin_view">
	<a onclick="history.go(-1)" style="cursor: pointer;color: blue;float: left;">Trang trước</a>
	<br>
	<form method="post" action="process_insert.php">
		<h1>Thêm thông tin</h1>
		  <?php if(isset($_GET['error_register'])){ ?>
		    <p class="error">
		      <?php echo $_GET['error_register'] ?>
		    </p>
     		<?php } ?>
		
		<label for="noi_dung">
			<b>Nội dung</b>
		</label>
		<span class="error" id="error_noi_dung"></span><br>
		<textarea name="noi_dung" id="noi_dung"></textarea>
    <script>CKEDITOR.replace('noi_dung');</script>
    <br>
		<button type="submit" class="btn" onclick="return kiem_tra_them_lien_he()">Thêm</button>
	</form>
</div>
<script type="text/javascript">
	function kiem_tra_them_lien_he() {
		var kiem_tra_loi = false;
    //noi_dung
    var noi_dung = CKEDITOR.instances.noi_dung.getData();
    if(noi_dung == ''){
      document.getElementById('error_noi_dung').innerHTML = 'Vui lòng nhập nội dung';
      kiem_tra_loi = true;
    	
    }
    else{
    	document.getElementById('error_noi_dung').innerHTML = '';
    }

if(kiem_tra_loi==true){
	return false;
}
}
</script>
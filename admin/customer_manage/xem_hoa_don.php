<?php include "../common/index.php" ?>
<?php if(!empty($_GET['ma'])){ ?>
<?php 
$ma = $_GET['ma'];
include "../../connect.php";
	//Kiem tra ma có trong CSDL hay không
$sql_check = "SELECT * from khach_hang where ma = '$ma'";
$result_check = mysqli_query($connect, $sql_check);

$count_check = mysqli_num_rows($result_check);
if($count_check > 0){ ?>
	<?php 
	$sql="SELECT * from hoa_don where ma_khach_hang = '$ma' order by thoi_gian_mua desc,trang_thai asc";
	$result = mysqli_query($connect, $sql);
	$tong_so_hoa_don = mysqli_num_rows($result);
	$so_hoa_don_1_trang = 8;
		//tinh số trang hiển thị
	$tong_so_trang = ceil($tong_so_hoa_don / $so_hoa_don_1_trang);
	$trang_hien_tai = 1;
	if(isset($_GET['trang'])){
		$trang_hien_tai = $_GET['trang'];
	}
	$so_hoa_don_can_bo_qua = ($trang_hien_tai-1) * $so_hoa_don_1_trang;
		//hiển thị sản phẩm trên từng trang một
	$sql = "$sql limit $so_hoa_don_1_trang offset $so_hoa_don_can_bo_qua";
	$result = mysqli_query($connect,$sql);
	$count = mysqli_num_rows($result);
	?>
	<div class="admin_view">
		<a onclick="history.go(-1)" style="cursor: pointer;color: blue;float: left;">Trang trước</a>
		<h1 align="center">Hóa đơn của 
			<?php 
			$each_check = mysqli_fetch_array($result_check);
			$ten_khach_hang = $each_check['ten']; 
			echo $ten_khach_hang; ?>
		</h1>
		<?php if($count > 0 ) { ?>
			<!--Phân trang -->
			<p align="center" class="so_trang">
				<?php if($trang_hien_tai > 3){
					$trang_dau_tien = 1; ?>
					<a href="?trang=<?php echo $trang_dau_tien ?>&ma=<?php echo $ma;?>">First</a>
				<?php }
				if($trang_hien_tai >1 ){
					$trang_truoc = $trang_hien_tai -1; ?>
					<a href="?trang=<?php echo $trang_truoc ?>&ma=<?php echo $ma;?>">Prev</a>
				<?php } ?>
				<?php for($i =1; $i <= $tong_so_trang; $i++){ ?>
					<?php if($i != $trang_hien_tai){ ?>
						<?php if($i > $trang_hien_tai -3 && $i < $trang_hien_tai +3){ ?>
							<a href="?trang=<?php echo $i ?>&ma=<?php echo $ma;?>"><?php echo $i ?></a>
						<?php } ?>
					<?php } else{ ?>
						<strong class="active"><?php echo $i ?></strong>
					<?php } ?>
				<?php } ?>
				<?php if($trang_hien_tai <= $tong_so_trang -1){
					$trang_tiep = $trang_hien_tai +1; ?>
					<a href="?trang=<?php echo $trang_tiep ?>&ma=<?php echo $ma;?>">Next</a>
				<?php }
				if ($trang_hien_tai < $tong_so_trang - 3) {
					$trang_cuoi = $tong_so_trang; ?>
					<a href="?trang=<?php echo $trang_cuoi ?>&ma=<?php echo $ma;?>">Last</a>
				<?php } ?>
			</p>
			<!--End -->	
			<table style="border: 1px solid gray; width: 100%;margin-top: 10px;">
				<tr valign="top">
					<th class="tieu_de" rowspan="2">Thời gian</th>
					<th class="tieu_de" rowspan="2">Tình trạng</th>
					<th class="tieu_de" colspan="3">Thông tin người nhận</th>
					
					<th class="tieu_de" rowspan="2">Ghi chú</th>
					<th class="tieu_de" rowspan="2">Xem</th>
				</tr>
				<tr>
					<td class="tieu_de_nho">Tên người nhận</td>
					<td class="tieu_de_nho">SĐT người nhận</td>
					<td class="tieu_de_nho">Địa chỉ người nhận</td>
					
				</tr>
				<?php foreach ($result as $each): ?>
					<tr>
						<tr rowspan="2">
							<td>
								<?php echo date_format(date_create($each['thoi_gian_mua']),'d-m-Y H:i:s') ?>
							</td>
							<td>
								<?php
								$trang_thai = $each['trang_thai'] ;
								if($trang_thai==1){
									echo "Đang chờ xử lý";
								}else if($trang_thai==2){
									echo "Đã duyệt";
								}else{
									echo "Đã hủy";
								}
								?>
							</td>
							<td>
								<?php echo $each['ten_nguoi_nhan']; ?>
							</td>
							<td>
								<?php echo $each['so_dien_thoai_nguoi_nhan']; ?>
							</td>
							<td>
								<?php echo $each['dia_chi_nguoi_nhan']; ?>
							</td>
							<td>
								<?php echo $each['ghi_chu'] ?>
							</td>

							<td>
								<a class="button" href="../bill_manage/xem_hoa_don_chi_tiet.php?ma=<?php echo $each['ma'] ?>">
									Xem
								</a>
							</td>
						</tr>
					</tr>
				<?php endforeach ?>
			</table>
			<div class="khoa_khach_hang">
				<?php 
				$tinh_trang = $each_check['tinh_trang'];
				if($tinh_trang == 1){
					echo "<p>Trạng thái: <b>Đã khóa</b></p>";
				} else{
					$count = 0;
					$sql = "SELECT count(*) as so_hoa_don_huy from hoa_don where ma_khach_hang = '$ma' and trang_thai = 3 group by day(thoi_gian_mua)";
					$result = mysqli_query($connect, $sql);
					foreach ($result as $each) {
						$so_hoa_don_huy = $each['so_hoa_don_huy'];
						if($so_hoa_don_huy > 3){
							$count = 1;
						}
					}
					if($count == 1){ ?>
						<a onclick="return confirm('Bạn muốn khóa tài khoản này chứ?')" href="khoa_khach_hang.php?ma=<?php echo $ma; ?>">Khóa tài khoản này</a>
						
					<?php } else{ ?>
						<p>
							<b>Trạng thái: Đang hoạt động.</b>
						</p>

					<?php }
				} ?>
			</div>
		<?php } else{
			echo "<h1 align='center'>Chưa có dữ liệu</h1>";
		} ?>
	</div>
<?php } else{ ?>
	<div class="admin_view">
		<h1>
			Chưa có dữ liệu.
		</h1>
		<br>
		<a href="index.php">Trang chủ</a>
	</div>
<?php } ?>

<?php } else{
header("location:index.php");
} ?>

	<title>Admin</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php 
	include "../../connect.php";
	$thu_muc_anh = '../../image/banner/';
	$sql = "SELECT anh from background where loai_anh = 0 order by ma desc limit 0,1";
	$result = mysqli_query($connect,$sql);
	$each = mysqli_fetch_array($result);
	$anh = $each['anh'];
 	?>
	<!--Logo-->
	<link rel = "icon" href ="<?php echo $thu_muc_anh.$anh ?>" type = "image/x-icon">
	<!--Font awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<!--Style css chung -->
	<link rel="stylesheet" type="text/css" href="../common/style.css">
	<!--Ck Editor-->
	<script src="../ckeditor/ckeditor.js"></script>
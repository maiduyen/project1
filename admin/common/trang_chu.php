<?php include "../common/index.php"; ?>
<?php 
include '../../connect.php';
$month = date('m');
$year = date('Y');
if(!empty($_GET['date'])){
	$date = $_GET['date'];
	$month = date('m', strtotime($date));
	$year = date('Y', strtotime($date));
}
$sql = "SELECT day(thoi_gian_mua) as ngay, sum(if(hoa_don.trang_thai = 2, hoa_don_chi_tiet.so_luong * hoa_don_chi_tiet.gia, 0)) as doanh_thu_da_nhan FROM hoa_don JOIN hoa_don_chi_tiet on hoa_don_chi_tiet.ma_hoa_don = hoa_don.ma WHERE month(thoi_gian_mua) = '$month' and year(thoi_gian_mua) = '$year' group by day(thoi_gian_mua)";
$result = mysqli_query($connect, $sql);
$count = mysqli_num_rows($result);

$date = getdate();
?>
<?php
$dataPoints = array();
foreach($result as $each) {
	$doanh_thu_da_nhan = $each['doanh_thu_da_nhan'];
	$ngay  = $each['ngay'];
	array_push($dataPoints,array("y" => $doanh_thu_da_nhan, "label" => "$ngay"));
}
?>
<div class="admin_view">
	<form>
		<input type="month" placeholder="Chọn một tháng" name="date"
		min="2018-03" value="<?php echo $date ?>">

		<button>Chọn tháng</button>
		<p>
			<b>Năm: </b>
			<?php echo $year; ?>
		</p>
		<p>
			<b>Tháng: </b>
			<?php echo $month; ?>
		</p>
		<p>
			<b>Tổng doanh thu tháng:</b>
			<span id="displayTotal"></span>
		</p>
		<p>
			<b>Doanh thu lớn nhất một ngày: </b>
			<span id="displayMaximum"></span>
		</p>
	</form>
	<?php if($count > 0){ ?>
	<div id="chartContainer" style="height: 370px; width: 100%;display: block;margin: 0 auto;">
	</div>
<?php } else{
	echo "<h1 align='center'>Tháng này chưa có doanh thu</h1>";
} ?>
</div>
<script>
	window.onload = function () {

		var chart = new CanvasJS.Chart("chartContainer", {
			title: {
				text: "Doanh thu theo ngày trong tháng"
			},
			axisY: {
				title: "Doanh thu"
			},
			axisX: {
				title: "Ngày trong tháng"
			},
			data: [{
				type: "line",
				dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
			}]
		});
		chart.render();
		var  sum;
		var totalArr = 0;
		var max = 0;
for(var i = 0; i < chart.options.data[0].dataPoints.length; i++) {
	sum = 0;
 
	for(var j = 0; j < chart.options.data.length; j++) {
		sum += chart.options.data[j].dataPoints[i].y;
	}
	totalArr+=sum;
	if(max < sum ){
			max = sum;
		}
}
document.getElementById('displayTotal').innerHTML = totalArr + ' đ';
document.getElementById("displayMaximum").innerHTML = max + ' đ';
}

</script>
<script src="../../javascript/canvasjs.min.js"></script>

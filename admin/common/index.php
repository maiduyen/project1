<?php include '../check_admin.php' ?>
<?php include "../common/title_chung.php" ?>
<?php if(isset($_SESSION['flash'])) { ?>
	<div class="success">
		<p>
			<?php echo $_SESSION['flash'];unset($_SESSION['flash']);?>
		</p>
	</div>
<?php } ?>

<?php if(isset($_GET['error'])) { ?>
	<div class="error">
		<p>
			<?php echo $_GET['error'];?>
		</p>
	</div>
<?php } ?>

<?php if(isset($_SESSION['flash'])) { ?>
	<div class="success">
		<p>
			<?php echo $_SESSION['flash'];
			unset($_SESSION['flash']);?>
		</p>
	</div>
<?php } ?>
<div class="view_all_menu" >
	<p class="welcome">Xin chào <b><?php echo $_SESSION['ten'] ?></b></p>
	<div class="menu">
		<ul class="menu-content">
			<li>
				<a href="../common/trang_chu.php" data-hover="Trang chủ">
					Trang chủ
				</a>
			</li>
			<?php if($_SESSION['cap_do']==1){ ?>
				<li>
					<a href="#" class="first-down">Nhân viên
						<span class="fas fa-caret-down first"></span>
					</a>
					<ul class="sub-menu first-show">
						<li>
							<a href="../staff_manage/index.php">Tất cả nhân viên
							</a>
						</li>
						<li>
							<a href="../staff_manage/view_insert.php">Thêm nhân viên
							</a>
						</li>
						
					</ul>
				</li>
			<?php } ?>
			<li>
				<a href="#" class="second-down">Khách hàng
					<span class="fas fa-caret-down second"></span>
				</a>
				<ul class="sub-menu second-show">
					<li>
						<a href="../customer_manage/index.php">Tất cả khách hàng
						</a>
					</li>
					<li>
						<a href="../customer_manage/view_insert.php">Thêm khách hàng
						</a>
					</li>
					
				</ul>
			</li>
			<li>
				<a href="#" class="third-down">Đồ dùng
					<span class="fas fa-caret-down third"></span>
				</a>
				<ul class="sub-menu third-show">
					<li>
						<a href="../product_manage/index.php">Tất cả sản phẩm
						</a>
					</li>
					<li>
						<a href="../product_manage/view_insert.php">Thêm sản phẩm
						</a>
					</li>
					
				</ul>
			</li>
			<li>
				<a href="../bill_manage/index.php">
					Hóa đơn
				</a>
			</li>
			<?php if($_SESSION['cap_do']==1){ ?>
			<li>
				<a href="../type_product_manage/index.php">
					Loại đồ dùng
				</a>
			</li>
			
			<li>
				<a href="../menu_ngang_manage">Menu ngang
				</a>
			</li>
			<li>
				<a href="../lien_he_manage">Liên hệ footer
				</a>
			</li>
			<li>
				<a href="../background_manage">Ảnh background
				</a>
			</li>
			<?php } ?>
			<li>
				<a href="../change_profile_admin/view_update.php">Thay đổi thông tin cá nhân</a>
			</li>
			<li>
				<a href="../common/dang_xuat.php">Đăng xuất
				</a>
			</li>

		</ul>
	</div>
</div>
<script src="../../javascript/jquery-3.5.1.min.js"></script>
<script type="text/javascript">
	$('.first-down').click(function(){
		$('.menu-content .first-show').toggleClass("show1")
	})
	$('.second-down').click(function(){
		$('.menu-content .second-show').toggleClass("show2")
	})
	$('.third-down').click(function(){
		$('.menu-content .third-show').toggleClass("show3")
	})
</script>
<?php include "../common/index.php" ?>
<?php if(($_SESSION['cap_do'])!= 1){
	header("location:../index.php?error=Bạn không có quyền vào đây.");
}
?>
<?php
$thu_muc_anh = '../../image/banner/';
if(isset($_POST['update'])) {
	require '../../connect.php';
	$ma = $_POST['ma'];
	$anh = $_FILES['anh_moi'];
	$loai_anh = $_POST['loai_anh'];

	if($anh['size']==0){
		$ten_anh = $_POST['anh_cu'];
	} else{
		$duoi_anh = explode('.',$anh['name'])[1];
		$ten_anh = time() . '.' . $duoi_anh;
		$duong_dan_anh = $thu_muc_anh . $ten_anh;
		move_uploaded_file($anh['tmp_name'], $duong_dan_anh);
	}

	$sql = "UPDATE background SET anh = '$ten_anh', loai_anh = '$loai_anh' WHERE ma = '$ma'";
	mysqli_query($connect, $sql );
	mysqli_close($connect);
	$_SESSION['flash'] = 'Thành công.';
	echo "<script>window.location.assign('index.php');</script>";
	// header("location:index.php?success=Đã sửa thành công");
} else if(isset($_POST['insert'])) {
	require '../../connect.php';

	$anh = $_FILES['anh'];
	$loai_anh = $_POST['loai_anh'];

	$duoi_anh = explode('.',$anh['name'])[1];

	$ten_anh = time() . '.'.$duoi_anh;
	$duong_dan_anh = $thu_muc_anh . $ten_anh;
	move_uploaded_file($anh['tmp_name'], $duong_dan_anh);
	$sql = "INSERT INTO background(anh,loai_anh) values('$ten_anh','$loai_anh')";
	mysqli_query($connect, $sql);
	mysqli_close($connect);
	$_SESSION['flash'] = 'Thành công.';
	echo "<script>window.location.assign('index.php');</script>";
	// header('location:index.php?success=Đã thêm ảnh');
} else if(isset($_POST['delete'])){
	require '../../connect.php';
	$ma = $_POST['ma'];
	$anh = $_POST['anh_cu'];
	$loai_anh = $_POST['loai_anh'];
	//kiểm tra số ảnh còn lại lớn hơn 5 thì xóa
	$sql = "SELECT * from background where loai_anh = '$loai_anh'";
	$result = mysqli_query($connect, $sql );
	$count = mysqli_num_rows($result);
	if($loai_anh == 1){
	if($count < 4){
		echo "<script>alert('Bạn không thể xóa ảnh này');</script>";
		echo "<script>window.location.assign('index.php');</script>";
		exit();
	} else{
	$sql = "DELETE from background WHERE ma = '$ma'";
	mysqli_query($connect, $sql );
	unlink($thu_muc_anh . $anh);
	mysqli_close($connect);
	$_SESSION['flash'] = 'Thành công.';
	echo "<script>history.go(-1)</script>";
	// header("location:index.php?success=Đã xóa thành công");
}
} else if($loai_anh == 0){
	if($count < 2){
		echo "<script>alert('Bạn không thể xóa ảnh này');</script>";
		echo "<script>window.location.assign('index.php');</script>";
		exit();
	} else{
	$sql = "DELETE from background WHERE ma = '$ma'";
	mysqli_query($connect, $sql );
	unlink($thu_muc_anh . $anh);
	mysqli_close($connect);
	$_SESSION['flash'] = 'Thành công.';
	echo "<script>window.location.assign('index.php');</script>";
	// header("location:index.php?success=Đã xóa thành công");
}
}
}else{ ?>
	<?php 
	require '../../connect.php';
	$sql_loai_anh = "SELECT distinct loai_anh from background";
	$result_loai_anh = mysqli_query($connect,$sql_loai_anh);
	?>
	<div class="admin_view">

		<h1 align="center">Thêm</h1>
		<!--Form thêm ảnh-->
		<form style="width: 90%;float: left;margin: 0 auto;" method = "post" action = "<?php $_PHP_SELF ?>" enctype="multipart/form-data">	
			<input id="anh" type="file" name="anh">
			<select name="loai_anh" id="loai_anh" style="width: 200px;">
				<?php foreach ($result_loai_anh as $each_loai_anh) { ?>
					<option value="<?php echo $each_loai_anh['loai_anh'] ?>"><?php if($each_loai_anh['loai_anh'] == 0) {echo 'Ảnh logo';} else{echo 'Ảnh banner';} ?>
				</option>
			<?php } ?>
		</select>
		<span class="error" id="error_anh"></span>
		<button name="insert" value="insert" onclick="return kiem_tra_them_anh()">Thêm</button>
	</form>
	<?php 
	$sql_logo = "SELECT *  from background where loai_anh = 0 order by ma desc";
	$result_logo = mysqli_query($connect,$sql_logo);
	?>
	
	<div style="width: 45%; float: left;display: block;height: auto;border-right: 2px solid black;">
		<h1 align="center">Ảnh logo</h1>
		<?php foreach ($result_logo as $each_logo):  ?>
			<!--Form hiển thị và sửa ảnh-->

			<form style="border: 1px solid gray;padding:10px;margin:5px;"  method = "post" action = "<?php $_PHP_SELF ?>" enctype="multipart/form-data">	
				<input name = "ma" type = "hidden" value="<?php echo $each_logo['ma'] ?>">
				<input type="hidden" name="anh_cu" value="<?php echo $each_logo['anh'] ?>">
				<img height="100px" width="200px" src="<?php echo $thu_muc_anh . $each_logo['anh'] ?>">
				<br>
				<label for="loai_anh">Loại ảnh: </label>
				<select name="loai_anh" id="loai_anh" style="width: 200px;"> 
					<option value="<?php echo $each_logo['loai_anh'] ?>" selected>
						Ảnh logo
					</option>
			</select>
			<input  type="file" name="anh_moi">
			<button name="update" value="update">Sửa</button>
			<button name="delete" value="delete" onclick="return confirm('Bạn muốn xóa chứ?')">Xóa</button>
		</form>
	<?php endforeach ?>
</div>
	<?php 
	$sql_banner = "SELECT *  from background where loai_anh = 1 order by ma desc";
	$result_banner = mysqli_query($connect,$sql_banner);
	?>
<div style="width: 45%; float: left;display: block;height: auto;">
	<h1 align="center">Ảnh banner</h1>
		<?php foreach ($result_banner as $each_banner):  ?>
			<!--Form hiển thị và sửa ảnh-->
			
			<form style="border: 1px solid gray;padding:10px;margin:5px;"  method = "post" action = "<?php $_PHP_SELF ?>" enctype="multipart/form-data">	
				<input name = "ma" type = "hidden" value="<?php echo $each_banner['ma'] ?>">
				<input type="hidden" name="anh_cu" value="<?php echo $each_banner['anh'] ?>">
				<img height="100px" width="200px" src="<?php echo $thu_muc_anh . $each_banner['anh'] ?>">
				<br>
				<label for="loai_anh">Loại ảnh: </label>
				<select name="loai_anh" id="loai_anh" style="width: 200px;"> 
					<option value="<?php echo $each_banner['loai_anh'] ?>" selected>
						Ảnh banner
					</option>
			</select>
			<input  type="file" name="anh_moi">
			<button name="update" value="update">Sửa</button>
			<button name="delete" value="delete" onclick="return confirm('Bạn muốn xóa chứ?')">Xóa</button>
		</form>
	<?php endforeach ?>
</div>
</div>
<?php } ?>
<script type="text/javascript">
	function kiem_tra_them_anh(){
		var kiem_tra_loi = false;
		// Ảnh
		var anh = document.getElementById('anh').value;
		if(anh != ''){
			document.getElementById('error_anh').innerHTML = '';
		}
		else{
			document.getElementById('error_anh').innerHTML = 'Vui lòng chọn ảnh sản phẩm.';
			kiem_tra_loi = true;
		}

		if(kiem_tra_loi==true){
			return false;
		}
	}
</script>
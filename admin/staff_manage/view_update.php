<?php include '../common/index.php' ?>
<?php if(($_SESSION['cap_do'])!= 1){
 		header("location:../index.php?error=Bạn không có quyền vào đây.");}
?>
<?php if (isset($_GET['ma'])) { ?>
<?php 
	$ma = $_GET['ma'];
	require '../../connect.php';
	$sql = "SELECT * from admin where ma ='$ma'";
	$result = mysqli_query($connect,$sql);
	$count = mysqli_num_rows($result);
	if($count == 0){
		header("location:index.php");
		exit();
	}
	$each = mysqli_fetch_array($result);
	$gioi_tinh = $each['gioi_tinh'];
	$cap_do = $each['cap_do'];
?>
<div class="admin_view">
		<a onclick="history.go(-1)" style="cursor: pointer;color: blue;float: left;">
			Trang trước
		</a>
		<br>
		 <?php if(isset($_GET['error_register'])){ ?>
		    <p class="error">
		      <?php echo $_GET['error_register'] ?>
		    </p>
		  <?php } ?>
		<form method="post" action="process_update.php">
		<h1>Sửa thông tin nhân viên</h1>
		  
     		<input type="hidden" name="ma" value="<?php echo $each['ma']?>">
		<label for="ten">
			<b>Họ và tên</b>
		</label>
		<span class="error" id="error_ten"></span>
		<br>
		<input id="ten" type="text" name="ten" value="<?php echo $each['ten'] ?>"><br>
		<label for="ngay_sinh">
			<b>Ngày sinh</b>
		</label>
		<span class="error" id="error_ngay_sinh"></span><br>
		<input id="ngay_sinh" type="date" name="ngay_sinh" value="<?php echo $each['ngay_sinh'] ?>"><br>
		<label for="gioi_tinh">
			<b>Giới tính</b>
			<span class="error" id="error_gioi_tinh"></span>
			<br>
			<input type="radio" name="gioi_tinh" value="Nam" <?php if($gioi_tinh == 'Nam') echo 'checked' ?>>Nam
			<input type="radio" name="gioi_tinh" value="Nữ" <?php if($gioi_tinh == 'Nữ') echo 'checked' ?>>Nữ
			<br>
		</label><br>
		<label for="so_dien_thoai">
			<b>Số điện thoại</b>
		</label>
		<span class="error" id="error_so_dien_thoai"></span><br>
		<input id="so_dien_thoai" type="text" name="so_dien_thoai" value="<?php echo $each['so_dien_thoai'] ?>"><br>
		<label for="email">
			<b>Email</b>
		</label>
		<span class="error" id="error_email"></span><br>
		<input id="email" type="email" name="email" value="<?php echo $each['email'] ?>"><br>
		<label for="dia_chi">
			<b>Địa chỉ</b>
		</label>
		<span class="error" id="error_dia_chi"></span><br>
		<input id="dia_chi" type="text" name="dia_chi" value="<?php echo $each['dia_chi'] ?>"><br>

		<label for="can_cuoc_cong_dan">
			<b>Số căn cước công dân</b>
		</label>
		<span class="error" id="error_can_cuoc_cong_dan"></span>
		<br>
		<input id="can_cuoc_cong_dan" type="text" name="can_cuoc_cong_dan" value="<?php echo $each['can_cuoc_cong_dan'] ?>">
			<br>
			<label for="quen_mat_khau">
				<b>Quên mật khẩu: </b>
			</label>
			<input name="quen_mat_khau" type="radio" value="123">
			<br>
		
		<input id="mat_khau" type="hidden" name="mat_khau" value="<?php echo $each['mat_khau'] ?>">
		<br>
		
		<label for="cap_do">
			<b>Cấp độ</b>
		</label>
		<span class="error" id="error_cap_do"></span>
		<br>
		<input type="radio" name="cap_do" value="0" <?php if($cap_do == 0 || $cap_do == 1) echo "checked disabled"; ?>>Admin
		<input type="radio" name="cap_do" value="1" <?php if($cap_do == 1) echo "checked"; ?>>Super admin
		<br>
		<button onclick="return kiem_tra_update_admin()">Sửa</button>
	</form>
		<?php mysqli_close($connect); ?>
	</div>
	<script type="text/javascript">
	function kiem_tra_update_admin(){
	var kiem_tra_loi = false;
    //ten
    var ten = document.getElementById('ten').value;
    var ten_regex = /^[A-Za-z\d\sáàảãạăâẳắằẩấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ýỳỷỹ]+$/;
    if(ten_regex.test(ten)){
    	document.getElementById('error_ten').innerHTML = '';
    }
    else{
    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên hợp lệ.';
    	kiem_tra_loi = true;
    }
    //ngay_sinh
    var ngay_sinh = document.getElementById('ngay_sinh').value;
    if(ngay_sinh ==''){
    	document.getElementById('error_ngay_sinh').innerHTML = 'Không được để trống.';
    	kiem_tra_loi = true;
    }
    else{
    	document.getElementById('error_ngay_sinh').innerHTML = '';
    }
   //gioi_tinh
   var gioi_tinh = document.getElementsByName('gioi_tinh');
   var check_gioi_tinh = false;
   for(var i=0;i<gioi_tinh.length;i++){
   	if(gioi_tinh[i].checked){
   		check_gioi_tinh = true;
   	}
   }
   if(check_gioi_tinh == true){
   	document.getElementById('error_gioi_tinh').innerHTML = '';
   }
   else{
   	document.getElementById('error_gioi_tinh').innerHTML = 'Vui lòng chọn giới tính.';
   	kiem_tra_loi = true;
   }
  //so_dien_thoai
  var so_dien_thoai = document.getElementById('so_dien_thoai').value;
  var so_dien_thoai_regex = /^\d*\.?\d+$/;
  if(so_dien_thoai_regex.test(so_dien_thoai)){
  	document.getElementById('error_so_dien_thoai').innerHTML = '';
  }
  else{
  	document.getElementById('error_so_dien_thoai').innerHTML = 'Vui lòng nhập số điện thoại của bạn.';
  	kiem_tra_loi = true;
  }

  //email
  var email = document.getElementById('email').value;
  var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(email_regex.test(email)){
  	document.getElementById('error_email').innerHTML = '';
  }
  else{
  	document.getElementById('error_email').innerHTML = 'Vui lòng nhập vào email của bạn.';
  	kiem_tra_loi = true;
  }

//dia_chi
var dia_chi = document.getElementById('dia_chi').value;
var dia_chi_regex = /^[a-zA-Z\d,. -\sáàảãạăâẩắằấầặẵẫậéèẻ ẽẹếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ýỳỷỹ]+$/;
if(dia_chi_regex.test(dia_chi)){
	document.getElementById('error_dia_chi').innerHTML = '';
}
else{
	document.getElementById('error_dia_chi').innerHTML = 'Vui lòng nhập vào địa chỉ hợp lệ.';
	kiem_tra_loi = true;
}

// //mat_khau
// var mat_khau = document.getElementById('mat_khau').value;
// var mat_khau_regex = /^[A-Za-z0-9.-_]+$/;
// if(mat_khau_regex.test(mat_khau)){
// 	document.getElementById('error_mat_khau').innerHTML = '';
// }
// else{
// 	document.getElementById('error_mat_khau').innerHTML = 'Mật khẩu không hợp lệ.';
// 	kiem_tra_loi = true;
// }
// //Mật khẩu mới
// var mat_khau_moi = document.getElementById('mat_khau_moi').value;
// var mat_khau_moi_regex = /^[A-Za-z0-9.-_]+$/;
// if(mat_khau_moi_regex.test(mat_khau_moi)){
// 	document.getElementById('error_mat_khau_moi').innerHTML = '';
// }
// else{
// 	document.getElementById('error_mat_khau_moi').innerHTML = 'Mật khẩu không hợp lệ.';
// 	kiem_tra_loi = true;
// }
//Căn cước
var can_cuoc_cong_dan = document.getElementById('can_cuoc_cong_dan').value;
var can_cuoc_cong_dan_regex = /^[0-9]+$/;
if(can_cuoc_cong_dan_regex.test(can_cuoc_cong_dan)){
	document.getElementById('error_can_cuoc_cong_dan').innerHTML = '';
}
else{
	document.getElementById('error_can_cuoc_cong_dan').innerHTML = 'Số căn cước công dân không hợp lệ.';
	kiem_tra_loi = true;
}
//cap_do
var cap_do = document.getElementsByName('cap_do');
  var check_cap_do = false;
   for(var i=0;i<cap_do.length;i++){
   	if(cap_do[i].checked){
   		check_cap_do = true;
   	}
   }
   if(check_cap_do == true){
   	document.getElementById('error_cap_do').innerHTML = '';
   }
   else{
   	document.getElementById('error_cap_do').innerHTML = 'Vui lòng chọn cấp độ.';
   	kiem_tra_loi = true;
   }

if(kiem_tra_loi==true){
	return false;
}
}
</script>
<?php } else{ 
	header("location:../common/404page.php"); }
	?>
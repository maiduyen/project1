<html>
<head>

<title>Đăng nhập</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--Logo-->
<link rel = "icon" href ="../image/banner/logo.jpg" type = "image/x-icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" href="common/dang_nhap.css" type="text/css" media="all" />
</head>
<body>

<?php if(isset($_GET['error'])) { ?>
	<h1>
		<?php echo $_GET['error'] ?>
	</h1>
<?php } ?>

<div class="container">
	<!--//header-->
	<div class="content_container">
		<div class="in_content">	
			<div class="login">
				<h2>Trang đăng nhập</h2>
			</div>
			<form action="process_login.php" method="post" style="position: relative;">
				<input id="email" placeholder="Nhập e-mail" name="email" class="user" type="email">
				<span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span>
				<br>
				<span class="error" id="error_email"></span>
				<br><br>
				<input id="password" placeholder="Nhập mật khẩu" name="mat_khau" class="pass" type="password">
				<span class="icon2"><i class="fa fa-lock" aria-hidden="true"></i></span>
				<br>
				<span class="error" id="error_password"></span>
				<br>
				<div class="button">
					<div >
						<input type="submit" value="Đăng nhập" onclick="return kiem_tra_dang_nhap_admin()">
					</div>
				</div>
			</form>
		</div>
	</div>
	<!--//main-->
</div>
<script>
	function kiem_tra_dang_nhap_admin(){
		var kiem_tra_loi = false;
		//Email
		var email = document.getElementById('email').value;
		var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(email_regex.test(email)){
			document.getElementById('error_email').innerHTML = '';
		}
		else{
			document.getElementById('error_email').innerHTML = 'Vui lòng nhập email của bạn.';
			kiem_tra_loi = true;
		}
		//password
		var password = document.getElementById('password').value;
		var password_regex = /^[A-Za-z0-9\@$!%*#?&-.,]+$/;
		if(password_regex.test(password)){
			document.getElementById('error_password').innerHTML = '';
		}
		else{
			document.getElementById('error_password').innerHTML = 'Mật khẩu không hợp lệ.';
			kiem_tra_loi = true;
		} 
		if(kiem_tra_loi==true){
			return false;
		}
}
</script>
</body>
</html>
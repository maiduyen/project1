<?php include "../common/index.php" ?>
<?php 
include '../../connect.php';
$sql = "SELECT * from loai_do_dung where ma_loai_cha is not null";
$result = mysqli_query($connect,$sql);
?>
<div class="admin_view">
	<a onclick="history.go(-1)" style="cursor: pointer;color: blue;float: left;">Trang trước</a>
	<br>
	<form method="post" action="process_insert.php" enctype="multipart/form-data">
		<label for="ten">
			<b>Tên</b>
		</label>
		<span class="error" id="error_ten"></span>
		<br>
		<input id="ten" type="text" name="ten">
		<br>
		<label for="gia">
			<b>Giá</b>
		</label>
		<span class="error" id="error_gia"></span>
		<br>
		<input id="gia" type="text" name="gia">
		<br>
		<label for="mo_ta">
			<b>Mô tả</b>
		</label>
		<span class="error" id="error_mo_ta"></span>
		<br>
		<textarea name="mo_ta" id="mo_ta"></textarea>
		<script>CKEDITOR.replace('mo_ta');</script>
		<br>
		<label for="loai_do_dung">
			<b>Loại đồ dùng</b>
		</label>
		<span class="error" id="error_loai_do_dung"></span>
		<br>
		<select name="ma_loai_do_dung">
			<?php foreach ($result as $each) : ?>
				<option name="loai_do_dung" value="<?php echo $each['ma'] ?>">
					<?php echo $each['ten'] ?>
				</option>
			<?php endforeach ?>
		</select>
		<br>
		<label for="anh">
			<b>Ảnh</b>
		</label>
		<span class="error" id="error_anh"></span>
		<br>
		<input type="file" id="anh" name="anh" class="custom_file_input">
		<br>
		<button onclick="return kiem_tra_them_san_pham()">Đăng</button>
	</form>
	
</div>
<script type="text/javascript">
	function kiem_tra_them_san_pham() {
		var kiem_tra_loi = false;
    //Tên
    var ten = document.getElementById('ten').value;
    var ten_regex = /^[A-Za-z\d-,.\sáàảãạăâẳẩắằấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
    if(ten_regex.test(ten)){
    	document.getElementById('error_ten').innerHTML = '';
    }
    else{
    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên sản phẩm.';
    	kiem_tra_loi = true;
    }
    //Giá
    var gia = document.getElementById('gia').value;
    var gia_regex = /^[1-9]([0-9]?)+$/;
    if(gia_regex.test(gia)){
    	document.getElementById('error_gia').innerHTML = '';
    }
    else{
    	document.getElementById('error_gia').innerHTML = 'Vui lòng nhập giá sản phẩm.';
    	kiem_tra_loi = true;
    }

    //Mô tả
    var mo_ta = CKEDITOR.instances.mo_ta.getData();
    if(mo_ta != ''){
    	document.getElementById('error_mo_ta').innerHTML = '';
    }
    else{
    	document.getElementById('error_mo_ta').innerHTML = 'Vui lòng nhập mô tả sản phẩm.';
    	kiem_tra_loi = true;
    }

    //Loại đồ dùng
    var loai_do_dung = document.getElementsByName('loai_do_dung');
   var check_loai_do_dung = false;
   for(var i=0;i<loai_do_dung.length;i++){
   	if(loai_do_dung[i].selected){
   		check_loai_do_dung = true;
   	}
   }
   if(check_loai_do_dung == true){
   	document.getElementById('error_loai_do_dung').innerHTML = '';
   }
   else{
   	document.getElementById('error_loai_do_dung').innerHTML = 'Vui lòng chọn loại sản phẩm.';
   	kiem_tra_loi = true;
   }
  //Ảnh
  var anh = document.getElementById('anh').value;
    if(anh != ''){
    	document.getElementById('error_anh').innerHTML = '';
    }
    else{
    	document.getElementById('error_anh').innerHTML = 'Vui lòng chọn ảnh sản phẩm.';
    	kiem_tra_loi = true;
    }

    if(kiem_tra_loi==true){
	return false;
	}
}
 </script>
<?php mysqli_close($connect) ?>
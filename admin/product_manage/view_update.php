<?php include "../common/index.php" ?>
<?php if (isset($_GET['ma'])) { ?>
<?php 
$ma = $_GET['ma'];
$thu_muc_anh = '../../image/product/';

include '../../connect.php';
//kiểm tra mã tồn tại trong csdl
$sql_sp = "SELECT * from do_dung where ma = '$ma'";
$result_sp = mysqli_query($connect,$sql_sp);
$count = mysqli_num_rows($result_sp);
	if($count != 1){
		echo "<script>alert('Sản phẩm này đang cập nhật...');window.location.assign('index.php');</script>";
		exit();
	}
$each_sp = mysqli_fetch_array($result_sp);

$sql_lsp = "SELECT * from loai_do_dung where ma_loai_cha is not null";
$result_lsp = mysqli_query($connect,$sql_lsp);
?>

<div class="admin_view">
	<a onclick="history.go(-1)" style="cursor: pointer;color: blue;float: left;">Trang trước</a>
	<br>
	<form action="process_update.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="ma" value="<?php echo $each_sp['ma'] ?>">
		<label for="ten">
			<b>Tên</b>
		</label>
		<span class="error" id="error_ten"></span>
		<br>
		<input id="ten" type="text" name="ten" value="<?php echo $each_sp['ten'] ?> ">
		<br>
		<label for="gia">
			<b>Giá</b>
		</label>
		<span class="error" id="error_gia"></span>
		<br>
		<input id="gia" type="number" name="gia" value="<?php echo $each_sp['gia'] ?>">
		<br>
		<label for="mo_ta">
			<b>Mô tả</b>
		</label>
		<span class="error" id="error_mo_ta"></span>
		<br>
		<textarea name="mo_ta" id="mo_ta" rows="6"><?php echo $each_sp['mo_ta'] ?></textarea>
		<script>CKEDITOR.replace('mo_ta');</script>
		<br>
		<label for="loai_do_dung">
			<b>Loại sản phẩm</b>
		</label>
		<span class="error" id="error_loai_do_dung"></span>
		<br>
		<select name="ma_loai_do_dung">
			<?php foreach ($result_lsp as $each_lsp) : ?>
				<option name="loai_do_dung" value="<?php echo $each_lsp['ma'] ?>"
					<?php if ($each_lsp['ma'] == $each_sp['ma_loai_do_dung']) echo "selected"; ?>
					>
					<?php echo $each_lsp['ten'] ?>
				</option>
			<?php endforeach ?>
		</select>
		<br>
		<input type="hidden" name="anh_cu" value="<?php echo $each_sp['anh'] ?> ">
		<img height="180px" width="150px" src="<?php echo $thu_muc_anh . $each_sp['anh'] ?>">
		<br>
		<input class="custom_file_input" type="file" name="anh_moi">
		<br>
		<button onclick="return kiem_tra_sua_do_dung()">Sửa</button>
	</form>
	<?php mysqli_close($connect) ?>
</div>
<script type="text/javascript">
	function kiem_tra_sua_do_dung() {
		var kiem_tra_loi = false;
    //Tên
    var ten = document.getElementById('ten').value;
    var ten_regex = /^[A-Za-z\d-.,\sáàảãạăẩâẳắằấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
    if(ten_regex.test(ten)){
    	document.getElementById('error_ten').innerHTML = '';
    }
    else{
    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên sản phẩm.';
    	kiem_tra_loi = true;
    }
    //Giá
    var gia = document.getElementById('gia').value;
    var gia_regex = /^[1-9]([0-9]?)+$/;
    if(gia_regex.test(gia)){
    	document.getElementById('error_gia').innerHTML = '';
    }
    else{
    	document.getElementById('error_gia').innerHTML = 'Vui lòng nhập giá sản phẩm.';
    	kiem_tra_loi = true;
    }

    //Mô tả
    var mo_ta = CKEDITOR.instances.mo_ta.getData();
    if(mo_ta != ''){
    	document.getElementById('error_mo_ta').innerHTML = '';
    }
    else{
    	document.getElementById('error_mo_ta').innerHTML = 'Vui lòng nhập mô tả sản phẩm.';
    	kiem_tra_loi = true;
    }

    //Loại đồ dùng
    var loai_do_dung = document.getElementsByName('loai_do_dung');
    var check_loai_do_dung = false;
    for(var i=0;i<loai_do_dung.length;i++){
    	if(loai_do_dung[i].selected){
    		check_loai_do_dung = true;
    	}
    }
    if(check_loai_do_dung == true){
    	document.getElementById('error_loai_do_dung').innerHTML = '';
    }
    else{
    	document.getElementById('error_loai_do_dung').innerHTML = 'Vui lòng chọn loại đồ dùng';
    	kiem_tra_loi = true;
    }

    if(kiem_tra_loi==true){
    	return false;
    }
}
</script>

<?php } else{
	header("location:../common/404page.php");
} ?>
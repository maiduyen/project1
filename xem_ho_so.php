<?php session_start() ?>
<?php if (isset($_SESSION['ma_kh'])) { ?>
<?php include "title_chung.php" ?>
<?php include "include/header.php" ?>
<?php include "thong_bao.php" ?>
<?php 
	$ma_khach_hang=$_SESSION['ma_kh'];
	include 'connect.php';
	$sql = "SELECT * from khach_hang where ma ='$ma_khach_hang'";
	$result = mysqli_query($connect,$sql);
	$each = mysqli_fetch_array($result);
	$gioi_tinh = $each['gioi_tinh'];
 ?>
 <style>
 	.admin_view{
 		margin: 0 auto;
 		width: 70%;
 		height: auto;
 	}
 	input[type=text],input[type=password], input[type=date], input[type=email], input[type=number], input[type=file],textarea,select {
  width: 60%;
  padding: 10px;
  margin: 5px 0 15px 0;
  display: inline-block;
  border: none;
  font-size: 16px;
  background: #f7f7f7;
  height: auto;
  font-family: Montserrat,Arial, Helvetica, sans-serif;
}
input[type=radio]{
    height: 18px;
    width: 18px;
    margin: 5px;
    vertical-align: middle;
}
input[type=text]:focus,input[type=password]:focus, input[type=date]:focus, input[type=number]:focus, input[type=email]:focus, input[type=file]:focus,select:focus {
  background-color: #ddd;
  outline: none;
}
.error, .error_register{
  color: red;
}
/*---end-*/
/*--Style insert sản phẩm--*/
.custom_file_input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom_file_input::before {
  content: 'Chọn file ảnh';
  display: inline-block;
  background: linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 5px 8px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  font-weight: 700;
  font-size: 10pt;
}
.custom_file_input:hover::before {
  border-color: black;
}
.custom_file_input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}
.admin_view button {
  background-color: #e500b2;
  color: white;
  padding: 14px 20px;
  margin: 0 auto;
  border: none;
  cursor: pointer;/* con trỏ chuột khi hover vào*/
  width: 200px;
  opacity: 0.9;
  font-size:16px;
  font-family: Montserrat,Arial, Helvetica, sans-serif;
  border-radius:10px;
}
.admin_view button:hover {
  opacity:1;
}
 </style>
 <div class="admin_view">
		 <?php if(isset($_GET['error_register'])){ ?>
		    <p class="error">
		      <?php echo $_GET['error_register'] ?>
		    </p>
		  <?php } ?>
		<form method="post" action="xu_ly_thay_doi_thong_tin.php">
			<input type="hidden" name="ma" value="<?php echo $each['ma']?>">
			<label for="ten">
				<b>Họ và tên</b>
			</label>
			<span class="error" id="error_ten"></span>
			<br>
			<input id="ten" type="text" name="ten" value="<?php echo $each['ten'] ?>">
			<br>
			<label for="ngay_sinh">
				<b>Ngày sinh</b>
			</label>
			<span class="error" id="error_ngay_sinh"></span>
			<br>
			<input id="ngay_sinh" type="date" name="ngay_sinh" value="<?php echo $each['ngay_sinh'] ?>">
			<br>
			<label for="gioi_tinh">
				<b>Giới tính</b>
			</label>
			<span class="error" id="error_gioi_tinh"></span>
			<br>
			<input type="radio" name="gioi_tinh" value="Nam" <?php if($gioi_tinh=='Nam') echo "checked"; ?>>Nam
			<input type="radio" name="gioi_tinh" value="Nữ" <?php if($gioi_tinh=='Nữ') echo "checked"; ?>>Nữ
			<br>

			<label for="email">
				<b>Email</b>
			</label>
			<span class="error" id="error_email"></span><br>
			<br>
			<input id="email" type="email" name="email" value="<?php echo $each['email'] ?>">
			<br>
			<label for="so_dien_thoai">
				<b>Số điện thoại</b>
			</label>
			<span class="error" id="error_so_dien_thoai"></span>
			<br>
			<input id="so_dien_thoai" type="text" name="so_dien_thoai" value="<?php echo $each['so_dien_thoai'] ?>">
			<br>
			<label for=dia_chi>
				<b>Địa chỉ</b>
			</label>
			<span class="error" id="error_dia_chi"></span>
			<br>
			<input id="dia_chi" type="text" name="dia_chi" value="<?php echo $each['dia_chi'] ?>">
			<br>
			<label for=mat_khau>
				<b>Mật khẩu cũ</b>
			</label>
			<span class="error" id="error_mat_khau"></span>
			<br>
			<input id="mat_khau" type="password" name="mat_khau" value="<?php echo $each['mat_khau'] ?>" disabled>
			<br>
			<label for=mat_khau_moi>
				<b>Mật khẩu mới</b>
				<p>(để mặc định sẽ không đổi mật khẩu)</p>
			</label>
			<span class="error" id="error_mat_khau_moi"></span>
			<br>
			<input id="mat_khau_moi" type="password" name="mat_khau_moi" value="<?php echo $each['mat_khau'] ?>">
      <span onclick="show_password()" style="font-size: 1em;color: gray;"><i class="far fa-eye"></i></span>
      <input type="hidden" name="tinh_trang" value="<?php echo $each['tinh_trang'] ?>">
      <br>
			<button onclick="return kiem_tra_update()">Đổi thông tin</button>
		</form>
		<?php mysqli_close($connect); ?>
	</div>
	<script type="text/javascript">
  function show_password(){
        var temp = document.getElementById("mat_khau_moi"); 
        if (temp.type === "password") { 
            temp.type = "text"; 
        } 
        else { 
            temp.type = "password"; 
        } 
    } 

	function kiem_tra_update() {
		var kiem_tra_loi = false;
    //ten
    var ten = document.getElementById('ten').value;
    var ten_regex = /^[A-Za-z\sáàảãạăâẳắằấẩầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
    if(ten_regex.test(ten)){
    	document.getElementById('error_ten').innerHTML = '';
    }
    else{
    	document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên hợp lệ.';
    	kiem_tra_loi = true;
    }
    //ngay_sinh
    var ngay_sinh = document.getElementById('ngay_sinh').value;
    if(ngay_sinh ==''){
    	document.getElementById('error_ngay_sinh').innerHTML = 'Không được để trống.';
    	kiem_tra_loi = true;
    }
    else{
    	document.getElementById('error_ngay_sinh').innerHTML = '';
    }
   //gioi_tinh
   var gioi_tinh = document.getElementsByName('gioi_tinh');
   var check_gioi_tinh = false;
   for(var i=0;i<gioi_tinh.length;i++){
   	if(gioi_tinh[i].checked){
   		check_gioi_tinh = true;
   	}
   }
   if(check_gioi_tinh == true){
   	document.getElementById('error_gioi_tinh').innerHTML = '';
   }
   else{
   	document.getElementById('error_gioi_tinh').innerHTML = 'Vui lòng chọn giới tính.';
   	kiem_tra_loi = true;
   }
  //so_dien_thoai
  var so_dien_thoai = document.getElementById('so_dien_thoai').value;
  var so_dien_thoai_regex = /^\d*\.?\d+$/;
  if(so_dien_thoai_regex.test(so_dien_thoai)){
  	document.getElementById('error_so_dien_thoai').innerHTML = '';
  }
  else{
  	document.getElementById('error_so_dien_thoai').innerHTML = 'Vui lòng nhập số điện thoại của bạn.';
  	kiem_tra_loi = true;
  }

  //email
  var email = document.getElementById('email').value;
  var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(email_regex.test(email)){
  	document.getElementById('error_email').innerHTML = '';
  }
  else{
  	document.getElementById('error_email').innerHTML = 'Vui lòng nhập vào email của bạn.';
  	kiem_tra_loi = true;
  }

//dia_chi
var dia_chi = document.getElementById('dia_chi').value;
var dia_chi_regex = /^[a-zA-Z\d,. -\sáàảãạăâắằẩấầặẵẫậéèẻ ẽẹếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
if(dia_chi_regex.test(dia_chi)){
	document.getElementById('error_dia_chi').innerHTML = '';
}
else{
	document.getElementById('error_dia_chi').innerHTML = 'Vui lòng nhập vào địa chỉ hợp lệ.';
	kiem_tra_loi = true;
}

//mat_khau
var mat_khau = document.getElementById('mat_khau').value;
var mat_khau_regex = /^[A-Za-z0-9.-_]+$/;
if(mat_khau_regex.test(mat_khau)){
	document.getElementById('error_mat_khau').innerHTML = '';
}
else{
	document.getElementById('error_mat_khau').innerHTML = 'Mật khẩu không hợp lệ.';
	kiem_tra_loi = true;
}
//Mật khẩu mới
var mat_khau_moi = document.getElementById('mat_khau_moi').value;
var mat_khau_moi_regex = /^[A-Za-z0-9.-_]+$/;
if(mat_khau_moi_regex.test(mat_khau_moi)){
	document.getElementById('error_mat_khau_moi').innerHTML = '';
}
else{
	document.getElementById('error_mat_khau_moi').innerHTML = 'Mật khẩu không hợp lệ.';
	kiem_tra_loi = true;
}

if(kiem_tra_loi==true){
	return false;
}
}
</script>
<?php include "include/footer.php" ?>
<?php } else {
  header("location:index.php?error_login=Bạn phải đăng nhập để tiếp tục.");
  // header("location:404page.php");
} ?>
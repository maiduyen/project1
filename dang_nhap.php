<?php
if (isset($_COOKIE['ma_kh'])) {
  $ma_kh = $_COOKIE['ma_kh'];
  $ten_kh = $_COOKIE['ten_kh'];

  setcookie('ma_kh',$ma_kh,time() + 60*86400);
  // session_start();
  $_SESSION['ma_kh'] = $ma_kh;
  $_SESSION['ten_kh'] = $ten_kh;
  header("location:index.php");
}
 ?>
<div id="id01" class="modal" >
  <form class="modal-content animate" method="post" action="xu_ly_dang_nhap.php">
    <div class="imgcontainer">
     <!--  <p id="error_login">
        Sai email hoặc mật khẩu.
      </p> -->
      <?php if (isset($_GET['error_login'])) { ?>
        <p id="error_login">
          <?php echo $_GET['error_login']; ?>
        </p>
      <?php } ?>
      <h1 align="center">Đăng nhập</h1>
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close">&times;</span>
    </div>

    <div class="container" style="position: relative;">
      <label for="uname"><b>Email</b></label>
      <span class="error" id="error_emaill"></span>
      <input id="emaill" type="email" placeholder="Nhập email của bạn" name="email">

      <label for="psw"><b>Mật khẩu</b></label>
      <span class="error" id="error_passwordd"></span>
      <input id="passwordd" type="password" placeholder="Nhập mật khẩu của bạn" name="mat_khau">
      <span onclick="show_passwordd()" style="    font-size: 1em;color: gray;position: absolute;right: 24px;display: inline-block;top: 48%;"><i class="far fa-eye"></i></span>
      <button type="submit" >Đăng nhập</button>
      <label>
        <input type="checkbox" checked="checked" name="remember">Ghi nhớ mật khẩu
      </label>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <a class="register" onclick="register_button()">Đăng ký</a>
      <span class="psw">Nếu bạn chưa có tài khoản.</span>
    </div>
  </form>
</div>
<script>
  function show_passwordd(){
            var temp = document.getElementById("passwordd");
            if (temp.type === "password") { 
                temp.type = "text"; 
            } 
            else { 
                temp.type = "password"; 
            } 
        } 
</script>
//click to hide modal
window.onclick = function(event) {
  var dang_ky = document.getElementById('id02');
  var dang_nhap = document.getElementById('id01');
  if ((event.target == dang_ky) || (event.target == dang_nhap)) {
    dang_ky.style.display = "none";
    dang_nhap.style.display = "none";
  }
}
  //show/hide register error
  function display_register(){
    var dang_ky = document.getElementById('id02');
    var error = document.getElementById('error_register');
    if(dang_ky.style.display=='block'){
      dang_ky.style.display = "none";
      error.style.display = "none";
    }
    else {
      dang_ky.style.display = "block";
      error.style.display = "block";
    }
  }
 //show/hide register in cart
function register_new_customer(){
    var dang_ky = document.getElementById('id02');
    if(dang_ky.style.display=='block'){
      dang_ky.style.display = "none";
    }
    else {
      dang_ky.style.display = "block";
    }
  }

// //show/hide log in error
function display_login(){
  var dang_nhap = document.getElementById('id01');
  var error = document.getElementById('error_login');
  if(dang_nhap.style.display=='block'){
    dang_nhap.style.display = "none";
    error.style.display = "none";
  }
  else {
    dang_nhap.style.display = "block";
    error.style.display = "block";
  }
}
 //show/hide register
function register_button(){
  var dang_nhap =document.getElementById('id01');
  dang_nhap.style.display='none';
  var dang_ky = document.getElementById('id02');
  dang_ky.style.display='block';
}

// validate input form
function kiem_tra_dang_nhap() {
  var kiem_tra_loi = false;
  //Email
  var emaill = document.getElementById('emaill').value;
  var emaill_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(emaill_regex.test(emaill)){
    document.getElementById('error_emaill').innerHTML = '';
  }
  else{
    document.getElementById('error_emaill').innerHTML = 'Email không tồn tại.';
    kiem_tra_loi = true;
  }
 //password
 var passwordd = document.getElementById('passwordd').value;
 var passwordd_regex = /^[A-Za-z0-9\@$!%*#?&-.,]+$/;
 if(passwordd_regex.test(passwordd)){
  document.getElementById('error_passwordd').innerHTML = '';
}
else{
  document.getElementById('error_passwordd').innerHTML = 'Mật khẩu không hợp lệ.';
  kiem_tra_loi = true;
} 
  if(kiem_tra_loi==true){
    return false;
  }
}
//validate đăng ký
function kiem_tra_dang_ky() {
  var kiem_tra_loi = false;
  //Name
  var ten = document.getElementById('ten').value;
  var ten_regex = /^[A-Za-z\d\sáàảãạăẩâẳắằấầặẵẫậéèẻ ẽẹeêếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứửữừ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ÊỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
  if(ten_regex.test(ten)){
    document.getElementById('error_ten').innerHTML = '';
  }
  else{
    document.getElementById('error_ten').innerHTML = 'Vui lòng nhập tên hợp lệ.';
    kiem_tra_loi = true;
  }
  //ngay_sinh
  var ngay_sinh = document.getElementById('ngay_sinh').value;
  if(ngay_sinh ==''){
    document.getElementById('error_ngay_sinh').innerHTML = 'Không được để trống.';
    kiem_tra_loi = true;
  }
  else{
   document.getElementById('error_ngay_sinh').innerHTML = '';
 }
 //gioi_tinh
 var gioi_tinh = document.getElementsByName('gioi_tinh');
 var check_gioi_tinh = false;
 for(var i=0;i<gioi_tinh.length;i++){
  if(gioi_tinh[i].checked){
    check_gioi_tinh = true;
  }
}
if(check_gioi_tinh == true){
  document.getElementById('error_gioi_tinh').innerHTML = '';
}
else{
  document.getElementById('error_gioi_tinh').innerHTML = 'Vui lòng chọn giới tính.';
  kiem_tra_loi = true;
}
//so_dien_thoai
var so_dien_thoai = document.getElementById('so_dien_thoai').value;
var so_dien_thoai_regex = /^\d*\.?\d+$/;
if(so_dien_thoai_regex.test(so_dien_thoai)){
  document.getElementById('error_so_dien_thoai').innerHTML = '';
}
else{
  document.getElementById('error_so_dien_thoai').innerHTML = 'Vui lòng nhập số điện thoại của bạn.';
  kiem_tra_loi = true;
}

//email
var email = document.getElementById('email').value;
  var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
if(email_regex.test(email)){
  document.getElementById('error_email').innerHTML = '';
}
else{
  document.getElementById('error_email').innerHTML = 'Vui lòng nhập vào email của bạn.';
  kiem_tra_loi = true;
}

//dia_chi
var dia_chi = document.getElementById('dia_chi').value;
var dia_chi_regex = /^[a-zA-Z\d,.-\sáàảãạăâắằấầặẩẵẫậéèẻ ẽẹếềểễệóòỏõọôốồổỗộ ơớờởỡợíìỉĩịđùúủũụưứ� �ửữựÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠ ƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼ� ��ỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞ ỠỢỤỨỪỬỮỰỲỴÝỶỸửữựỵ ỳýỷỹ]+$/;
if(dia_chi_regex.test(dia_chi)){
  document.getElementById('error_dia_chi').innerHTML = '';
}
else{
  document.getElementById('error_dia_chi').innerHTML = 'Vui lòng nhập vào địa chỉ hợp lệ.';
  kiem_tra_loi = true;
}

//mat_khau
var mat_khau = document.getElementById('mat_khau').value;
var mat_khau_regex = /^[A-Za-z0-9.-_]+$/;
if(mat_khau_regex.test(mat_khau)){
  document.getElementById('error_mat_khau').innerHTML = '';
}
else{
  document.getElementById('error_mat_khau').innerHTML = 'Mật khẩu không hợp lệ.';
  kiem_tra_loi = true;
}

//Re-password
var nhap_lai_mat_khau = document.getElementById('nhap_lai_mat_khau').value;
if(nhap_lai_mat_khau==mat_khau){
  document.getElementById('error_nhap_lai_mat_khau').innerHTML = '';
}
else{
  document.getElementById('error_nhap_lai_mat_khau').innerHTML = 'Mật khẩu không trùng khớp.';
  kiem_tra_loi = true;
}


if(kiem_tra_loi==true){
  return false;
}
}